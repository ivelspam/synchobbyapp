package com.nhomolka.synchobby;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;

public class ApplicationLifecycleHandler implements Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {
    private static final String TAG = "##ApplicationLifecycle";
    private static boolean isInBackground = false;

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
//        Log.i(TAG, "onActivityCreated");
    }

    @Override
    public void onActivityStarted(Activity activity) {
//        Log.i(TAG, "onActivityStarted");

    }

    @Override
    public void onActivityResumed(Activity activity) {
//        Log.i(TAG, "onActivityResumed");

    }

    @Override
    public void onActivityPaused(Activity activity) {
//        Log.i(TAG, "onActivityPaused");

    }

    @Override
    public void onActivityStopped(Activity activity) {
//        Log.i(TAG, "onActivityStopped");

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
//        Log.i(TAG, "onActivitySaveInstanceState");

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
//        Log.i(TAG, "onActivityDestroyed");

    }

    @Override
    public void onTrimMemory(int i) {
        if(i == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN){
//            Log.d(TAG, "app went to background");
            isInBackground = true;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration configuration) {

    }

    @Override
    public void onLowMemory() {

    }
}
