package com.nhomolka.synchobby.controller.CustomViews.FormViews;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

import com.nhomolka.synchobby.model.Constants.CalendarFormats;
import com.nhomolka.synchobby.model.Extensions.DateExtension;
import com.nhomolka.synchobby.model.Extensions.ExtensionsHack;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class StartEndTimeCL extends ConstraintLayout implements AdapterView.OnClickListener{

    private static final String TAG = "##StartEndTimeCL";

    Button startTimeBT, endTimeBT;
    TextView errorTV, startTV, endTV;
    Context mContext;
    Calendar startCalendar = DateExtension.CalendarToNearestRoundUp(15);
    Calendar endCalendar = DateExtension.CalendarToNearestRoundUp(5);

    enum FieldError {
        StartLowerthanCurrent("Start Time Lower Than Current Time."),
        EndSmallerThanStart("End Calendar Smaller Than Start Calendar."),
        LongerThanFourHours("Event Can't be Longer Than 4 hours."),
        StartTimeLongerThanFourHours("A event can't be create more than 4 hours before."),
        StartEndSame("A event can't be create more than 4 hours before.");


        private String value;
        public String getValue() { return this.value; }
        FieldError(String value){ this.value = value; }
    }

    public StartEndTimeCL(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;

        endCalendar.add(Calendar.MINUTE, 15);
        initializeViews();
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(this);
        constraintSet.connect(startTV.getId(), ConstraintSet.TOP, this.getId(), ConstraintSet.TOP);
        constraintSet.connect(endTV.getId(), ConstraintSet.BASELINE, startTV.getId(), ConstraintSet.BASELINE);
        constraintSet.connect(errorTV.getId(), ConstraintSet.TOP, startTV.getId(), ConstraintSet.BOTTOM);

        constraintSet.constrainWidth(startTimeBT.getId(), 0);
        constraintSet.constrainWidth(endTimeBT.getId(), 0);

        constraintSet.connect(startTimeBT.getId(), ConstraintSet.TOP, errorTV.getId(), ConstraintSet.BOTTOM);
        constraintSet.connect(endTimeBT.getId(), ConstraintSet.TOP, errorTV.getId(), ConstraintSet.BOTTOM);

        constraintSet.connect(startTimeBT.getId(), ConstraintSet.START, this.getId(), ConstraintSet.START);
        constraintSet.connect(startTimeBT.getId(), ConstraintSet.END, endTimeBT.getId(), ConstraintSet.START);
        constraintSet.connect(endTimeBT.getId(), ConstraintSet.START, startTimeBT.getId(), ConstraintSet.END);
        constraintSet.connect(endTimeBT.getId(), ConstraintSet.END, this.getId(), ConstraintSet.END);

        constraintSet.setHorizontalChainStyle(startTimeBT.getId(), ConstraintSet.CHAIN_SPREAD);

        constraintSet.connect(startTV.getId(), ConstraintSet.LEFT, startTimeBT.getId(), ConstraintSet.LEFT);
        constraintSet.connect(endTV.getId(), ConstraintSet.LEFT, endTimeBT.getId(), ConstraintSet.LEFT);
        constraintSet.applyTo(this);

        errorTV.setVisibility(GONE);
    }

    void initializeViews(){
        errorTV = new TextView(getContext());
        errorTV.setText("This is a Error");
        errorTV.setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
        errorTV.setTextSize(12);
        errorTV.setId(View.generateViewId());

        startTV = new TextView(getContext());
        startTV.setText("Start Time");
        startTV.setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
        startTV.setTextSize(12);
        startTV.setId(View.generateViewId());

        endTV = new TextView(getContext());
        endTV.setText("End Time");
        endTV.setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
        endTV.setTextSize(12);
        endTV.setId(View.generateViewId());

        startTimeBT = new Button(getContext());
        startTimeBT.setId(View.generateViewId());
        startTimeBT.setText(DateExtension.CalendarGetFormated(startCalendar, "E H:mma"));
        startTimeBT.setOnClickListener(this);

        endTimeBT = new Button(getContext());
        endTimeBT.setId(View.generateViewId());
        endTimeBT.setText(DateExtension.CalendarGetFormated(endCalendar, "E H:mma"));
        endTimeBT.setOnClickListener(this);

        this.addView(errorTV);
        this.addView(startTV);
        this.addView(endTV);
        this.addView(startTimeBT);
        this.addView(endTimeBT);
    }

    public boolean findError(){

        boolean foundError = false;
        List<FieldError> errors = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();


        if(DateExtension.CalendarGetDifference(calendar, startCalendar, Calendar.MILLISECOND) > 0){
            foundError = true;
            errors.add(FieldError.StartLowerthanCurrent);
        }else if (DateExtension.CalendarGetDifference(endCalendar , startCalendar, endCalendar.MINUTE) < 0){
            foundError = true;
            errors.add(FieldError.EndSmallerThanStart);
        }else if (DateExtension.CalendarGetDifference(endCalendar , startCalendar, endCalendar.MINUTE) > 240){
            foundError = true;
            errors.add(FieldError.LongerThanFourHours);
        }else if (DateExtension.CalendarGetDifference(startCalendar, calendar , endCalendar.MINUTE) > 240){
            foundError = true;
            errors.add(FieldError.StartTimeLongerThanFourHours);
        }else if (DateExtension.CalendarGetDifference(startCalendar, calendar , endCalendar.MINUTE) > 240){
            foundError = true;
            errors.add(FieldError.LongerThanFourHours);
        }else if (DateExtension.CalendarGetDifference(startCalendar, calendar , endCalendar.MINUTE) > 240){
            foundError = true;
            errors.add(FieldError.StartEndSame);
        }

        if(foundError){
            errorTV.setVisibility(VISIBLE);
            errorTV.setText(errors.get(0).value);
        }else{
            errorTV.setVisibility(GONE);
        }
        return foundError;
    }

    public Calendar getStartDatetime(){ return startCalendar;}

    public Calendar getEndDatetime(){ return endCalendar;}

    @Override
    public void onClick(View view) {
        DialogFragment timePickerFragment = new TimePickerFragment(view.getId());
        timePickerFragment.show(((Activity)getContext()).getFragmentManager(), "time picker");
    }


    @SuppressLint("ValidFragment")
    class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener{
        int id;
        public TimePickerFragment(int id){
            this.id = id;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Calendar calendar;
            if(id == startTimeBT.getId()){
                calendar = startCalendar;
            }else{
                calendar = endCalendar;
            }

            return new TimePickerDialog(mContext, this, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), android.text.format.DateFormat.is24HourFormat(mContext));
        }

        @Override
        public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
            Calendar calendar = Calendar.getInstance();

            if(hourOfDay < calendar.get(Calendar.HOUR_OF_DAY)){
                calendar.add(Calendar.DAY_OF_MONTH, 1);
            }

            if(id == startTimeBT.getId()){

                startCalendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH));
                startCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                startCalendar.set(Calendar.MINUTE, minutes);
                startTimeBT.setText(DateExtension.CalendarGetFormated(startCalendar, CalendarFormats.dayhour));

            }else if(id == endTimeBT.getId()){
                endCalendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH));
                endCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                endCalendar.set(Calendar.MINUTE, minutes);
                endTimeBT.setText(DateExtension.CalendarGetFormated(endCalendar, CalendarFormats.dayhour));
            }

            findError();
        }
    }
}
