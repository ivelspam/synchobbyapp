package com.nhomolka.synchobby.controller.CustomViews.FormViews;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.List;

public class ReachCL extends ConstraintLayout {

    enum FieldError {
        empty("Selected a Range Cannot Be Empty.");
        private String value;
        public String getValue() { return this.value; }
        FieldError(String value){ this.value = value; }
    }


    ErrorAndTitlesTextViews errorAndTitlesTextViews;
    RadioGroup owner;
    final RadioButton[] radioButtons = new RadioButton[3];

    public ReachCL(Context context, AttributeSet attrs) {
        super(context, attrs);
        owner = new RadioGroup(getContext());

        radioButtons[0] = new RadioButton(getContext());
        radioButtons[0].setText("Public");
        radioButtons[0].setId(View.generateViewId());
        owner.addView(radioButtons[0]);

        radioButtons[1] = new RadioButton(getContext());
        radioButtons[1].setText("Friends");
        radioButtons[1].setId(View.generateViewId());
        owner.addView(radioButtons[1]);

        radioButtons[2] = new RadioButton(getContext());
        radioButtons[2].setId(View.generateViewId());
        radioButtons[2].setText("Friends and Friends of Friends");
        owner.addView(radioButtons[2]);
        owner.setOrientation(RadioGroup.VERTICAL);
        owner.setId(View.generateViewId());
        this.addView(owner);
        errorAndTitlesTextViews = new ErrorAndTitlesTextViews(getContext(), this, owner, "Event Reach");
        errorAndTitlesTextViews.hideError();
        errorAndTitlesTextViews.bottomTV.setVisibility(GONE);
        owner.check(radioButtons[0].getId());
    }

    public boolean findError(){

        boolean foundError = false;
        List<FieldError> errors = new ArrayList<>();

        if(owner.getCheckedRadioButtonId() == -1){
            foundError = true;
            errors.add(FieldError.empty);
        }

        if(foundError){
            errorAndTitlesTextViews.errorTV.setVisibility(VISIBLE);
            errorAndTitlesTextViews.errorTV.setText(errors.get(0).value);
        }else{
            errorAndTitlesTextViews.errorTV.setVisibility(GONE);
        }

        return foundError;


    }

    public int getValue(){

        if(owner.getCheckedRadioButtonId() == radioButtons[0].getId()){
            return 0;
        }else if(owner.getCheckedRadioButtonId() == radioButtons[1].getId()){
            return 1;
        }else if(owner.getCheckedRadioButtonId() == radioButtons[2].getId()){
            return 2;
        }

        return -1;

    }
}
