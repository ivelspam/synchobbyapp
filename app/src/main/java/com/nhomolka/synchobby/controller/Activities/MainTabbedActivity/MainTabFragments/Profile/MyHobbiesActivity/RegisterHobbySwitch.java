package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Profile.MyHobbiesActivity;

import android.content.Context;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Switch;

import com.nhomolka.synchobby.controller.Interfaces.MyHobbiesInterface;
import com.nhomolka.synchobby.model.SQLite.Objects.Hobby;
import com.nhomolka.synchobby.model.SQLite.Motherbase;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Hobbies_addHobby;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Hobbies_removeHobby;
import com.nhomolka.synchobby.model.ServerRequest.ServerAPI;
import com.nhomolka.synchobby.model.Toasts;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterHobbySwitch extends Switch implements View.OnClickListener{


    private static final String TAG = "##RegisterHobbySwitch";

    Hobby mHobby;
    MyHobbiesInterface mMyHobbiesInterface;
    String mType;

    public RegisterHobbySwitch(Context context) {
        super(context);
    }

    public RegisterHobbySwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void customInit(Hobby hobby, MyHobbiesInterface myHobbiesInterface, String type){
        mHobby = hobby;
        mMyHobbiesInterface = myHobbiesInterface;
        mType = type;
        setChecked(hobby.isInHobby());

        this.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        new AddHobbyRequest().execute();
    }


    public class AddHobbyRequest extends AsyncTask<String, String, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... params) {

            if(isChecked()) {
                return new ServerAPI(getContext()).serverRequestPOST(new Hobbies_addHobby(mHobby.getHobbyID()));
            }else {
                return new ServerAPI(getContext()).serverRequestPOST(new Hobbies_removeHobby(mHobby.getHobbyID()));
            }
        }

        protected void onPostExecute(JSONObject jsonObject) {
            try {

                Log.i(TAG, "onPostExecute(JSONObject jsonObject)");

                Log.i(TAG, jsonObject.toString());

                if(Toasts.responseIsGoodToaster(jsonObject, getContext())) {

                    switch (jsonObject.getString(APIRequest.message)){
                        case Hobbies_removeHobby.Response.removed:
                            Motherbase.getInstance(getContext()).getAppUserHobby().removeAHobbies(mHobby.getHobbyID());
                            if(mMyHobbiesInterface != null){
                                mMyHobbiesInterface.updateHobby(mHobby, false, mType);
                            }
                            break;
                        case Hobbies_addHobby.Response.hobbyAdded:
                            Motherbase.getInstance(getContext()).getAppUserHobby().insertAHobby(mHobby.getHobbyID());
                            if(mMyHobbiesInterface != null){
                                mMyHobbiesInterface.updateHobby(mHobby, true, mType);
                            }

                            break;
                        default:
                            setChecked(!isChecked());
                    }
                }else{
                    Log.i(TAG, "AAAAAAAAAAAAAAAAAAA");
                    Log.i(TAG, "AAAAAAAAAAAAAAAAAAA: " + isEnabled());
                    setChecked(!isChecked());
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
