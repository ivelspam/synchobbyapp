package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Profile;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.BaseActivity;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Appuser_updateUsername;
import com.nhomolka.synchobby.model.ServerRequest.ServerAPI;
import com.nhomolka.synchobby.model.SharedPreferencesUtils;
import com.nhomolka.synchobby.model.Toasts;

import org.json.JSONException;
import org.json.JSONObject;

public class EditUsernameActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "##EditUsernameActivity";

    Button updateBT;
    TextView usernameTIET;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_username);

        view_build();
    }


    void view_build(){
        usernameTIET = findViewById(R.id.activityEditUserName_usernameTIET);
        updateBT = findViewById(R.id.activityEditUsername_updateBT);

        //INITIAL STATE
        usernameTIET.setText(SharedPreferencesUtils.getInstance(getBaseContext()).getString(SharedPreferencesUtils.PrefKeys.Username_str));
        updateBT.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == updateBT.getId()){
            String newUsername = usernameTIET.getText().toString();
            String oldUsername = SharedPreferencesUtils.getInstance(getBaseContext()).getString(SharedPreferencesUtils.PrefKeys.Username_str);
            if(newUsername.compareTo(oldUsername) !=0 ){
                new UpdateUsername_http().execute();
            }else{
                Toast.makeText(getBaseContext(), "Nothing Changed. Try a Different Username.", Toast.LENGTH_LONG).show();
            }

        }
    }


    private class UpdateUsername_http extends AsyncTask<Void, Void, JSONObject>{

        @Override
        protected JSONObject doInBackground(Void... voids) {
            return new ServerAPI(getBaseContext()).serverRequestPOST( new Appuser_updateUsername(usernameTIET.getText().toString()));
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                if(Toasts.responseIsGoodToaster(jsonObject, getBaseContext())){
                    switch(jsonObject.getString(APIRequest.message)){
                        case Appuser_updateUsername.Response.updated:
                            SharedPreferencesUtils.getInstance(getBaseContext()).put(SharedPreferencesUtils.PrefKeys.Username_str, usernameTIET.getText().toString());

                            Toast.makeText(getBaseContext(), "Updated", Toast.LENGTH_SHORT).show();
                            break;
                        case Appuser_updateUsername.Response.notUnique:
                            Toast.makeText(getBaseContext(), "Can't use this Username", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}
