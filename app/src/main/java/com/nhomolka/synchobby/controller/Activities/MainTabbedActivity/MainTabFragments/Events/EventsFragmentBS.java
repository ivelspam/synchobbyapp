package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Events;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.nhomolka.synchobby.controller.Interfaces.UpdateEventsFragmentInterface;
import com.nhomolka.synchobby.model.Constants.Timers;
import com.nhomolka.synchobby.model.Extensions.DateExtension;
import com.nhomolka.synchobby.model.Extensions.ExtensionsHack;
import com.nhomolka.synchobby.model.SQLite.Objects.Event;
import com.nhomolka.synchobby.model.SQLite.Motherbase;
import com.nhomolka.synchobby.model.SQLite.Tables.EventTable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventsFragmentBS extends Service {
    private static final String TAG = "##ShouldEventsFragmentU";

    private final IBinder localBinder = new LocalBinder();
    List<Event> mEvents = new ArrayList<>();
    Handler shouldEventsUpdateHandler = new Handler();
    String mClientUpdatedTS = "1970-01-01 00:00:00.000";
    UpdateEventsFragmentInterface updateEventsFragmentInterface;

    EventTable eventTable = Motherbase.getInstance(getBaseContext()).getEventTable();


    Runnable shouldEventsUpdateRunnable = new Runnable() {
        @Override
        public void run() {
            getUpdates();
            shouldEventsUpdateHandler.postDelayed(this, Timers.UPDATE_EVENTS_FRAGMENT);
        }
    };

    void getUpdates(){
        new GetUpdatesAsync().execute();
    }

    public void setInterface(UpdateEventsFragmentInterface updateEventsFragmentInterface){
        this.updateEventsFragmentInterface = updateEventsFragmentInterface;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return localBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        shouldEventsUpdateHandler.postDelayed(shouldEventsUpdateRunnable, 0);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        shouldEventsUpdateHandler.removeCallbacks(shouldEventsUpdateRunnable);
        return super.onUnbind(intent);
    }

    public class LocalBinder extends Binder {
        public EventsFragmentBS getService(){
            return EventsFragmentBS.this;
        }
    }

    class GetUpdatesAsync extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... params) {
            mEvents =  Motherbase.getInstance(getBaseContext()).getEventTable().selectEventGreaterThanClientUpdatedTS(mClientUpdatedTS);
            updatemEventUpdatedTS();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            updateEventsFragmentInterface.setUpEvents(mEvents);
            eventTable.removeOldValues();
        }
    }

    void updatemEventUpdatedTS(){
        for(Event event : mEvents){
            if(DateExtension.CalendarCompareTwoSQLiteTSFirstBiggerThanSecond(event.getClientUpdatedTS(), mClientUpdatedTS)){
                mClientUpdatedTS = event.getClientUpdatedTS();
            }
        }
    }
}
