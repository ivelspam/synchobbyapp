package com.nhomolka.synchobby.controller.CustomViews.FormViews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class ErrorAndTitlesTextViews{
    private static final String TAG = "##ErrorAndTitlesTextVie";

    public TextView errorTV, titleTV, bottomTV;

    void initializeViews(Context context, String title){
        errorTV = new TextView(context);
        errorTV.setText("This is a Error");
        errorTV.setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
        errorTV.setTextSize(12);
        errorTV.setId(View.generateViewId());
        errorTV.setVisibility(View.GONE);

        titleTV = new TextView(context);
        titleTV.setText(title);
        titleTV.setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
        titleTV.setTextSize(12);
        titleTV.setId(View.generateViewId());

        bottomTV = new TextView(context);
        bottomTV.setText("This is here");
        bottomTV.setTypeface(Typeface.create("sans-serif", Typeface.BOLD));
        bottomTV.setTextSize(12);
        bottomTV.setId(View.generateViewId());
        bottomTV.setVisibility(View.GONE);
    }

    public ErrorAndTitlesTextViews(Context context, ConstraintLayout selfLayout, View ownerView, String title){
        initializeViews(context, title);

        selfLayout.addView(errorTV);
        selfLayout.addView(titleTV);
        selfLayout.addView(bottomTV);

        ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(0, ConstraintLayout.LayoutParams.WRAP_CONTENT);
        ownerView.setLayoutParams(layoutParams);

        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(selfLayout);

        constraintSet.connect(titleTV.getId(), ConstraintSet.TOP, selfLayout.getId(), ConstraintSet.TOP);
        constraintSet.connect(errorTV.getId(), ConstraintSet.TOP, titleTV.getId(), ConstraintSet.BOTTOM);
        constraintSet.connect(ownerView.getId(), ConstraintSet.TOP, errorTV.getId(), ConstraintSet.BOTTOM);
        constraintSet.connect(bottomTV.getId(), ConstraintSet.TOP, ownerView.getId(), ConstraintSet.BOTTOM);
        constraintSet.connect(bottomTV.getId(), ConstraintSet.BOTTOM, selfLayout.getId(), ConstraintSet.BOTTOM);

        constraintSet.connect(ownerView.getId(), ConstraintSet.START, selfLayout.getId(), ConstraintSet.START);
        constraintSet.connect(ownerView.getId(), ConstraintSet.END, selfLayout.getId(), ConstraintSet.END);
        constraintSet.connect(titleTV.getId(), ConstraintSet.LEFT, ownerView.getId(), ConstraintSet.LEFT);
        constraintSet.connect(errorTV.getId(), ConstraintSet.LEFT, ownerView.getId(), ConstraintSet.LEFT);

        constraintSet.constrainDefaultWidth(selfLayout.getId(), ConstraintSet.MATCH_CONSTRAINT);

        constraintSet.applyTo(selfLayout);
    }

    public void hideError(){

        errorTV.setVisibility(View.GONE);

    }

    public void showError(){
        errorTV.setVisibility(View.VISIBLE);
    }

}
