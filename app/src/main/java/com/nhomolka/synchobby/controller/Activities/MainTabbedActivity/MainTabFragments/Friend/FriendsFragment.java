package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Friend;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;

import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Events.EventsFragment;
import com.nhomolka.synchobby.controller.Interfaces.UpdateFriendsFragmentInferface;
import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.model.Constants.RequestCodes;
import com.nhomolka.synchobby.model.SQLite.Objects.Friend;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.ServerRequest.QueryForUpdatedSingleton;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.AllInfo_getUpdates;
import com.nhomolka.synchobby.model.Toasts;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class FriendsFragment extends Fragment implements View.OnClickListener, ViewPager.OnPageChangeListener, UpdateFriendsFragmentInferface {
    private static final String TAG = "##FriendsFragment";

    SwipeRefreshLayout vSwipeRefreshLayout;
    Button friendsBT, pendingBT, sentBT;
    ImageButton findBT;
    ViewPager viewPager;
    FriendsListFragment confirmedFragment, requestFragment, sentFragment;
    CustomPagerAdapter mCustomPagerAdapter;
    FriendListFragmentBS checkForNewFriendsBS;
    UpdateFriendsFragmentInferface updateFriendsFragmentInferface;
    private boolean isServiceBound = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_friends, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        updateFriendsFragmentInferface = this;
        Log.i(TAG, "onCreate");

        Bundle args1 = new Bundle();
        args1.putInt(MySQLColumns.State, 2);
        confirmedFragment = new FriendsListFragment();
        confirmedFragment.setArguments(args1);

        Bundle args2 = new Bundle();
        args2.putInt(MySQLColumns.State, 1);
        requestFragment = new FriendsListFragment();
        requestFragment.setArguments(args2);

        Bundle args3 = new Bundle();
        args3.putInt(MySQLColumns.State, 0);
        sentFragment = new FriendsListFragment();
        sentFragment.setArguments(args3);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i(TAG, "onViewCreated");
        build_view();
    }

    void build_view(){
        friendsBT = getView().findViewById(R.id.fragmentFriends_friendsBT);
        pendingBT = getView().findViewById(R.id.fragmentFriends_pendingBT);
        sentBT = getView().findViewById(R.id.fragmentFriends_sentBT);
        findBT = getView().findViewById(R.id.fragmentFriends_findBT);
        viewPager = getView().findViewById(R.id.fragmentFriends_viewPager);
        vSwipeRefreshLayout = getView().findViewById(R.id.fragmentFriends_swipeRefreshLayout);

        //listeners
        friendsBT.setOnClickListener(this);
        pendingBT.setOnClickListener(this);
        sentBT.setOnClickListener(this);
        findBT.setOnClickListener(this);
        viewPager.addOnPageChangeListener(this);
        //Listeners
        vSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                vSwipeRefreshLayout.setRefreshing(false);
                new getFriendsUpdate().execute();
            }
        });

        //set adapters
        mCustomPagerAdapter = new CustomPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(mCustomPagerAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult: " + requestCode + " " + resultCode);
        if(requestCode == RequestCodes.FriendSentRequest && resultCode == Activity.RESULT_OK){
            sentFragment.remove(data.getIntExtra(MySQLColumns.AppUserID, -1));
        }
        checkForNewFriendsBS.reload();
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == friendsBT.getId()){

            friendsBT.setSelected(true);
            pendingBT.setSelected(false);
            sentBT.setSelected(false);
            viewPager.setCurrentItem(0, false);
        }else if(view.getId() == pendingBT.getId()){

            friendsBT.setSelected(false);
            pendingBT.setSelected(true);
            sentBT.setSelected(false);
            viewPager.setCurrentItem(1, false);
        }else if(view.getId() == sentBT.getId()){

            friendsBT.setSelected(false);
            pendingBT.setSelected(false);
            sentBT.setSelected(true);
            viewPager.setCurrentItem(2, false);
        }else if(view.getId() == findBT.getId()){
            Intent intent = new Intent(getContext(), FindFriendActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if(position == 0){
            friendsBT.setSelected(true);
            pendingBT.setSelected(false);
            sentBT.setSelected(false);
        }else if(position == 1){
            friendsBT.setSelected(false);
            pendingBT.setSelected(true);
            sentBT.setSelected(false);
        }else if(position == 2){
            friendsBT.setSelected(false);
            pendingBT.setSelected(false);
            sentBT.setSelected(true);
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            checkForNewFriendsBS = ((FriendListFragmentBS.LocalBinder)service).getService();
            checkForNewFriendsBS.customInit(updateFriendsFragmentInferface);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.i(TAG, "onServiceDisconnected");
        }
    };


    class getFriendsUpdate extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(Void... voids) {
            return QueryForUpdatedSingleton.getUpdates(getActivity());
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {

            Log.i(TAG, jsonObject.toString());
            try {
                if(Toasts.responseIsGoodToaster(jsonObject, getActivity())) {
                    switch (jsonObject.getString(APIRequest.message)){
                        case AllInfo_getUpdates.Response.foundUpdates:
                            checkForNewFriendsBS.getUpdates();
                        default:
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();

        startService();

    }

    void startService(){

        if(!isServiceBound){
            isServiceBound = true;
//            Log.i(TAG, "START SERVICE");
            Intent intent = new Intent(getActivity(), FriendListFragmentBS.class);
            getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    public void onDetach() {
        endService();
        super.onDetach();
    }

    void endService(){
//        Log.i(TAG, "END SERVICE");
        getActivity().unbindService(mConnection);
        isServiceBound = false;
    }

    @Override
    public void updateList(List<Friend> friends) {
        confirmedFragment.insertOrRemoveFriends(friends);
        requestFragment.insertOrRemoveFriends(friends);
        sentFragment.insertOrRemoveFriends(friends);
    }

    @Override
    public void updateListView() {

    }

    private class CustomPagerAdapter extends FragmentPagerAdapter {

        public CustomPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0 :
                    return confirmedFragment;
                case 1 :
                    return requestFragment;
                case 2 :
                    return sentFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }


    @Override
    public void onPageSelected(int position) {}

    @Override
    public void onPageScrollStateChanged(int state) {}

}
