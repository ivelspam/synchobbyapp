package com.nhomolka.synchobby.controller.Activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Email_addUser;
import com.nhomolka.synchobby.model.ServerRequest.ServerAPI;
import com.nhomolka.synchobby.model.SharedPreferencesUtils;
import com.nhomolka.synchobby.model.Toasts;

import org.json.JSONException;
import org.json.JSONObject;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "##SignupActivity";

    String email, password, name;
    ProgressDialog progressDialog;
    EditText etName, etEmail, etPassword;
    Button btSignup;
    TextView tvLogin;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        view_build();
    }

    private void view_build(){

        etName = findViewById(R.id.signupActivity_inputNameET);
        etEmail = findViewById(R.id.signupActivity_emailET);
        etPassword = findViewById(R.id.signupActivity_passwordET);
        btSignup = findViewById(R.id.signupActivity_signupBT);
        tvLogin = findViewById(R.id.signupActivity_loginTV);

        //Listeners
        btSignup.setOnClickListener(this);
        tvLogin.setOnClickListener(this);
    }

    public void signup() {
        Log.d(TAG, "Signup");
        if (!validate()) {
            onSignupFailed();
            return;
        }
        btSignup.setEnabled(false);

        view_progressDialog();


        name = etName.getText().toString();
        email = etEmail.getText().toString();
        password = etPassword.getText().toString();

        // TODO: Implement your own signup logic here.
        new RegisterUserByEmail_http().execute();
    }

    public void onSignupSuccess(JSONObject jsonObject) throws JSONException {
        Log.i(TAG, "onSignupSuccess");
        SharedPreferencesUtils.getInstance(this).put(SharedPreferencesUtils.PrefKeys.Token_str, jsonObject.getString("token"));

        Log.i(TAG, jsonObject.getString("token"));
        setResult(RESULT_OK, null);

        Intent intent = new Intent(this, TransactionLoginScreenActivity.class);
        startActivity(intent);
        finish();
    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
        Log.i(TAG, "onSignupFailed");
    }

    public boolean validate() {
        boolean valid = true;

        String name = etName.getText().toString();
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            etName.setError("at least 3 characters");
            valid = false;
        } else {
            etName.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.setError("enter a valid email address");
            valid = false;
        } else {
            etEmail.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 ) {
            etPassword.setError("Password can't be smaller than 4 characters");
            valid = false;
        } else {
            etPassword.setError(null);
        }

        return valid;
    }



    public class RegisterUserByEmail_http extends AsyncTask<String, String, JSONObject> {
        @Override
        protected JSONObject doInBackground(String... params) {
            return new ServerAPI(getApplicationContext()).serverRequestPOST(new Email_addUser(name, email, password));
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                progressDialog.dismiss();
                btSignup.setEnabled(true);

                if(Toasts.responseIsGoodToaster(jsonObject, getBaseContext())){
                    switch (jsonObject.getString(APIRequest.message)){
                        case Email_addUser.Response.registered:
                            onSignupSuccess(jsonObject);
                            break;
                        case Email_addUser.Response.alreadyExists:
                            onSignupFailed();
                            break;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View view) {

        if(view == btSignup){
            signup();
        }else if(view == tvLogin){
            finish();
        }

    }

    private void view_progressDialog(){
        progressDialog = new ProgressDialog(SignupActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");
        progressDialog.show();
    }

}
