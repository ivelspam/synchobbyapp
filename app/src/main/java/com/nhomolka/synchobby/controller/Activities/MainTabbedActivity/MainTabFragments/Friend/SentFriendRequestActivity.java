package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Friend;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.BaseActivity;
import com.nhomolka.synchobby.model.SQLite.Motherbase;
import com.nhomolka.synchobby.model.SQLite.Objects.Friend;
import com.nhomolka.synchobby.model.SQLite.Tables.FriendTable;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Friend_cancel;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.ServerRequest.ServerAPI;
import com.nhomolka.synchobby.model.UniversalImageLoader;
import com.nhomolka.synchobby.model.Toasts;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class SentFriendRequestActivity extends BaseActivity {
    private static final String TAG = "##PendingResponseActivi";

    CircleImageView vFriendCIV;
    Button vCancelBT;
    TextView vStatusTV, vNameTV;



    int mAppUserID, mImageVersion;
    Friend mFriend;
    FriendTable mFriendTable = Motherbase.getInstance(getBaseContext()).getFriendTable();

    boolean canceled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sent_friend_request);
        Bundle extras = getIntent().getExtras();
        mAppUserID = extras.getInt(MySQLColumns.AppUserID);
        mImageVersion = extras.getInt(MySQLColumns.ImageVersion);

        mFriend = mFriendTable.selectFriendByAppUserID(mAppUserID);
        build_view();
    }

    void build_view(){
        vFriendCIV = findViewById(R.id.activityPendingResponse_friendCIV);
        vCancelBT = findViewById(R.id.activityPendingResponse_cancelBT);
        vNameTV = findViewById(R.id.activityPendingResponse_nameTV);
        vStatusTV = findViewById(R.id.activityPendingResponse_statusTV);

        //SET LISTENERS
        vCancelBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new cancelRequest_http().execute();
            }
        });

        //INITIAL STATES
        vNameTV.setText(mFriend.getName());
        UniversalImageLoader.setImage(mAppUserID, mImageVersion, vFriendCIV);

    }

    class cancelRequest_http extends AsyncTask<Void, Void, JSONObject>{
        @Override
        protected JSONObject doInBackground(Void... voids) {
            return new ServerAPI(getBaseContext()).serverRequestPOST(new Friend_cancel(mAppUserID));
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                if(Toasts.responseIsGoodToaster(jsonObject, getBaseContext()));
                switch (jsonObject.getString(APIRequest.message)) {
                    case Friend_cancel.Response.canceled :
                        canceled = true;
                        mFriendTable.deleteFriend(mFriend.getAppUserID());
                        vStatusTV.setText("Canceled");
                        vCancelBT.setVisibility(View.GONE);
                        break;
                    default :
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onBackPressed() {

        if(canceled){


            Intent i = new Intent();
            i.putExtra(MySQLColumns.AppUserID, mFriend.getAppUserID());
            setResult(RESULT_OK, i);
        }

        super.onBackPressed();



    }
}
