package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Friend;

import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.BaseActivity;
import com.nhomolka.synchobby.model.Extensions.ExtensionsHack;
import com.nhomolka.synchobby.model.SQLite.Objects.Friend;
import com.nhomolka.synchobby.model.SQLite.Motherbase;
import com.nhomolka.synchobby.model.SQLite.Tables.FriendTable;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Appuser_findAUserByUsername;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Friend_sendRequest;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.ServerRequest.ServerAPI;
import com.nhomolka.synchobby.model.UniversalImageLoader;
import com.nhomolka.synchobby.model.Toasts;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;


public class FindFriendActivity extends BaseActivity implements View.OnClickListener{
    private static final String TAG = "##FindFriendActivity";

    Button findUsernameBT, addUserFriendBT;
    EditText findUserNameET;
    ConstraintLayout foundUserContainerCL;
    CircleImageView vFoundUserCIV;
    TextView foundUsernameTV, responseMessageTV;
    TextView notFoundTV;

    private String mUsername;
    private Friend friend;
    private FriendTable mFriendTable = Motherbase.getInstance(getBaseContext()).getFriendTable();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user);
        view_build();
    }

    private void view_build(){
        responseMessageTV = findViewById(R.id.activitySearchUser_tvResponseMessage);
        findUsernameBT = findViewById(R.id.activitySearchUser_btFindUsername);
        foundUserContainerCL = findViewById(R.id.activitySearchUser_foundUserContainerCL);
        addUserFriendBT = findViewById(R.id.activitySearchUser_btAddUserFriend);
        findUserNameET = findViewById(R.id.activitySearchUser_findUsernameET);
        vFoundUserCIV = findViewById(R.id.activitySearchUser_FoundUserCIV);
        foundUsernameTV = findViewById(R.id.tvFoundUsername);
        notFoundTV = findViewById(R.id.activitySearchUser_notFoundTV);

        //Initial State
        notFoundTV.setVisibility(View.INVISIBLE);
        foundUserContainerCL.setVisibility(View.INVISIBLE);

        //add clickListeners
        findUsernameBT.setOnClickListener(this);
        addUserFriendBT.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if(view.getId() == findUsernameBT.getId()){
            mUsername = findUserNameET.getText().toString();
            responseMessageTV.setText("");
            new findUserByUsername_http().execute();
        }else if(view.getId() == addUserFriendBT.getId()){
            new addFriendUser_http().execute();
        }
    }

    public class addFriendUser_http extends AsyncTask<String, String, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... params) {
            return new ServerAPI(getApplicationContext()).serverRequestPOST(new Friend_sendRequest(friend.getAppUserID()));
        }

        protected void onPostExecute(JSONObject jsonObject) {


            Log.i(TAG, jsonObject.toString());
            try {
                if(Toasts.responseIsGoodToaster(jsonObject, getBaseContext())){
                    switch (jsonObject.getString(APIRequest.message)){
                        case Friend_sendRequest.Response.userAlreadyExists:
                            responseMessageTV.setText("User Already added");
                            break;
                        case Friend_sendRequest.Response.userAdded:
                            Log.i(TAG, jsonObject.toString());
                            responseMessageTV.setText("User Added");
                            mFriendTable.insertOrReplaceFriendFromMySQL(jsonObject.getJSONObject("friendInfo"));
                            break;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public class findUserByUsername_http extends AsyncTask<String, String, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... params) {
            return new ServerAPI(getApplicationContext()).serverRequestGET(new Appuser_findAUserByUsername(mUsername));
        }

        protected void onPostExecute(JSONObject jsonObject) {
            try {
                if(Toasts.responseIsGoodToaster(jsonObject, getBaseContext())){
                    switch (jsonObject.getString(APIRequest.message)){
                        case Appuser_findAUserByUsername.Response.userFound :
                            JSONObject userJSONObject = jsonObject.getJSONObject(Appuser_findAUserByUsername.ResponseFields.user);
                            Log.i(TAG, ExtensionsHack.GsonPrettify(userJSONObject));
                            friend = new Friend(userJSONObject);
                            foundUsernameTV.setText(userJSONObject.getString(MySQLColumns.Name));
                            foundUserContainerCL.setVisibility(View.VISIBLE);
                            notFoundTV.setVisibility(View.INVISIBLE);
                            UniversalImageLoader.setImage(userJSONObject.getInt(MySQLColumns.AppUserID), userJSONObject.getInt(MySQLColumns.ImageVersion), vFoundUserCIV);

                            break;
                        case Appuser_findAUserByUsername.Response.userNotFound:
                            notFoundTV.setVisibility(View.VISIBLE);
                            foundUserContainerCL.setVisibility(View.INVISIBLE);
                            break;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
