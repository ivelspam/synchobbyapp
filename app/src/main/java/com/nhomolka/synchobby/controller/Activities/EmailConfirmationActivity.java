package com.nhomolka.synchobby.controller.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.auth0.android.jwt.JWT;
import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.model.Constants.TokenValues;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Appuser_checkIfExists;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Email_isConfirmed;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Email_sendAnotherToken;
import com.nhomolka.synchobby.model.LogOut.Logout;
import com.nhomolka.synchobby.model.ServerRequest.ServerAPI;
import com.nhomolka.synchobby.model.SharedPreferencesUtils;
import com.nhomolka.synchobby.model.Toasts;

import org.json.JSONException;
import org.json.JSONObject;

public class EmailConfirmationActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "##EmailConfirmationAct";
    ProgressDialog progressDialog;
    Button vConfirmButtonBT, vSendAnotherTokenBT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_confirmation);
        new CheckIfUserExists().execute();
    }

    private void view_build(){
        vConfirmButtonBT = findViewById(R.id.activityEmailConfirmation_confirmButtonBT);
        vSendAnotherTokenBT = findViewById(R.id.activityEmailConfirmation_sendAnotherTokenBT);

        //LISTENERS
        vConfirmButtonBT.setOnClickListener(this);
        vSendAnotherTokenBT.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        Log.i(TAG, "CLICK");
        if(view == vConfirmButtonBT){
            vConfirmButtonBT.setEnabled(false);
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Checking for token...");
            progressDialog.show();
            new checkIfEmailIsConfirmed().execute();
        }else if(view == vSendAnotherTokenBT){

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Sending Another Token...");
            progressDialog.show();
            new SendAnotherToken().execute();
        }
    }

    Activity getActivity(){
        return this;
    }

    public class checkIfEmailIsConfirmed extends AsyncTask<String, String, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... params) {
            Log.i(TAG, "checkIfEmailIsConfirmed");
            return new ServerAPI(getApplicationContext()).serverRequestGET(new Email_isConfirmed());
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                progressDialog.dismiss();

                if(Toasts.responseIsGoodToaster(jsonObject, getBaseContext())){
                    switch (jsonObject.getString(APIRequest.message)){
                        case Email_isConfirmed.Response.confirmed:
                            JWT jwtToken = new JWT(jsonObject.getString("token"));

                            String accountStatus = jwtToken.getClaim(TokenValues.AccountStatus).asString();

                            if(accountStatus.compareTo(TokenValues.AccountStatusState.Confirmed.getValueString()) == 0){
                                SharedPreferencesUtils.getInstance(getBaseContext()).put(SharedPreferencesUtils.PrefKeys.Token_str, jsonObject.getString("token"));
                                Intent intent = new Intent(getBaseContext(), TransactionLoginScreenActivity.class);
                                startActivity(intent);
                                finish();
                            }else{
                                Toast.makeText(getBaseContext(), "Not Confirmed Yet", Toast.LENGTH_LONG).show();
                                vConfirmButtonBT.setEnabled(true);
                            }

                            break;
                        case Email_isConfirmed.Response.notConfirmedYet:
                            Toast.makeText(getBaseContext(), "Not Confirmed Yet", Toast.LENGTH_LONG).show();
                            vConfirmButtonBT.setEnabled(true);

                            break;
                        default:
                            Logout.logout(getActivity());
                            break;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public class CheckIfUserExists extends AsyncTask<String, String, JSONObject> {
        @Override
        protected JSONObject doInBackground(String... params) {
            Log.i(TAG, "CheckIfUserExists");

            Log.i(TAG, SharedPreferencesUtils.getInstance(getBaseContext()).getString(SharedPreferencesUtils.PrefKeys.Token_str));
            return new ServerAPI(getApplicationContext()).serverRequestGET(new Appuser_checkIfExists());
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                if(Toasts.responseIsGoodToaster(jsonObject, getBaseContext())) {
                    switch (jsonObject.getString(APIRequest.message)) {
                        case Appuser_checkIfExists.Response.userFound:
                            view_build();
                            break;
                        case Appuser_checkIfExists.Response.userNotFound:
                            Logout.logout(getActivity());
                            break;

                        default:
//                            Logout.logout(getActivity());
                            Log.i(TAG, "ERROR WRONG OPTION CheckIfUserExists");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public class SendAnotherToken extends AsyncTask<String, String, JSONObject> {
        @Override
        protected JSONObject doInBackground(String... params) {
            return new ServerAPI(getApplicationContext()).serverRequestPOST(new Email_sendAnotherToken());
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                progressDialog.dismiss();
                if(Toasts.responseIsGoodToaster(jsonObject, getBaseContext())){
                    switch (jsonObject.getString(APIRequest.message)){
                        case Email_sendAnotherToken.Response.sent :
                            Toast.makeText(getBaseContext(), "Email Sent.", Toast.LENGTH_LONG).show();
                            break;
                        case Email_sendAnotherToken.Response.registeredAlready :
                            Toast.makeText(getBaseContext(), "You are registered already", Toast.LENGTH_LONG).show();
                            break;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}
