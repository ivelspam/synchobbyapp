package com.nhomolka.synchobby.controller.CustomViews.FormViews;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class PlaceNameCL extends ConstraintLayout {
    ErrorAndTitlesTextViews errorAndTitlesTextViews;
    EditText owner;

    enum FieldError {
        empty("Place Name Cannot Be Empty.");
        private String value;
        public String getValue() { return this.value; }
        FieldError(String value){ this.value = value; }
    }

    public PlaceNameCL(Context context, AttributeSet attrs) {
        super(context, attrs);
        owner = new EditText(getContext());
        owner.setId(View.generateViewId());
        this.addView(owner);
        errorAndTitlesTextViews = new ErrorAndTitlesTextViews(getContext(), this, owner, "Place Name");
    }

    public boolean findError(){

        boolean foundError = false;
        List<FieldError> errors = new ArrayList<>();

        if(owner.getText().length() == 0){
            foundError = true;
            errors.add(FieldError.empty);
        }

        if(foundError){
            errorAndTitlesTextViews.errorTV.setVisibility(VISIBLE);
            errorAndTitlesTextViews.errorTV.setText(errors.get(0).value);
        }else{
            errorAndTitlesTextViews.errorTV.setVisibility(GONE);
        }

        return foundError;
    }


    public String getValue(){
        return owner.getText().toString();
    }
}
