package com.nhomolka.synchobby.controller.Interfaces;

import com.nhomolka.synchobby.model.SQLite.Objects.Hobby;

/**
 * Created by nhomo on 9/13/2018.
 */

public interface MyHobbiesInterface {

    void updateHobby(Hobby HobbyID, Boolean inHobby, String option);

}
