package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Events;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.BaseActivity;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;

public class InEventMoreInfoActivity extends BaseActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {
    private static final String TAG = "##InEventMoreInfoAct";
    int mEventID;
    Button commentsBT, informationBT;
    ViewPager fragmentsVP;
    FragmentsVPA fragmentsVPA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_event_more_info);
        mEventID = getIntent().getExtras().getInt(MySQLColumns.EventID);
        fragmentsVPA = new FragmentsVPA(getSupportFragmentManager());
        build_view();
    }

    void build_view() {
        commentsBT = findViewById(R.id.activityInEventMoreInfo_commentsBT);
        informationBT = findViewById(R.id.activityInEventMoreInfo_informationBT);
        fragmentsVP = findViewById(R.id.activityInEventMoreInfo_fragmentsVP);

        //Initial State
        informationBT.setSelected(true);

        //Set Listeners
        commentsBT.setOnClickListener(this);
        informationBT.setOnClickListener(this);
        fragmentsVP.addOnPageChangeListener(this);

        //set adapters
        fragmentsVP.setAdapter(fragmentsVPA);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == informationBT.getId()) {
            commentsBT.setSelected(false);
            informationBT.setSelected(true);
            fragmentsVP.setCurrentItem(0);
        }else if (view.getId() == commentsBT.getId()) {
            commentsBT.setSelected(true);
            informationBT.setSelected(false);
            fragmentsVP.setCurrentItem(1);
        }
    }

    @Override
    public void onPageSelected(int position) {
        if(position == 0){
            informationBT.setSelected(true);
            commentsBT.setSelected(false);
        }else if(position == 1){
            informationBT.setSelected(false);
            commentsBT.setSelected(true);
        }
    }

    private class FragmentsVPA extends FragmentPagerAdapter {

        public FragmentsVPA(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Bundle bundle = new Bundle();
            bundle.putInt(MySQLColumns.EventID, mEventID);
            switch (position) {
                case 0:
                    EventInfoFragment eventInfoFragment = new EventInfoFragment();
                    eventInfoFragment.setArguments(bundle);
                    return eventInfoFragment;
                case 1:
                    EventCommentFragment eventCommentFragment = new EventCommentFragment();
                    eventCommentFragment.setArguments(bundle);
                    return eventCommentFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {}

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
}