package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Profile;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Profile.MyHobbiesActivity.MyHobbiesActivity;
import com.nhomolka.synchobby.model.SharedPreferencesUtils;

import static com.nhomolka.synchobby.model.SharedPreferencesUtils.PrefKeys.Distance_int;
import static com.nhomolka.synchobby.model.SharedPreferencesUtils.PrefKeys.MeasureSystem_str;
import static com.nhomolka.synchobby.model.SharedPreferencesUtils.PrefKeys.Name_str;
import static com.nhomolka.synchobby.model.SharedPreferencesUtils.PrefKeys.Username_str;

public class ProfileValueRow {

    private static final String TAG = "SharedPreferencesUtils";

    String name, value;
    int icon;
    Intent intent;
    Context mContext;

    public ProfileValueRow( Context context, String name){
        this.name = name;
        mContext = context;
        setIcon();
        setValue();
        setIntent();
    }

    public String getName() {
        return name;
    }

    public int getIcon(){return icon;}

    private void setIcon(){
        switch (name){
            case "Hobbies":
                icon = R.drawable.ic__profile_fragment__hobbies;
            return;
            case "Name":
                icon = R.drawable.ic__fragment_profile__name;
            return;
            case "Distance":
                icon = R.drawable.ic__profile_fragment__distance;
            return;
            case "Username":
                icon = R.drawable.ic__profile_fragment__username;
                return;
            case "Delete Account":
                icon = R.drawable.ic__profile_fragment__logout;
                return;
            case "Log Out":
                icon = R.drawable.ic__profile_fragment__logout;
            return;
        }
    }

    public String getValue(){return value;}

    private void setValue(){
        Log.i(TAG, "setValue: " + name);
        switch (name){
            case "Hobbies":
                value = "";
                return;
            case "Name":
                value = SharedPreferencesUtils.getInstance(mContext).getString(Name_str);
                return;
            case "Distance":
                String temp = "";
                Log.i(TAG, SharedPreferencesUtils.getInstance(mContext).getString(MeasureSystem_str));

                if(SharedPreferencesUtils.getInstance(mContext).getString(MeasureSystem_str).compareTo("mi") == 0){
                    temp = "Miles";
                }else{
                    temp = "Kilometers";
                }


                value = Integer.toString(SharedPreferencesUtils.getInstance(mContext).getInt(Distance_int)) + " " + temp;
                return;
            case "Username":
                value = SharedPreferencesUtils.getInstance(mContext).getString(Username_str);

                return;
            case "Delete Account":
                value = "";
                return;
            case "Log Out":
                value = "";
                return;
        }
    }

    public Intent getIntent(){return intent;}

    private void setIntent(){
        switch (name){
            case "Hobbies":
                intent = new Intent(mContext, MyHobbiesActivity.class);
                break;
            case "Name":
                intent = new Intent(mContext, EditNameActivity.class);
                break;
            case "Distance":
                intent = new Intent(mContext, EditDistanceActivity.class);
                break;
            case "Username":
                intent = new Intent(mContext, EditUsernameActivity.class);
                break;
            case "Delete Account":
                intent = new Intent(mContext, EditUsernameActivity.class);
                break;
            case "Log Out":
//                intent = new Intent(mContext, EditDistanceActivity.class);
                break;
        }
    }
}
