package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Profile;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Profile;
import com.facebook.ProfileManager;
import com.facebook.login.LoginManager;
import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.controller.Activities.TransactionLoginScreenActivity;
import com.nhomolka.synchobby.model.SQLite.Motherbase;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Appuser_deleteAccount;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Appuser_getImageVersion;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.FCMToken_removeTokenFromSystem;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Upload_profilePicture;
import com.nhomolka.synchobby.model.ServerRequest.ServerAPI;
import com.nhomolka.synchobby.model.ServerRequest.QueryForUpdatedSingleton;
import com.nhomolka.synchobby.model.Toasts;
import com.nhomolka.synchobby.model.UniversalImageLoader;
import com.nhomolka.synchobby.model.SharedPreferencesUtils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends Fragment {
    private static final String TAG = "##profileFragment";
    public static final int GET_FROM_GALLERY = 3;

    ProgressDialog uploadPictureProgressDialog;

    ImageButton ibtUploadPicture;
    CircleImageView vProfilePictureCIV;
    RecyclerView rvValuesRows;
    AdapterProfileInfo_RV mAdapterProfileInfo_rv;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        UniversalImageLoader universalImageLoader = new UniversalImageLoader(getContext());
        ImageLoader.getInstance().init(universalImageLoader.getConfig());
        view_build();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK ) {
                File file = new File(result.getUri().getPath());
                new UploadThePicture(file).execute();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode, @Nullable Bundle options) {
        super.startActivityForResult(intent, requestCode, options);

        Log.i(TAG, "startActivityForResult");
    }

    public void view_build() {

        view_btUploadPicture();
        view_civProfilePicture();
        view_rvValuesRows();
    }

    public void view_btUploadPicture() {
        ibtUploadPicture = getView().findViewById(R.id.ibtUploadPicture);

        ibtUploadPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "on click");
                final PopupMenu popupMenu = new PopupMenu(getContext(), ibtUploadPicture);
                popupMenu.getMenuInflater().inflate(R.menu.popup__upload_picture, popupMenu.getMenu());

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        Toast.makeText(getContext(), "" + item.getTitle(), Toast.LENGTH_SHORT).show();

                        Log.i(TAG, item.getTitle().toString());

                        switch (item.getTitle().toString()){
                            case "From Facebook":
                                Log.i(TAG, item.getTitle().toString());
                            break;
                            case "Upload a Picture":
                                CropImage.activity()
                                        .setGuidelines(CropImageView.Guidelines.ON)
                                        .setCropShape(CropImageView.CropShape.OVAL)
                                        .start(getActivity(), getFragment());
                            break;
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });
    }


    Fragment getFragment(){
        return this;
    }

    public void view_civProfilePicture() {
        vProfilePictureCIV = getView().findViewById(R.id.civProfilePicture);
        int appUserID = SharedPreferencesUtils.getInstance(getContext()).getInt(SharedPreferencesUtils.PrefKeys.AppUserID_int);
        int imageVersion = SharedPreferencesUtils.getInstance(getContext()).getInt(SharedPreferencesUtils.PrefKeys.ImageVersion_int);
        UniversalImageLoader.setImage(appUserID, imageVersion, vProfilePictureCIV);
    }

    public void view_rvValuesRows(){

        rvValuesRows = getView().findViewById(R.id.fragmentProfile_rvValuesRows);
        mAdapterProfileInfo_rv = new AdapterProfileInfo_RV(getContext());
        rvValuesRows.setAdapter(mAdapterProfileInfo_rv);
        rvValuesRows.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        for (int i = 0; i < permissions.length; i++) {
            Log.i(TAG, permissions[i] + " " + grantResults[i]);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        view_rvValuesRows();
    }


    void startUploadPictureProgressDialog(){
        uploadPictureProgressDialog = new ProgressDialog(getActivity());
//        progressDialog.setIndeterminate(true);
        uploadPictureProgressDialog.setMessage("Uploading");
        uploadPictureProgressDialog.show();
    }

    void endUploadPictureProgressDialog(){
        uploadPictureProgressDialog.dismiss();

    }


    class UploadThePicture extends AsyncTask<Void, Void, Integer>{

        File mFile;

        public UploadThePicture(File file) {
            mFile = file;
            startUploadPictureProgressDialog();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            new ServerAPI(getContext()).uploadPicturesBitmap(new Upload_profilePicture(mFile, Upload_profilePicture.UploadType.profileImage));

            try {
                JSONObject jsonObject = new ServerAPI(getContext()).serverRequestGET(new Appuser_getImageVersion(SharedPreferencesUtils.getInstance(getContext()).getInt(SharedPreferencesUtils.PrefKeys.AppUserID_int)));
                if(jsonObject.getString(APIRequest.message).compareTo(Appuser_getImageVersion.Response.foundImage) == 0){
                    return jsonObject.getInt(Appuser_getImageVersion.ResponseFields.ImageVersion);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer imageVersion) {
            if(imageVersion != null){
                UniversalImageLoader.setImage(SharedPreferencesUtils.getInstance(getContext()).getInt(SharedPreferencesUtils.PrefKeys.AppUserID_int), imageVersion, vProfilePictureCIV);
            }

            endUploadPictureProgressDialog();
        }
    }


    public class AdapterProfileInfo_RV extends RecyclerView.Adapter{

        private static final String TAG = "##RecyclerViewAdapterAd";

        String[] rows = new String[]{"Hobbies", "Name", "Username", "Distance", "Delete Account", "Log Out"};
        Context mContext;

        public AdapterProfileInfo_RV(Context context){
            mContext = context;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new rowHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_profile_info, parent, false));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ((rowHolder) holder).bind(rows[position], position == (rows.length - 1));
        }

        @Override
        public int getItemCount() {
            return rows.length;
        }

        class rowHolder extends RecyclerView.ViewHolder {
            RelativeLayout rlWholeLayout;
            TextView tvName, tvValue;
            ImageView ivIcon, ivArrow;
            View vBottomLine;

            public rowHolder(View itemView) {
                super(itemView);
                ivIcon = itemView.findViewById(R.id.adapterProfileInfo_ivIcon);
                tvName = itemView.findViewById(R.id.adapterProfileInfo_tvName);
                tvValue = itemView.findViewById(R.id.adapterProfileInfo_tvValue);
                ivArrow = itemView.findViewById(R.id.adapterProfileInfo_ivArrow);
                vBottomLine = itemView.findViewById(R.id.adapterProfileInfo_vBottomLine);
                rlWholeLayout = itemView.findViewById(R.id.adapterProfileInfo_rlWholeLayout);
            }

            void bind(String rowName, boolean lastRow) {

                final ProfileValueRow profileValueRow = new ProfileValueRow(mContext, rowName);

                tvName.setText(profileValueRow.getName());
                tvValue.setText(profileValueRow.getValue());
                ivIcon.setImageResource(profileValueRow.getIcon());

                rlWholeLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        switch (profileValueRow.getName()){
                            case "Delete Account":

                                new AlertDialog.Builder(getContext())
                                        .setTitle("Delete Account")
                                        .setMessage("Do you want to delete your account??")
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int whichButton) {
                                                new DeleteAccount_http().execute();
                                            }})
                                        .setNegativeButton(android.R.string.no, null).show();

                                break;
                            case "Log Out":
                                new Logout_http().execute();
                                break;

                            default:
                                startActivityForResult(profileValueRow.getIntent(), 987);
                        }
                    }
                });
                ivArrow = itemView.findViewById(R.id.adapterProfileInfo_ivArrow);

                if(lastRow){
                    vBottomLine.setVisibility(View.GONE);
                }
            }
        }

        private int get(){
            return R.drawable.com_facebook_tooltip_black_topnub;
        }

        class Logout_http extends AsyncTask<String, String, JSONObject> {
            @Override
            protected JSONObject doInBackground(String... strings) {

                return new ServerAPI(mContext).serverRequestPOST(new FCMToken_removeTokenFromSystem(mContext));
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {

                try {
                    if(Toasts.responseIsGoodToaster(jsonObject, getActivity())) {
                        Motherbase.getInstance(mContext).restartDatabase();
                        LoginManager.getInstance().logOut();

                        //Close Service
                        QueryForUpdatedSingleton.getInstance().endTask();
                        SharedPreferencesUtils.getInstance(mContext).clear();
                        Intent intent = new Intent(mContext, TransactionLoginScreenActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
                        mContext.startActivity(intent);
                        ((Activity)mContext).finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        class DeleteAccount_http extends AsyncTask<String, String, JSONObject> {
            @Override
            protected JSONObject doInBackground(String... strings) {
                new ServerAPI(mContext).serverRequestPOST(new FCMToken_removeTokenFromSystem(mContext));
                return new ServerAPI(mContext).serverRequestPOST(new Appuser_deleteAccount());
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                try {
                    if(Toasts.responseIsGoodToaster(jsonObject, getActivity())) {
                        try {
                            switch (jsonObject.getString(APIRequest.message)){
                                case Appuser_deleteAccount.Response.accountDeleted:
                                    Motherbase.getInstance(mContext).restartDatabase();
                                    LoginManager.getInstance().logOut();

                                    //Close Service
                                    QueryForUpdatedSingleton.getInstance().endTask();
                                    SharedPreferencesUtils.getInstance(mContext).clear();
                                    break;

                                default:

                                    break;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Intent intent = new Intent(mContext, TransactionLoginScreenActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
                        mContext.startActivity(intent);
                        ((Activity)mContext).finish();
                    }else{




                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }
    }
}
