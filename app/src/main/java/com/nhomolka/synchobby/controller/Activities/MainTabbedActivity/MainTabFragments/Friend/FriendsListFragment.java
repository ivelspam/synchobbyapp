package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Friend;

import android.content.Intent;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nhomolka.synchobby.R;

import com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Friend.FriendInfo.FriendActivity;
import com.nhomolka.synchobby.model.Constants.RequestCodes;
import com.nhomolka.synchobby.model.SQLite.Objects.Friend;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.UniversalImageLoader;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class FriendsListFragment extends Fragment{
    // TODO: Rename parameter arguments, choose names that match
    private static final String TAG = "##FriendsListFragment";

    RecyclerView vFriendListRV;

    FriendsRVA mFriendsRVA;
    List<Friend> mFriends = new ArrayList<>();

    Friend.FriendState mState;
    public FriendsListFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_friends_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        build_view();
    }

    void build_view(){
        vFriendListRV = getView().findViewById(R.id.fragmentFriendsList_friendsRV);


        //set adapters
        mFriendsRVA = new FriendsRVA();
        vFriendListRV.setAdapter(mFriendsRVA);
        vFriendListRV.setLayoutManager(new GridLayoutManager(getContext(), 4));

    }

//    public void makeChange(final List<Friend> friends){
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//
//                for(Friend friend : friends){
//
//                    for( int i = 0; i< mFrie; i++){
//
//                        final int temp = i;
//
//                        //Remove
//                        if(friends.get(i).getState() != mState && mFriends.containsKey(friend.getAppUserID())){
//                            mFriends.remove(friend.getAppUserID());
//
//                            getActivity().runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//
//                                }
//                            });
//                        }
//
//                        //Add
//                        else if(friend.getState() == mState && !mFriends.containsKey(friend.getAppUserID())){
//                            mFriends.put(friend.getAppUserID(), friend);
//                        }
//
//
//
//                    }
//
//                }
//
//
//
//
//
//            }
//        }).start();
//        //Remove
//
//    }


    @Override
    public void setArguments(@Nullable Bundle args) {
        super.setArguments(args);

        mState = Friend.FriendState.valueOf(getArguments().getInt(MySQLColumns.State, -1));
    }


    public void remove(int appUserID) {

        Log.i(TAG, "REMOVE: " + mState.getValue());

        for(int i = 0; i < mFriends.size(); i++){

            if(mFriends.get(i).getAppUserID() == appUserID){
                mFriends.remove(i);
                mFriendsRVA.notifyItemRemoved(appUserID);
                break;
            }
        }
    }


    public void insertOrRemoveFriends(List<Friend> friends){
        int i1 = 0;
        int i2 = 0;

        if(friends.size() != 0){
            while(i2 < friends.size()){
                Friend friend = friends.get(i2);
                if((mFriends.size() - 1) == (i1 - 1)) {
                    if(friend.getState() == mState){
                        mFriends.add(friend);
                        if(mFriendsRVA != null){
                            mFriendsRVA.notifyItemInserted(mFriends.size() - 1);
                        }
                    }
                    i2++;
                }else if(mFriends.get(i1).getAppUserID() == friend.getAppUserID()){
                    if(friend.getState() != mState) {
                        mFriends.remove(i1);
                        if(mFriendsRVA != null){mFriendsRVA.notifyItemRemoved(i1);}
                    }
                    i2++;
                }else if(friend.getAppUserID() < mFriends.get(i1).getAppUserID()){
                    if(friend.getState() == mState){
                        mFriends.add(i1 + 1, friend);
                        if(mFriendsRVA != null){
                            mFriendsRVA.notifyItemInserted(i1 + 1);
                        }
                    }
                    i2++;
                }else{
                    i1++;
                }
            }
        }
    }

    private class FriendsRVA extends RecyclerView.Adapter {

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.adapter_friend_circle, parent, false);
            MyViewHolder holder = new MyViewHolder(view);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            Friend friend = mFriends.get(position);
            ((MyViewHolder) holder).bind(friend);
        }

        @Override
        public int getItemCount() {
            return mFriends.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            TextView friendName;
            CircleImageView circleImageView;
            TextView tvUnreadMessages;

            public MyViewHolder(View itemView) {
                super(itemView);
                friendName = itemView.findViewById(R.id.adapterAutofitFriend_friendName);
                circleImageView = itemView.findViewById(R.id.adapterAutofitFriend_civFriend);
                tvUnreadMessages = itemView.findViewById(R.id.adapterAutofitFriend_tvUnreadMessages);
            }

            void bind(final Friend friend) {
                friendName.setText(friend.getName());
                circleImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent;
                        int requestCode = 0;

                        switch (mState) {
                            case Confirmed :
                                intent = new Intent(getContext(), FriendActivity.class);
                                requestCode = RequestCodes.ConfirmedFriend;
                                break;
                            case ReceivedRequest:
                                intent = new Intent(getContext(), AcceptFriendRequestActivity.class);
                                requestCode = RequestCodes.AcceptFriendRequest;
                                break;
                            case SentRequest:
                                intent = new Intent(getContext(), SentFriendRequestActivity.class);
                                requestCode = RequestCodes.FriendSentRequest;
                                break;
                            default:
                                Log.i(TAG, "SWITCH CASE INVALID");
                                return;
                        }


                        intent.putExtra(MySQLColumns.AppUserID, friend.getAppUserID());
                        intent.putExtra(MySQLColumns.ImageVersion, friend.getImageVersion());
                        getParentFragment().startActivityForResult(intent, requestCode);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                tvUnreadMessages.setText("0");
                                tvUnreadMessages.setVisibility(View.GONE);
                            }
                        }, 200);
                    }
                });
                UniversalImageLoader.setImage(friend.getAppUserID(), friend.getImageVersion(), circleImageView );
            }
        }
    }

}