package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Friend;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.BaseActivity;
import com.nhomolka.synchobby.model.SQLite.Motherbase;
import com.nhomolka.synchobby.model.SQLite.Objects.Friend;
import com.nhomolka.synchobby.model.SQLite.Tables.FriendTable;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Friend_accept;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Friend_cancel;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.ServerRequest.ServerAPI;
import com.nhomolka.synchobby.model.UniversalImageLoader;
import com.nhomolka.synchobby.model.Toasts;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class AcceptFriendRequestActivity extends BaseActivity implements View.OnClickListener {
    private static final String TAG = "##PendingRequestActivit";

    CircleImageView vFriendCIV;
    TextView vNameTV, vConfirmationTV;
    Button vCancelBT, vAcceptBT;
    int mAppUserID, mImageVersion;
    Friend mFriend;
    FriendTable friendTable = Motherbase.getInstance(getBaseContext()).getFriendTable();
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept_friend_request);
        Bundle extras = getIntent().getExtras();
        mAppUserID = extras.getInt(MySQLColumns.AppUserID);
        mImageVersion = extras.getInt(MySQLColumns.ImageVersion);

        mFriend = friendTable.selectFriendByAppUserID(mAppUserID);

        build_view();
    }

    void build_view(){
        vFriendCIV = findViewById(R.id.activityAcceptFriendRequest_friendCIV);
        vCancelBT = findViewById(R.id.activityAcceptFriendRequest_cancelBT);
        vAcceptBT = findViewById(R.id.activityAcceptFriendRequest_acceptBT);
        vNameTV = findViewById(R.id.activityAcceptFriendRequest_nameTV);
        vConfirmationTV = findViewById(R.id.activityAcceptFriendRequest_confirmationTV);

        //SET LISTENERS
        vCancelBT.setOnClickListener(this);
        vAcceptBT.setOnClickListener(this);

        //SET INITIAL VALUES
        UniversalImageLoader.setImage( mAppUserID, mImageVersion, vFriendCIV);
        vNameTV.setText(mFriend.getName());

    }

    @Override
    public void onClick(View view) {

        if(vCancelBT == view){
            new CancelRequest_http().execute();
        }else if(vAcceptBT == view){
            new AcceptRequest_http().execute();
        }
    }



    class CancelRequest_http extends AsyncTask<Void, Void, JSONObject>{
        @Override
        protected JSONObject doInBackground(Void... voids) {
            return new ServerAPI(getBaseContext()).serverRequestPOST(new Friend_cancel(mAppUserID));
        }

        @Override
        protected void onPostExecute(JSONObject jsonObjectResult) {
            try {

                if(Toasts.responseIsGoodToaster(jsonObjectResult, getBaseContext())){
                    switch (jsonObjectResult.getString(APIRequest.message)) {
                        case Friend_cancel.Response.canceled :
                            friendTable.deleteFriend(mAppUserID);
                            vCancelBT.setVisibility(View.GONE);
                            vAcceptBT.setVisibility(View.GONE);
                            vConfirmationTV.setText("Canceled");
                            break;
                        default :
                            break;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    class AcceptRequest_http extends AsyncTask<Void, Void, JSONObject>{
        @Override
        protected JSONObject doInBackground(Void... voids) {
            return new ServerAPI(getBaseContext()).serverRequestPOST(new Friend_accept(mAppUserID));
        }

        @Override
        protected void onPostExecute(JSONObject jsonObjectResult) {
            try {
                if(Toasts.responseIsGoodToaster(jsonObjectResult, getBaseContext())) {
                    switch ( jsonObjectResult.getString(APIRequest.message)) {
                        case Friend_accept.Response.accepted :
                            friendTable.confirmFriend(mAppUserID);
                            vCancelBT.setVisibility(View.GONE);
                            vAcceptBT.setVisibility(View.GONE);
                            vConfirmationTV.setText("Accepted");
                            break;
                        case Friend_accept.Response.notPossible:
                            vCancelBT.setVisibility(View.GONE);
                            vAcceptBT.setVisibility(View.GONE);
                            vConfirmationTV.setText("Request Was Removed Or User Does Not Exists");
                            break;
                        default :
                            break;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}
