package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Profile.MyHobbiesActivity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.controller.Interfaces.MyHobbiesInterface;
import com.nhomolka.synchobby.model.Extensions.ExtensionsHack;
import com.nhomolka.synchobby.model.SQLite.Objects.Hobby;
import com.nhomolka.synchobby.model.SQLite.Tables.AppUserHobbyTable;
import com.nhomolka.synchobby.model.SQLite.Motherbase;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AllHobbiesListFragment extends Fragment implements TextWatcher {
    private static final String TAG = "##AllHobbiesListFrag";

    EditText vFilterET;
    RecyclerView listRV;
    HobbyRowRVA mHobbyRowRVA = new HobbyRowRVA();

    List<Hobby> mHobbies = new ArrayList<>();
    List<Hobby> mFiltered = new ArrayList<>();
    MyHobbiesInterface mMyHobbiesInterface;
    AppUserHobbyTable mAppUserHobbyTable = Motherbase.getInstance(getContext()).getAppUserHobby();

    public AllHobbiesListFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view_build();
        Log.i(TAG, "onViewCreated");
        updateTableValues();
    }

    void view_build(){

        Log.i(TAG, "view_build");
        vFilterET = getView().findViewById(R.id.fragmentAllHobbiesList_filterET);
        listRV = getView().findViewById(R.id.fragmentAllHobbiesList_listRV);

        //Listeners
        vFilterET.addTextChangedListener(this);

        //Adapters And LayoutManager
        listRV.setAdapter(mHobbyRowRVA);
        listRV.setLayoutManager(new LinearLayoutManager(getContext()));
        updateTableValues();

    }

    public void customInit( MyHobbiesInterface myHobbiesInterface ){
        mMyHobbiesInterface = myHobbiesInterface;
    }

    public void updateItemInList(Hobby hobbyID, boolean inHobby){

        for(int i = 0; i< mHobbies.size(); i++){
            if(mHobbies.get(i).getHobbyID() == hobbyID.getHobbyID()){
                mHobbies.get(i).setInHobby(inHobby);
            }
        }

        filter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_all_hobbies_list, container, false);
    }

    public void updateTableValues(){
        mHobbies = mAppUserHobbyTable.selectHobbiesWithInfo();
        mFiltered.clear();
        mFiltered.addAll(mHobbies);
        mHobbyRowRVA.notifyDataSetChanged();
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        filter();
    }

    public void filter(){
        String charText = vFilterET.getText().toString().toLowerCase(Locale.getDefault());
        mFiltered.clear();
        if(charText.length() == 0){
            mFiltered.addAll(mHobbies);
        }else{
            for(Hobby hobby : mHobbies){
                if(hobby.getName().toLowerCase(Locale.getDefault()).contains(charText)){
                    mFiltered.add(hobby);
                }
            }
        }

        mHobbyRowRVA.notifyDataSetChanged();
    }

    public class HobbyRowRVA extends RecyclerView.Adapter{

        private static final String TAG = "##RegisterHobbySwitch";

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            Log.i(TAG, "onCreateViewHolder");
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_row_hobbies_child, parent, false);
                return new MyViewHolderChild(view);
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            ((MyViewHolderChild) holder).bind(mFiltered.get(position));
        }

        @Override
        public int getItemCount() {
            return mFiltered.size();
        }

        class MyViewHolderChild extends RecyclerView.ViewHolder {
            TextView nameTV;
            RegisterHobbySwitch registerSW;

            public MyViewHolderChild(View itemView) {
                super(itemView);
                registerSW = itemView.findViewById(R.id.addHobby_registerSW);
                nameTV = itemView.findViewById(R.id.addHobby_tvName);
            }

            void bind(final Hobby hobby) {
                nameTV.setText(ExtensionsHack.StringToTitleCase(hobby.getName()));
                registerSW.customInit(hobby, mMyHobbiesInterface, "list");
                nameTV.setOnClickListener(null);

            }
        }

    }

    @Override
    public void afterTextChanged(Editable editable) { }
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
}
