package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Events;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.nhomolka.synchobby.controller.Interfaces.UpdateEventsFragmentInterface;
import com.nhomolka.synchobby.model.Constants.RequestCodes;
import com.nhomolka.synchobby.model.SQLite.Objects.Event;

import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.ServerRequest.QueryForUpdatedSingleton;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.AllInfo_getUpdates;
import com.nhomolka.synchobby.model.SharedPreferencesUtils;
import com.nhomolka.synchobby.model.Toasts;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.nhomolka.synchobby.model.Constants.RequestCodes.InEvent;


public class EventsFragment extends Fragment implements UpdateEventsFragmentInterface, ViewPager.OnPageChangeListener, View.OnClickListener {
    private static final String TAG = "##EventsFragment";

    //View view;
    ViewPager vViewPagerVP;
    Button vCurrentEventsBT, vPastEventsBT, vCreateButtonBT;
    EventsListFragment currentEventsFragment, pastEventsFragment;
    EventsFragmentBS eventsFragmentBS;
    EventsFragmentPagerAdapter mEventsFragmentPagerAdapter;
    SwipeRefreshLayout vSwipeRefreshLayout;

    UpdateEventsFragmentInterface updateEventsFragmentInterface;

    boolean isBound = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        updateEventsFragmentInterface = this;
        return inflater.inflate(R.layout.fragment_events, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle currentArgs = new Bundle();
        currentArgs.putInt(MySQLColumns.State, Event.EventState.Created.getValue());
        currentEventsFragment = new EventsListFragment();
        currentEventsFragment.setArguments(currentArgs);

        Bundle pastArgs = new Bundle();
        pastArgs.putInt(MySQLColumns.State, Event.EventState.Ended.getValue());
        pastEventsFragment = new EventsListFragment();
        pastEventsFragment.setArguments(pastArgs);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        view_build();
    }

    void view_build() {
        vViewPagerVP = getView().findViewById(R.id.fragmentEvents_viewPagerVP);
        vCreateButtonBT = getView().findViewById(R.id.fragmentEvents_createEventBT);
        vCurrentEventsBT = getView().findViewById(R.id.fragmentEvents_currentEventsBT);
        vPastEventsBT = getView().findViewById(R.id.fragmentEvents_pastEventsBT);
        vSwipeRefreshLayout =  getView().findViewById(R.id.fragmentEvents_swipeRefreshLayout);

        //Set Listeners
        vViewPagerVP.addOnPageChangeListener(this);
        vCurrentEventsBT.setOnClickListener(this);
        vPastEventsBT.setOnClickListener(this);
        vCreateButtonBT.setOnClickListener(this);

        //Listeners
        vSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                vSwipeRefreshLayout.setRefreshing(false);
                new getEventUpdates().execute();
            }
        });


        //Set Adapters
        mEventsFragmentPagerAdapter = new EventsFragmentPagerAdapter(getChildFragmentManager());
        vViewPagerVP.setAdapter(mEventsFragmentPagerAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        startService();
    }

    @Override
    public void onStop(){
        super.onStop();
        endService();
    }

    void startService(){
        Intent intent = new Intent(getActivity(), EventsFragmentBS.class);
        getActivity().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    void endService(){
        getActivity().unbindService(mConnection);
        isBound = false;
    }

    @Override
    public void onClick(View view) {
        if(view == vCurrentEventsBT){
            vCurrentEventsBT.setSelected(true);
            vPastEventsBT.setSelected(false);
            vViewPagerVP.setCurrentItem(0, false);
        }else if(view == vPastEventsBT){
            vCurrentEventsBT.setSelected(false);
            vPastEventsBT.setSelected(true);
            vViewPagerVP.setCurrentItem(1, false);
        }else if (view == vCreateButtonBT){
            final int value = SharedPreferencesUtils.getInstance(getContext()).getInt(SharedPreferencesUtils.PrefKeys.EventID_int, -1);
            if(SharedPreferencesUtils.getInstance(getContext()).getInt(SharedPreferencesUtils.PrefKeys.EventID_int, -1) == -1){
                Intent intent = new Intent(getContext(), AddEventActivity.class);
                startActivityForResult(intent, RequestCodes.CreateEvent);
            }else{
                Intent intent = new Intent(getContext(), InEventMoreInfoActivity.class);
                intent.putExtra(MySQLColumns.EventID, value);
                startActivityForResult(intent, InEvent);
            }
        }else{
            Log.i(TAG, "Not Suppose to be here");
        }
    }



    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            eventsFragmentBS = ((EventsFragmentBS.LocalBinder)service).getService();
            eventsFragmentBS.setInterface(updateEventsFragmentInterface);
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.i(TAG, "onServiceDisconnected");
        }
    };

    @Override
    public void setUpEvents(List<Event> events) {
        currentEventsFragment.insertOrRemoveOrUpdateEvent(events);
        pastEventsFragment.insertOrRemoveOrUpdateEvent(events);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        if(position == 0){
            vCurrentEventsBT.setSelected(true);
            vPastEventsBT.setSelected(false);
        }else if(position == 1){
            vCurrentEventsBT.setSelected(false);
            vPastEventsBT.setSelected(true);
        }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private class EventsFragmentPagerAdapter extends FragmentPagerAdapter {

        public EventsFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0 :
                    return currentEventsFragment;
                case 1 :
                    return pastEventsFragment;
                default:
                    Log.i(TAG, "ERROR: Wrong View Page");
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    class getEventUpdates extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(Void... voids) {
            return QueryForUpdatedSingleton.getUpdates(getActivity());
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {

            Log.i(TAG, jsonObject.toString());
            try {
                if(Toasts.responseIsGoodToaster(jsonObject, getActivity())) {
                    switch (jsonObject.getString(APIRequest.message)){
                        case AllInfo_getUpdates.Response.foundUpdates:
                            eventsFragmentBS.getUpdates();
                        default:
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}