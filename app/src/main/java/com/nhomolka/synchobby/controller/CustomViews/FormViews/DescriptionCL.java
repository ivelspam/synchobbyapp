package com.nhomolka.synchobby.controller.CustomViews.FormViews;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;

public class DescriptionCL extends ConstraintLayout {


    ErrorAndTitlesTextViews errorAndTitlesTextViews;
    EditText owner;

    public DescriptionCL(Context context) {
        super(context);
    }

    public DescriptionCL(Context context, AttributeSet attrs) {
        super(context, attrs);
        owner = new EditText(getContext());
        owner.setId(View.generateViewId());
        this.addView(owner);
        errorAndTitlesTextViews = new ErrorAndTitlesTextViews(getContext(), this, owner, "Event Info");
        errorAndTitlesTextViews.hideError();
    }

    public boolean isGood(){
        return true;
    }

    public String getValue(){
        return owner.getText().toString();
    }
}
