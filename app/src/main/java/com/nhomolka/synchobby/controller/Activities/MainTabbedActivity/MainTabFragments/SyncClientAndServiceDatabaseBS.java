package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.nhomolka.synchobby.model.Constants.Timers;

public class SyncClientAndServiceDatabaseBS extends Service {
    private static final String TAG = "##SyncClientAndServiceD";

    private final IBinder localBinder = new LocalBinder();
    Handler syncClientAndServer = new Handler();

    Runnable shouldEventsUpdateRunnable = new Runnable() {
        @Override
        public void run() {
            Log.i(TAG, "syncClientAndServer");
            getUpdates();
            syncClientAndServer.postDelayed(this, 1000);
        }
    };

    void getUpdates(){
        new GetUpdatesAsync().execute();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return localBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        syncClientAndServer.postDelayed(shouldEventsUpdateRunnable, 0);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        syncClientAndServer.removeCallbacks(shouldEventsUpdateRunnable);
        return super.onUnbind(intent);
    }

    public class LocalBinder extends Binder {
        public SyncClientAndServiceDatabaseBS getService(){
            return SyncClientAndServiceDatabaseBS.this;
        }
    }

    class GetUpdatesAsync extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... params) {
            Log.i(TAG, "THIS IS RUNNING");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

        }
    }
}
