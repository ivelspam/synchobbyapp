package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Friend;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.nhomolka.synchobby.controller.Interfaces.UpdateFriendsFragmentInferface;
import com.nhomolka.synchobby.model.Constants.MagicNumbers;
import com.nhomolka.synchobby.model.Constants.Timers;
import com.nhomolka.synchobby.model.Extensions.DateExtension;
import com.nhomolka.synchobby.model.SQLite.Objects.Friend;
import com.nhomolka.synchobby.model.SQLite.Motherbase;

import java.util.ArrayList;
import java.util.List;

public class FriendListFragmentBS extends Service{
    private static final String TAG = "##FriendListFragmentBS";

    private final IBinder localBinder = new LocalBinder();
    List<Friend> mFriends = new ArrayList<>();

    Handler shouldFriendsUpdateHandler = new Handler();
    String mClientUpdatedTS = MagicNumbers.LOWEST_TS;

    UpdateFriendsFragmentInferface updateFriendsFragmentInferface;

    Runnable shouldFriendsUpdateRunnable = new Runnable() {
        @Override
        public void run() {
            getUpdates();
            shouldFriendsUpdateHandler.postDelayed(this, Timers.UPDATE_FRIENDS_FRAGMENT);
        }
    };

    public void getUpdates(){
        new GetUpdatesAsync().execute();
    }

    public void reload(){
        mClientUpdatedTS = MagicNumbers.LOWEST_TS;
        getUpdates();
    }

    public void customInit(UpdateFriendsFragmentInferface updateFriendsFragmentInferface){
        this.updateFriendsFragmentInferface = updateFriendsFragmentInferface;
        shouldFriendsUpdateHandler.postDelayed(shouldFriendsUpdateRunnable, 100);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return localBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        shouldFriendsUpdateHandler.removeCallbacks(shouldFriendsUpdateRunnable);
        return super.onUnbind(intent);
    }

    public class LocalBinder extends Binder {
        public FriendListFragmentBS getService(){
            return FriendListFragmentBS.this;
        }
    }

    class GetUpdatesAsync extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... params) {
            mFriends = Motherbase.getInstance(getBaseContext()).getFriendTable().selectFriendsByGreaterThanClientUpdatedTS(mClientUpdatedTS);
            updatemUpdateTS();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            updateFriendsFragmentInferface.updateList(mFriends);
            if(mFriends.size() > 0){
                updateFriendsFragmentInferface.updateListView();
            }
        }
    }

    void updatemUpdateTS(){
        for(Friend friend : mFriends){
            if(DateExtension.CalendarCompareTwoSQLiteTSFirstBiggerThanSecond(friend.getClientUpdatedTS(), mClientUpdatedTS)){
                mClientUpdatedTS = friend.getClientUpdatedTS();
            }
        }
    }
    public List<Friend> getFriends(){
        return mFriends;
    }
}
