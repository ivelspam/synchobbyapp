package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Profile.MyHobbiesActivity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.BaseActivity;
import com.nhomolka.synchobby.model.Extensions.ExtensionsHack;
import com.nhomolka.synchobby.model.SQLite.Objects.Hobby;
import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.model.SQLite.Tables.HobbyTable;
import com.nhomolka.synchobby.model.SQLite.Motherbase;

import java.util.ArrayList;
import java.util.List;

public class AddHobbyActivity extends BaseActivity implements TextWatcher, View.OnClickListener {
    private static final String TAG = "##AddHobbyActivity";
    final int typeParent = 0;
    final int typeChild = 1;

    RecyclerView vHobbiesRV;
    EditText vFilterTV;
    Button vReturnBT;

    Hobby mCurrentParent;
    List<Hobby> mHobbies = new ArrayList<>();
    HobbyTable hobbyTable = Motherbase.getInstance(getBaseContext()).getHobbyTable();

    HobbyRowRVA mHobbyRowRVA = new HobbyRowRVA();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_hobby);
        view_build();
    }

    //build views
    public void view_build(){

        vHobbiesRV = findViewById(R.id.activityAddHobby_hobbiesRV);
        vFilterTV = findViewById(R.id.activityAddHobby_filterTV);
        vFilterTV = findViewById(R.id.activityAddHobby_filterTV);
        vReturnBT = findViewById(R.id.activityAddHobby_returnBT);

        //Listeners
        vFilterTV.addTextChangedListener(this);
        vReturnBT.setOnClickListener(this);

        //set adapters
        vHobbiesRV.setAdapter(mHobbyRowRVA);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        vHobbiesRV.setLayoutManager(linearLayoutManager);

        loadNewParent(mCurrentParent);
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
        if(charSequence.toString().length() > 0){
            mHobbies = hobbyTable.selectHobbyByToken(charSequence.toString());
            mHobbyRowRVA.notifyDataSetChanged();
        }else{
            loadNewParent(mCurrentParent);
        }
    }

    @Override
    public void onClick(View view) {
        if(view == vReturnBT){

            Log.i(TAG, mCurrentParent.toString());
            loadNewParent(hobbyTable.selectHobbyByID(mCurrentParent.getParentID()));
        }
    }

    public class HobbyRowRVA extends RecyclerView.Adapter{

        private static final String TAG = "##RegisterHobbySwitch";

        @Override
        public int getItemViewType(int position) {
            if (mHobbies.get(position).getType().compareTo(Hobby.parent) == 0) {
                return typeParent;
            } else if (mHobbies.get(position).getType().compareTo(Hobby.child) == 0){
                return typeChild;
            }else{
                Log.i(TAG, "ERROR: FOUND NOT TYPE");
                return -1;
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            Log.i(TAG, "onCreateViewHolder");
            if(viewType == typeParent){
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_row_hobbies_parent, parent, false);
                return new MyViewHolderParent(view);
            }else {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_row_hobbies_child, parent, false);
                return new MyViewHolderChild(view);
            }
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            Log.i(TAG, "onBindViewHolder");
            if(holder instanceof MyViewHolderParent){
                ((MyViewHolderParent) holder).bind(mHobbies.get(position));
            }else {
                ((MyViewHolderChild) holder).bind(mHobbies.get(position));
            }
        }

        @Override
        public int getItemCount() {
            return mHobbies.size();
        }

        class MyViewHolderParent extends RecyclerView.ViewHolder {
            Button itemBT;

            public MyViewHolderParent(View itemView) {
                super(itemView);
                itemBT = itemView.findViewById(R.id.adapterRowHobbiesParent_itemBT);
            }

            void bind(final Hobby hobby) {
                itemBT.setText(ExtensionsHack.StringToTitleCase(hobby.getName()));

                itemBT.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadNewParent(hobby);
                    }
                });
            }
        }

        class MyViewHolderChild extends RecyclerView.ViewHolder {
            TextView nameTV;
            RegisterHobbySwitch registerSW;

            public MyViewHolderChild(View itemView) {
                super(itemView);
                registerSW = itemView.findViewById(R.id.addHobby_registerSW);
                nameTV = itemView.findViewById(R.id.addHobby_tvName);
            }

            void bind(final Hobby hobby) {
                nameTV.setText(ExtensionsHack.StringToTitleCase(hobby.getName()));

                Log.i(TAG, hobby.toString());
                registerSW.customInit(hobby, null, null);
                registerSW.setVisibility(View.VISIBLE);
                nameTV.setOnClickListener(null);

            }
        }

    }


    void loadNewParent(Hobby hobby){

        Log.i(TAG, "void loadNewParent(Hobby hobby)");

        int hobbyID = -1;

        if( hobby != null){
            mCurrentParent = hobby;
            hobbyID = mCurrentParent.getHobbyID();
        }

        mHobbies = hobbyTable.selectChildrenOFHobbyJoined(hobbyID);

        vFilterTV.removeTextChangedListener(this);
        vFilterTV.setText("");
        vFilterTV.addTextChangedListener(this);
        mHobbyRowRVA.notifyDataSetChanged();

        if(hobbyID == -1){
            vReturnBT.setEnabled(false);
            vReturnBT.setText("Categories");
            mCurrentParent = null;

        }else{
            vReturnBT.setEnabled(true);
            vReturnBT.setText(mCurrentParent.getName());
        }
    }

    public void onBackPressed(){
        Intent resultIntent = new Intent();
        setResult(RESULT_OK, resultIntent);
        finish();
        super.onBackPressed();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}


    @Override
    public void afterTextChanged(Editable editable) {

    }
}
