package com.nhomolka.synchobby.controller.Interfaces;

import com.nhomolka.synchobby.model.SQLite.Objects.Friend;

import java.util.List;

public interface UpdateFriendsFragmentInferface {

    void updateList(List<Friend> friends);
    void updateListView();

}
