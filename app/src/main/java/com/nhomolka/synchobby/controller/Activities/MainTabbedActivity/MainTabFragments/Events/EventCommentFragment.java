package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Events;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Ack;
import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.model.Constants.CalendarFormats;
import com.nhomolka.synchobby.model.Constants.SocketIOConnections;
import com.nhomolka.synchobby.model.Extensions.DateExtension;
import com.nhomolka.synchobby.model.SQLite.Objects.EventComment;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Eventcomment_getEventComments;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.ServerRequest.ServerAPI;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.nhomolka.synchobby.model.Toasts;
import com.nhomolka.synchobby.model.UniversalImageLoader;
import com.nhomolka.synchobby.model.SharedPreferencesUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class EventCommentFragment extends Fragment implements View.OnClickListener{

    private static final String TAG = "##EventCommentFragment";
    RecyclerView mEventCommentRV;
    EditText messageET;
    Button sendBT;
    int mEventID = -1;
    List<EventComment> mEventComments = new ArrayList<>();
    EventCommentRVA mEventCommentRVA;

    public EventCommentFragment() {}

    private Socket mSocket;

    {
        IO.Options options = new IO.Options();
        options.query = "token=" + SharedPreferencesUtils.getInstance(getContext()).getString(SharedPreferencesUtils.PrefKeys.Token_str);
        try {
            mSocket = IO.socket("https://synchobby.com", options);
        } catch (URISyntaxException e) {}
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEventID = getArguments().getInt(MySQLColumns.EventID);
        mEventCommentRVA = new EventCommentRVA();
        mSocket.connect();
        mSocket.on(SocketIOConnections.clientReceiveEventMessage(mEventID), handleIncomeMessages);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_event_comment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        build_view();
    }

    void build_view(){

        //connections
        mEventCommentRV = getView().findViewById(R.id.fragmentEventComment_eventCommentRV);
        messageET = getView().findViewById(R.id.fragmentEventComment_messageET);
        sendBT = getView().findViewById(R.id.fragmentEventComment_sendBT);

        //Listeners
        sendBT.setOnClickListener(this);


        //adapters
        mEventCommentRV.setAdapter(mEventCommentRVA);
        mEventCommentRV.setLayoutManager(new LinearLayoutManager(getContext()));

        new GetEventMessages().execute();

    }

    private Emitter.Listener handleIncomeMessages = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    JSONObject jsonObject = (JSONObject) args[0];
                    String message;

                    Log.i(TAG, jsonObject.toString());
                    try {
                        EventComment eventComment = new EventComment(jsonObject);
                        mEventComments.add(eventComment);
                        mEventCommentRV.scrollToPosition(mEventCommentRVA.getItemCount() - 1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    @Override
    public void onClick(View view) {
        if(view.getId() == sendBT.getId()){
            Log.i(TAG, "sendMessage");

            int appUserID = SharedPreferencesUtils.getInstance(getContext()).getInt(SharedPreferencesUtils.PrefKeys.AppUserID_int);
            int imageVersion = SharedPreferencesUtils.getInstance(getContext()).getInt(SharedPreferencesUtils.PrefKeys.ImageVersion_int);
            String name = SharedPreferencesUtils.getInstance(getContext()).getString(SharedPreferencesUtils.PrefKeys.Name_str);

            final EventComment eventComment = new EventComment(mEventID, messageET.getText().toString(), appUserID, imageVersion, name);

            mEventComments.add(eventComment);
            mEventCommentRVA.notifyItemInserted(mEventComments.size() -1);
            mEventCommentRV.scrollToPosition(mEventCommentRVA.getItemCount() - 1);

            mSocket.emit(SocketIOConnections.serverReceiveEventComment, eventComment.getJSONString(), new Ack() {
                @Override
                public void call(Object... args) {

                }
            });
        }
    }

    class GetEventMessages extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(Void... params) {
            return new ServerAPI(getContext()).serverRequestGET(new Eventcomment_getEventComments(mEventID));
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {

            try {
                if(Toasts.responseIsGoodToaster(jsonObject, getContext())) {

                    String message = jsonObject.getString(APIRequest.message);
                    if (message.compareTo(Eventcomment_getEventComments.Response.foundComments) == 0) {
                        JSONArray jsonArray = jsonObject.getJSONArray("comments");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            EventComment EventComment = new EventComment(jsonArray.getJSONObject(i));
                            mEventComments.add(EventComment);

                        }
                        mEventCommentRVA.notifyDataSetChanged();
                        mEventCommentRV.scrollToPosition(mEventCommentRVA.getItemCount() - 1);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public class EventCommentRVA extends RecyclerView.Adapter{
        private static final String TAG = "##ActivitiyRunningComm";

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_event_message, parent, false);
            return new EventMessageHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            EventComment EventComment = mEventComments.get(position);
            ((EventMessageHolder) holder).bind(EventComment);
        }

        @Override
        public int getItemCount() {
            return mEventComments.size();
        }

        private class EventMessageHolder extends RecyclerView.ViewHolder {
            TextView appUserName, message, timeCommentTV;
            CircleImageView imageCIV;
            EventComment hEventComment;

            public EventMessageHolder(View itemView) {
                super(itemView);
                appUserName = itemView.findViewById(R.id.adapterActivityMessage_appUserNameTV);
                message = itemView.findViewById(R.id.adapterActivityMessage_messageTV);
                imageCIV = itemView.findViewById(R.id.adapterActivityMessage_appUserImageTV);
                timeCommentTV = itemView.findViewById(R.id.adapterActivityMessage_timeCommentTV);
            }

            void bind(final EventComment eventComment) {
                hEventComment = eventComment;
                appUserName.setText(eventComment.getName());
                message.setText(eventComment.getComment());
                timeCommentTV.setText(DateExtension.CalendarGetFormated(eventComment.getCreatedTS(), CalendarFormats.shortFormat));

                UniversalImageLoader.setImage(eventComment.getAppUserID(),eventComment.getImageVersion(), imageCIV);
            }
        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
