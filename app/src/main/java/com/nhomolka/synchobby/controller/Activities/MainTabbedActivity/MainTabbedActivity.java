package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.BaseActivity;
import com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Events.EventsFragment;
import com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Friend.FriendsFragment;
import com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Profile.ProfileFragment;
import com.nhomolka.synchobby.model.PermissionsUtils;
import com.nhomolka.synchobby.model.SharedPreferencesUtils;

import java.util.ArrayList;
import java.util.List;

public class MainTabbedActivity extends BaseActivity {
    private static final String TAG = "##MainTabbedActivity";
    private static final int ADDRUNNINGACTIVITYCODE = 1000;

    private ViewPager vpPages;
    private SectionsPageAdapter mSectionsPageAdapter;
    Fragment profileFragment, friendsFragment;
    EventsFragment eventsFragment;
    String token;
    LocationManager mLocationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main_tabbed);
        token = SharedPreferencesUtils.getInstance(this).getString(SharedPreferencesUtils.PrefKeys.Token_str);
        mSectionsPageAdapter = new SectionsPageAdapter( getSupportFragmentManager());
        PermissionsUtils.requestLocationPermission(this, 10);
        build_locationManager();
        profileFragment = new ProfileFragment();
        eventsFragment = new EventsFragment();
        friendsFragment = new FriendsFragment();

        build_view();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.i(TAG, "onActivityResult: " + resultCode + " " + requestCode);

        if(requestCode == ADDRUNNINGACTIVITYCODE){

//            if(eventsFragment == null){
//                eventsFragment = new EventsFragment();
//            }
//
//            eventsFragment.build_Activity();

        }
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void build_locationManager() {
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.i(TAG, location.getLatitude() + "  " + location.getLongitude());
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        };
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 15000, 100, locationListener);
    }

    void build_view(){
        vpPages = findViewById(R.id.mainTabbed_vpPages);
        TabLayout tabLayout = findViewById(R.id.mainTabbed_tlTabs);



        mSectionsPageAdapter.addFragment(profileFragment, "Profile");
        mSectionsPageAdapter.addFragment(eventsFragment, "Events");
        mSectionsPageAdapter.addFragment(friendsFragment, "Friends");

        vpPages.setAdapter(mSectionsPageAdapter);

        tabLayout.setupWithViewPager(vpPages);

    }

    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();
    }

    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public class SectionsPageAdapter extends FragmentPagerAdapter {


        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();


        public void addFragment(Fragment fragment, String title){
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public SectionsPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }
    }
}



//    ArrayList<Integer> array1 = new ArrayList<>(Arrays.asList(1,7, 11, 13, 14, 15));
//    ArrayList<Integer> array2 = new ArrayList<>(Arrays.asList(5, 6, 10, 14, 14, 17));
//
//    int i1 = array1.size() -1;
//    int i2 = array2.size() -1;
//
//        Log.i(TAG, "BBBBBBBBBBBBBBBBBBBBB");
//                while(i2 > -1){
//
//                Log.i(TAG,  "Current: " + Integer.toString(array1.get(i1)) + " " +  Integer.toString(array2.get(i2)));
//                Log.i(TAG, "Position: " + Integer.toString(i1) + " " +  Integer.toString(i2));
//
//                Log.i(TAG, array1.toString());
//
//                if(array1.get(i1) == array2.get(i2)){
//                Log.i(TAG, "Skipped");
////                i1--;
//                i2--;
//                }else if(array2.get(i2) > array1.get(i1)){
//                array1.add(i1 +1, array2.get(i2));
//                Log.i(TAG, "Added");
////                i1--;
//                i2--;
//                }else{
//                Log.i(TAG, "greater than new");
//                i1--;
////                i2--;
//                }
//
//                Log.i(TAG, array1.toString());
//
//                }