package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Friend.FriendInfo;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Ack;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.model.Constants.SocketIOConnections;
import com.nhomolka.synchobby.model.Extensions.DateExtension;
import com.nhomolka.synchobby.model.SQLite.Motherbase;
import com.nhomolka.synchobby.model.SQLite.Objects.Chat;
import com.nhomolka.synchobby.model.SQLite.Objects.Friend;
import com.nhomolka.synchobby.model.SQLite.Tables.ChatTable;
import com.nhomolka.synchobby.model.ServerRequest.ServerAPI;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Chat_getNewFromFriend;
import com.nhomolka.synchobby.model.SharedPreferencesUtils;
import com.nhomolka.synchobby.model.Toasts;
import com.nhomolka.synchobby.model.UniversalImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class FriendChatFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "##FriendChatFragment";

    RecyclerView vMessageListRV;

    MessageListRVA mMessageListRVA = new MessageListRVA();
    List<Chat> mChats;
    TextView vTypedMessageTV;
    Button vSendMessageBT;
    String lastSentMessage;
    Friend mFriend;
    int friendID;
    private Socket mSocket;
    ChatTable mChatTable = Motherbase.getInstance(getContext()).getChatMessageTable();
    FriendInfoInterface mFriendInfoInterface;

    public FriendChatFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view_build();
    }

    public void customInit(Friend friend, FriendInfoInterface friendInfoInterface){
        mFriend = friend;
        mFriendInfoInterface = friendInfoInterface;

        IO.Options options = new IO.Options();
        options.query = "token=" + SharedPreferencesUtils.getInstance(getContext()).getString(SharedPreferencesUtils.PrefKeys.Token_str);

        try {
            mSocket = IO.socket("https://synchobby.com", options);
        } catch (URISyntaxException e) {
        }
        mSocket.on(SocketIOConnections.friendChat(SharedPreferencesUtils.getInstance(getContext()).getInt(SharedPreferencesUtils.PrefKeys.AppUserID_int), friendID), handleIncomeMessages);
        mSocket.connect();
    }

    void view_build(){

        //Connect
        vMessageListRV = getView().findViewById(R.id.fragmentFriendChat_messageListRV);
        vTypedMessageTV = getView().findViewById(R.id.fragmentFriendChat_typedMessageET);
        vSendMessageBT = getView().findViewById(R.id.fragmentFriendChat_sendMessageBT);

        //SET ADAPTER
        vMessageListRV.setAdapter(mMessageListRVA);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setStackFromEnd(true);
        vMessageListRV.setLayoutManager(linearLayoutManager);
//        vMessageListRV.scrollToPosition(mChats.size() - 1);

        //listeners
        vSendMessageBT.setOnClickListener(this);

        mChats = mChatTable.selectChatsFromAppUserID(mFriend.getAppUserID());
        new getNewMessages().execute();
    }

    public void userDeleted(){
        vSendMessageBT.setEnabled(false);
        vTypedMessageTV.setEnabled(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_friend_chat, container, false);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == vSendMessageBT.getId()){
            int appUserID = SharedPreferencesUtils.getInstance(getContext()).getInt(SharedPreferencesUtils.PrefKeys.AppUserID_int);
            final Chat chat = new Chat(appUserID, mFriend.getAppUserID(), vTypedMessageTV.getText().toString());

            mChats.add(chat);


            mMessageListRVA.notifyItemInserted(mChats.size());
            lastSentMessage = chat.getMessage();
            vTypedMessageTV.setText("");
            mSocket.emit(SocketIOConnections.sendChatMessage, chat.getJSONString(), new Ack() {
                @Override
                public void call(Object... args) {
                    JSONObject jsonObject = (JSONObject)args[0];
                    mChatTable.insertOrReplaceChatFromMySQL(jsonObject);
                    try {
                        final List<Chat> newChats = new ArrayList<>();
                        newChats.add(new Chat(jsonObject));

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                insertNewChats(newChats);
                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            });
        }
    }


    public class MessageListRVA extends RecyclerView.Adapter {
        private static final int VIEW_TYPE_MESSAGE_SENT = 1;
        private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;

        public MessageListRVA(){}

        @Override
        public int getItemViewType(int position) {
            Chat message = mChats.get(position);
            int appUserId = SharedPreferencesUtils.getInstance(getContext()).getInt(SharedPreferencesUtils.PrefKeys.AppUserID_int);
            if (appUserId == message.getOwner()) {
                return VIEW_TYPE_MESSAGE_SENT;
            } else {
                return VIEW_TYPE_MESSAGE_RECEIVED;
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view;
            if(viewType == VIEW_TYPE_MESSAGE_SENT){
                view = LayoutInflater.from(
                        parent.getContext()).inflate(R.layout.item_message_sent,
                        parent,
                        false
                );
                return new SentMessageHolder(view);
            }else if(viewType == VIEW_TYPE_MESSAGE_RECEIVED){
                view = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.item_message_received,
                        parent,
                        false
                );
                return new ReceivedMessageHolder(view);
            }
            return  null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            Chat message = mChats.get(position);
            switch (holder.getItemViewType()) {
                case VIEW_TYPE_MESSAGE_SENT:
                    ((SentMessageHolder) holder).bind(message);
                    break;
                case VIEW_TYPE_MESSAGE_RECEIVED:
                    ((ReceivedMessageHolder) holder).bind(message);
            }
        }

        private class SentMessageHolder extends RecyclerView.ViewHolder {
            TextView messageText, timeText;

            SentMessageHolder(View itemView) {
                super(itemView);

                messageText = itemView.findViewById(R.id.text_message_body);
                timeText = itemView.findViewById(R.id.text_message_time);
            }

            void bind(Chat chat) {
                messageText.setText(chat.getMessage());
                timeText.setText(  DateExtension.MySQLToPattern(chat.getCreatedTS(), "HH:MM"));
            }
        }

        class ReceivedMessageHolder extends RecyclerView.ViewHolder {
            TextView messageText, timeText, nameText;
            CircleImageView profileImage;

            public ReceivedMessageHolder(View itemView) {
                super(itemView);
                messageText = itemView.findViewById(R.id.text_message_body);
                timeText = itemView.findViewById(R.id.text_message_time);
                nameText = itemView.findViewById(R.id.text_message_name);
                profileImage = itemView.findViewById(R.id.itemMessageReceived_civReceiverProfile);
            }

            void bind(final Chat chat) {
                messageText.setText(chat.getMessage());
                timeText.setText(DateExtension.MySQLToPattern(chat.getCreatedTS(), "HH:MM"));
                nameText.setText(mFriend.getName());

                UniversalImageLoader.setImage(mFriend.getAppUserID(), mFriend.getImageVersion(), profileImage);
            }
        }




        @Override
        public int getItemCount() {
            return mChats.size();
        }

    }

    class getNewMessages extends AsyncTask<Void, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(Void... voids) {
            return new ServerAPI(getContext()).serverRequestGET(new Chat_getNewFromFriend(mFriend.getAppUserID(), getContext()));
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                if(Toasts.responseIsGoodToaster(jsonObject, getActivity())) {
                    switch (jsonObject.getString(APIRequest.message)){
                        case Chat_getNewFromFriend.Response.foundNewMessages:
                            Chat_getNewFromFriend chat_getNewFromFriend = new Chat_getNewFromFriend(mFriend.getAppUserID(), getContext());
                            JSONArray jsonArray = jsonObject.getJSONArray(Chat_getNewFromFriend.ResponseFields.newMessagesFound);
                            mChatTable.insertOrReplaceChatsFromMySQL(jsonArray);
                            List<Chat> chats = mChatTable.selectFriendChatHigherThanChatID(friendID, chat_getNewFromFriend.latestChatID);
                            if(chats != null){
                                insertNewChats(chats);
                            }
                        case Chat_getNewFromFriend.Response.notFoundNewMessages:

                            break;
                        default:
                            Log.i(TAG, "SWITCH CASE NOT FOUND: ");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void insertNewChats(List<Chat> chats){

        Log.i(TAG, "insertNewChats");

        Log.i(TAG, chats.toString());

        int i1 = 0;
        int i2 = 0;
        ArrayList<Integer> insertedPositions = new ArrayList<>();

        if(chats.size() > 0){

            Chat chat = chats.get(i2);

            while(i2 < chats.size()){
                if(mChats.size() -1 == i1 - 1) {
                    mChats.add(chats.get(i2));
                    insertedPositions.add(mChats.size() - 1);
                    i2++;
                }else if(mChats.get(i1).getUUID().compareTo(chats.get(i2).getUUID()) == 0) {
                    if(mChats.get(i1).getChatID() == -1){
                        mChats.set(i1, chats.get(i2));
                    }
                    i2++;
                }else if(mChats.get(i1).getChatID() == chats.get(i2).getChatID()){
                    i2++;
                }else if(chats.get(i2).getChatID() < mChats.get(i1).getChatID()){
                    insertedPositions.add(i1 + 1);
                    mChats.add(i1 + 1, chat);
                    i2++;
                }else{
                    i1++;
                }
            }
        }

        for(int position : insertedPositions){
            mMessageListRVA.notifyItemInserted(position);
        }

        vMessageListRV.scrollToPosition(mMessageListRVA.getItemCount() - 1);
    }

    private Emitter.Listener handleIncomeMessages = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject jsonObject = (JSONObject) args[0];
                    try {
                        mChatTable.insertOrReplaceChatFromMySQL(jsonObject);
                        Chat chat = new Chat(jsonObject);
                        if(chat.getOwner() == friendID){
                            List<Chat> chats = new ArrayList<>();
                            insertNewChats(chats);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };


    @Override
    public void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
    }
}
