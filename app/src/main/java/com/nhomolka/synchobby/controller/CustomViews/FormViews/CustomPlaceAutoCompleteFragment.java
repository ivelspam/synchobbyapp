package com.nhomolka.synchobby.controller.CustomViews.FormViews;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.widget.TextView;


import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.nhomolka.synchobby.R;

public class CustomPlaceAutoCompleteFragment extends PlaceAutocompleteFragment {

    Place place;

    @SuppressLint("RestrictedApi")
    public void onActivityResult(int var1, int var2, Intent var3) {
        super.onActivityResult(var1, var2, var3);
        TextView placeAutocompleteTV = getView().findViewById(R.id.place_autocomplete_search_input);

        place = PlaceAutocomplete.getPlace(this.getActivity(), var3);

        if(var1 == 30421) {
            if(var2 == -1) {
                Place var4 = PlaceAutocomplete.getPlace(this.getActivity(), var3);
                placeAutocompleteTV.setText(var4.getAddress().toString());
            } else if(var2 == 2) {
                Status var5 = PlaceAutocomplete.getStatus(this.getActivity(), var3);

            }
        }

    }

    Place getPlace(){
        return place;
    }


}
