package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Profile;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.BaseActivity;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Appuser_UpdateDistanceAndMeasurementSystem;
import com.nhomolka.synchobby.model.ServerRequest.ServerAPI;
import com.nhomolka.synchobby.model.SharedPreferencesUtils;
import com.nhomolka.synchobby.model.Toasts;

import org.json.JSONException;
import org.json.JSONObject;

import static com.nhomolka.synchobby.model.SharedPreferencesUtils.PrefKeys.Distance_int;
import static com.nhomolka.synchobby.model.SharedPreferencesUtils.PrefKeys.MeasureSystem_str;

public class EditDistanceActivity extends BaseActivity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener, SeekBar.OnSeekBarChangeListener{
    static private String TAG = "##EditDistanceActivity";

    RadioGroup distanceTypeRG;
    RadioButton kilometersRB, milesRB;
    SeekBar distanceSB;
    TextView distanceTV;
    Button updateBT;
    double multiplier = 1.6;
    int distance;
    String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_distance);
        build_view();
    }

    void build_view() {
        distanceTV = findViewById(R.id.editDistance_distanceTV);
        distanceTypeRG = findViewById(R.id.editDistance_distanceTypeRG);
        distanceSB = findViewById(R.id.editDistance_distanceRangeSB);
        updateBT = findViewById(R.id.editDistance_updateBT);
        kilometersRB = findViewById(R.id.editDistance_kilometersRB);
        milesRB = findViewById(R.id.editDistance_milesRB);


        //add listeners
        updateBT.setOnClickListener(this);
        distanceSB.setOnSeekBarChangeListener(this);
        distanceTypeRG.setOnCheckedChangeListener(this);


        //initial state
        distance = SharedPreferencesUtils.getInstance(getApplication()).getInt(Distance_int);
        switch (SharedPreferencesUtils.getInstance(getBaseContext()).getString(MeasureSystem_str)){
            case "km":
                distanceTypeRG.check(kilometersRB.getId());
                break;
            case "mi":
                distanceTypeRG.check(milesRB.getId());
                break;
        }
        changeRange();

        distanceSB.setProgress((int)(distance/multiplier));
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == updateBT.getId()) {
            new UpdateDistanceAndMeasurure().execute();
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (seekBar.getId() == distanceSB.getId()) {
            changeRange();
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    void changeRange(){
        if(milesRB.isChecked()){
            multiplier = 1;
            type = "Miles";
        }else{
            multiplier = 1.6;
            type = "Kilometers";
        }

        distanceTV.setText("Distance: " + (int)(distanceSB.getProgress() * multiplier) + " " + type);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        Log.i(TAG, "AQUI");
//        SharedPreferencesUtils.getInstance(getBaseContext()).put(Distance_int, distanceSB.getProgress());
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
        changeRange();
    }

    class UpdateDistanceAndMeasurure extends AsyncTask<String, String, JSONObject> {

        int distance;
        Appuser_UpdateDistanceAndMeasurementSystem.Type type = Appuser_UpdateDistanceAndMeasurementSystem.Type.km;

        @Override
        protected JSONObject doInBackground(String... strings) {

            type = Appuser_UpdateDistanceAndMeasurementSystem.Type.km;
            distance = (int)(distanceSB.getProgress() * multiplier);
            if(milesRB.isChecked()){
                type = Appuser_UpdateDistanceAndMeasurementSystem.Type.mi;
            }

            return new ServerAPI(getBaseContext()).serverRequestPOST(new Appuser_UpdateDistanceAndMeasurementSystem(type, distance));
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                if(Toasts.responseIsGoodToaster(jsonObject, getBaseContext())) {
                    switch (jsonObject.getString(APIRequest.message)){
                        case Appuser_UpdateDistanceAndMeasurementSystem.Response.updated :
                            SharedPreferencesUtils.getInstance(getBaseContext()).put(SharedPreferencesUtils.PrefKeys.Distance_int, distance);
                            SharedPreferencesUtils.getInstance(getBaseContext()).put(SharedPreferencesUtils.PrefKeys.MeasureSystem_str, type.getValue());
                            break;
                        case Appuser_UpdateDistanceAndMeasurementSystem.Response.notUpdated :

                            break;

                        default:
                            Log.i(TAG, "ERROR: MESSAGE OPTION NOT FOUND Appuser_UpdateDistanceAndMeasurementSystem");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
