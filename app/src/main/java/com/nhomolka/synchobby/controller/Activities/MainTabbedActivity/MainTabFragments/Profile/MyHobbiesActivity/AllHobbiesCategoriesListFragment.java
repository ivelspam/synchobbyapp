package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Profile.MyHobbiesActivity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.controller.Interfaces.MyHobbiesInterface;
import com.nhomolka.synchobby.model.Extensions.ExtensionsHack;
import com.nhomolka.synchobby.model.SQLite.Objects.Hobby;
import com.nhomolka.synchobby.model.SQLite.Tables.AppUserHobbyTable;
import com.nhomolka.synchobby.model.SQLite.Tables.HobbyTable;
import com.nhomolka.synchobby.model.SQLite.Motherbase;
import com.nhomolka.synchobby.model.UniversalImageLoader;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class AllHobbiesCategoriesListFragment extends Fragment implements TextWatcher, View.OnClickListener {
    private static final String TAG = "##AllHobbiesCatego";
    final int typeParent = 0;
    final int typeChild = 1;

    EditText vFilterET;
    RecyclerView vListRV;
    Button vReturnBT;

    HobbyRowRVA mHobbyRowRVA;
    List<Hobby> mHobbies = new ArrayList<>();
    List<Hobby> mFiltered = new ArrayList<>();
    Hobby mCurrentParent;
    AppUserHobbyTable mAppUserHobbyTable = Motherbase.getInstance(getContext()).getAppUserHobby();
    HobbyTable mHobbyTable = Motherbase.getInstance(getContext()).getHobbyTable();
    MyHobbiesInterface mMyHobbiesInterface;

    public AllHobbiesCategoriesListFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mHobbyRowRVA = new HobbyRowRVA();

        UniversalImageLoader universalImageLoader = new UniversalImageLoader(getContext());
        ImageLoader.getInstance().init(universalImageLoader.getConfig());

    }

    public void customInit(MyHobbiesInterface myHobbiesInterface){
        mMyHobbiesInterface = myHobbiesInterface;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view_build();


    }

    void view_build(){
        vFilterET = getView().findViewById(R.id.fragmentAllHobbiesCategoriesList_filterET);
        vListRV = getView().findViewById(R.id.fragmentAllHobbiesCategoriesList_listRV);
        vReturnBT = getView().findViewById(R.id.fragmentAllHobbiesCategoriesList_returnBT);

        //Listeners
        vFilterET.addTextChangedListener(this);
        vReturnBT.setOnClickListener(this);

        //Adapters And LayoutManager
        vListRV.setAdapter(mHobbyRowRVA);
        vListRV.setLayoutManager(new LinearLayoutManager(getContext()));

        //initial state
        vReturnBT.setEnabled(false);

        loadNewParent(mCurrentParent);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_all_hobbies_categories_list, container, false);
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        filter(charSequence.toString());
    }

    public void filter(String charText){
        charText = charText.toLowerCase(Locale.getDefault());
        mFiltered.clear();
        Log.i(TAG, charText);
        if(charText.length() == 0){
            mFiltered.addAll(mHobbies);
        }else{
            for(Hobby ac : mHobbies){
                if(ac.getName().toLowerCase(Locale.getDefault()).contains(charText)){
                    mFiltered.add(ac);
                }
            }
        }
        mHobbyRowRVA.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        if(view == vReturnBT){

            Log.i(TAG, mCurrentParent.toString());
            loadNewParent(mHobbyTable.selectHobbyByID(mCurrentParent.getParentID()));
        }
    }

    public void updateTableValues(){
        loadNewParent(mCurrentParent);
    }

    void loadNewParent(Hobby hobby){

        int hobbyID = -1;

        if( hobby != null){
            mCurrentParent = hobby;
            hobbyID = mCurrentParent.getHobbyID();
        }

        mHobbies = mAppUserHobbyTable.selectChildNodesWithInHobby(hobbyID);

        vFilterET.removeTextChangedListener(this);
        vFilterET.setText("");
        vFilterET.addTextChangedListener(this);
        mHobbyRowRVA.notifyDataSetChanged();

        if(hobbyID == -1){
            vReturnBT.setEnabled(false);
            vReturnBT.setText("Categories");
            mCurrentParent = null;

        }else{
            vReturnBT.setEnabled(true);
            vReturnBT.setText(mCurrentParent.getName());
        }
    }


    public class HobbyRowRVA extends RecyclerView.Adapter{

        private static final String TAG = "##RegisterHobbySwitch";

        @Override
        public int getItemViewType(int position) {
            if (mHobbies.get(position).getType().compareTo(Hobby.parent) == 0) {
                return typeParent;
            } else if (mHobbies.get(position).getType().compareTo(Hobby.child) == 0){
                return typeChild;
            }else{
                Log.i(TAG, "ERROR: FOUND NOT TYPE");
                return -1;
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            Log.i(TAG, "onCreateViewHolder");
            if(viewType == typeParent){
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_row_hobbies_parent, parent, false);
                return new MyViewHolderParent(view);
            }else {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_row_hobbies_child, parent, false);
                return new MyViewHolderChild(view);
            }
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            Log.i(TAG, "onBindViewHolder");
            if(holder instanceof MyViewHolderParent){
                ((MyViewHolderParent) holder).bind(mHobbies.get(position));
            }else {
                ((MyViewHolderChild) holder).bind(mHobbies.get(position));
            }
        }

        @Override
        public int getItemCount() {
            return mHobbies.size();
        }

        class MyViewHolderParent extends RecyclerView.ViewHolder {
            Button itemBT;

            public MyViewHolderParent(View itemView) {
                super(itemView);
                itemBT = itemView.findViewById(R.id.adapterRowHobbiesParent_itemBT);
            }

            void bind(final Hobby hobby) {
                itemBT.setText(ExtensionsHack.StringToTitleCase(hobby.getName()));

                itemBT.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadNewParent(hobby);
                    }
                });
            }
        }

        class MyViewHolderChild extends RecyclerView.ViewHolder {
            TextView nameTV;
            RegisterHobbySwitch registerSW;

            public MyViewHolderChild(View itemView) {
                super(itemView);
                registerSW = itemView.findViewById(R.id.addHobby_registerSW);
                nameTV = itemView.findViewById(R.id.addHobby_tvName);
            }

            void bind(final Hobby hobby) {
                nameTV.setText(ExtensionsHack.StringToTitleCase(hobby.getName()));

                Log.i(TAG, hobby.toString());
                registerSW.customInit(hobby, mMyHobbiesInterface, "categories");
                registerSW.setVisibility(View.VISIBLE);
                nameTV.setOnClickListener(null);
            }
        }

    }


    @Override
    public void afterTextChanged(Editable editable) { }
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
}