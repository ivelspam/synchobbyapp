package com.nhomolka.synchobby.controller.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.auth0.android.jwt.JWT;
import com.facebook.login.LoginManager;
import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabbedActivity;
import com.nhomolka.synchobby.model.Constants.TokenValues;
import com.nhomolka.synchobby.model.ServerRequest.DownloadAllInfoCallable;
import com.nhomolka.synchobby.model.SQLite.Motherbase;
import com.nhomolka.synchobby.model.ServerRequest.QueryForUpdatedSingleton;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.AllInfo_getAllInfo;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.AllInfo_getUpdates;
import com.nhomolka.synchobby.model.LogOut.Logout;
import com.nhomolka.synchobby.model.Location.LocationUtils;
import com.nhomolka.synchobby.model.PermissionsUtils;
import com.nhomolka.synchobby.model.SharedPreferencesUtils;
import com.nhomolka.synchobby.model.Toasts;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class TransactionLoginScreenActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "##TransactionLoginScr";

    String token;
    LinearLayout llGivePermissions;
    TextView btGivePermissions;
    SharedPreferencesUtils mSharedPreferencesUtils;
    LocationListener mLocationListener;
    LocationManager mLocationManager;
    TextView vServerStatusTV;
    Button vGoToMainScreenBT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_transaction_login_screen);

        FirebaseInstanceId.getInstance().getToken();
        mSharedPreferencesUtils = SharedPreferencesUtils.getInstance(this);

        token = mSharedPreferencesUtils.getString(SharedPreferencesUtils.PrefKeys.Token_str);
        build_view();
        if (token != null) {
//            Log.i(TAG, "token != null");
            Log.i(TAG, token);
            JWT jwt = new JWT(token);
            String accountStatus = jwt.getClaim(TokenValues.AccountStatus).asString();


            Log.i(TAG, accountStatus);

            if (accountStatus.compareTo("0") == 0) {
//                Log.i(TAG, "confirmed == 0");

                Intent intent = new Intent(this, EmailConfirmationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
                startActivity(intent);
                finish();
            } else {
//                Log.i(TAG, "confirmed != 0");
                if (PermissionsUtils.checkAndRequestPermissions(this)) {
                    FindLocation();
                }
            }
        } else {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
            startActivity(intent);
            finish();
        }
    }

    void build_view(){
        btGivePermissions = findViewById(R.id.transactionLoginScreen_btGivePermission);
        llGivePermissions = findViewById(R.id.activityTransactionLoginScreen_givePermissionLL);
        vServerStatusTV = findViewById(R.id.transactionLoginScreen_serverStatusTV);
        vGoToMainScreenBT = findViewById(R.id.transactionLoginScreen_goToMainScreenBT);

        llGivePermissions.setOnClickListener(this);
        btGivePermissions.setOnClickListener(this);
        vGoToMainScreenBT.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        if(btGivePermissions == view){
            PermissionsUtils.checkAndRequestPermissions(getCurrentActivity() );
        }else if(vGoToMainScreenBT == view){
            Motherbase.getInstance(getBaseContext()).restartDatabase();
            LoginManager.getInstance().logOut();

            //Close Service
            QueryForUpdatedSingleton.getInstance().endTask();
            SharedPreferencesUtils.getInstance(getBaseContext()).clear();
            Intent intent = new Intent(getBaseContext(), TransactionLoginScreenActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // To clean up all activities
            startActivity(intent);
            finish();
        }
    }

    private Activity getCurrentActivity(){
        return this;
    }

    public void FindLocation() {
//        Log.i(TAG, "Find Location");
        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        mLocationListener = new LocationListener() {
            public void onLocationChanged(Location location) {

                Log.i(TAG, "onLocationChanged");
                if(location == null){
                    Log.i(TAG, "location == null");
                }else{
                    Log.i(TAG, "location != null");
                }
//                Log.i(TAG, "Find Location Change");

                mLocationManager.removeUpdates(this);
                makeAllRequests();
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            public void onProviderEnabled(String provider) {

            }

            public void onProviderDisabled(String provider) {
            }
        };
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        Location location = LocationUtils.getLastKnownLocation(getBaseContext());

        if(location != null){
//            Log.i(TAG, "make request");
            makeAllRequests();
        }else{
//            Log.i(TAG, "dont make request");
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 1000, 0, mLocationListener);
        }
    }

    private void noFirstLogin(){
        Log.i(TAG, "noFirstLogin");

        ExecutorService executorService = Executors.newFixedThreadPool(1);
        Future<JSONObject> future = executorService.submit(new Callable<JSONObject>() {
            @Override
            public JSONObject call() {
                return QueryForUpdatedSingleton.getUpdates(getBaseContext());
            }
        });

        executorService.shutdown();
        try {
            executorService.awaitTermination(8, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            JSONObject jsonObject = future.get();

            if(Toasts.responseIsGoodToaster(jsonObject, getBaseContext())){
                switch (jsonObject.getString(APIRequest.message)){
                    case AllInfo_getUpdates.Response.noUpdates:
                        Intent intent1 = new Intent(getBaseContext(), MainTabbedActivity.class);
                        intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);
                        finish();
                        break;
                    case AllInfo_getUpdates.Response.foundUpdates:
                        Intent intent2 = new Intent(getBaseContext(), MainTabbedActivity.class);
                        intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent2);
                        finish();
                        break;
                    default:
                        Log.i(TAG, "FOUND DEFAULT: ERROR 901");
                        break;
                }
            }else{
                vServerStatusTV.setText("Connection to server is bad. Try Another Time.");
                vServerStatusTV.setVisibility(View.VISIBLE);
                vGoToMainScreenBT.setVisibility(View.VISIBLE);

            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void firstLogin(){

        Log.i(TAG, "makeAllRequests");
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        Future<JSONObject> future = executorService.submit(new DownloadAllInfoCallable(getBaseContext()));

        executorService.shutdown();
        try {
            executorService.awaitTermination(8, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            JSONObject jsonObject = future.get();

            if(Toasts.responseIsGoodToaster(jsonObject, getBaseContext())){
                switch (jsonObject.getString(APIRequest.message)){
                    case AllInfo_getAllInfo.Response.userFound:
                        Intent intent = new Intent(getBaseContext(), MainTabbedActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        break;
                    case AllInfo_getAllInfo.Response.userNotFound:
                        Logout.logout(getCurrentActivity());
                        break;
                }
            }else{
                vServerStatusTV.setText("Server is Offline");
                vServerStatusTV.setVisibility(View.VISIBLE);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void makeAllRequests(){
        if(SharedPreferencesUtils.getInstance(getBaseContext()).getString(SharedPreferencesUtils.PrefKeys.UpdatedTS_str, "").compareTo("") != 0){
            Log.i(TAG, "not first login");
            noFirstLogin();
        }else{
            Log.i(TAG, "first login");
            firstLogin();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onDestroy()");
        mLocationManager.removeUpdates(mLocationListener);
        mLocationManager = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.i(TAG, "onRequestPermissionsResult");

        for (int i = 0; i < permissions.length; i++) {
            Log.i(TAG, permissions[i] + " " + grantResults[i]);
        }

        boolean permissionGranted = true;
        for (int i = 0; i < grantResults.length; i++) {
            if (grantResults[i] == -1) {
                Log.i(TAG, "false");
                permissionGranted = false;
            }
        }

        if (permissionGranted) {
            Log.i(TAG, "permissionGranted == true");
            FindLocation();
        } else {
            Log.i(TAG, "permissionGranted == false");
            llGivePermissions.setVisibility(View.VISIBLE);
            btGivePermissions.setVisibility(View.VISIBLE);
        }
    }
}