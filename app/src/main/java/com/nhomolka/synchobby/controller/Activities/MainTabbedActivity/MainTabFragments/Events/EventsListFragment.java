package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Events;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.model.Extensions.DateExtension;
import com.nhomolka.synchobby.model.SQLite.Objects.Event;
import com.nhomolka.synchobby.model.SQLite.Objects.Event.EventState;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.TextUtilities;
import com.nhomolka.synchobby.model.Location.LocationUtils;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class EventsListFragment extends Fragment {
    private static final String TAG = "##EventsListFragment";

    EventsListRVA mEventsListRVA = new EventsListRVA();
    RecyclerView vEventsListRV;
    List<Event> mEvents = new ArrayList<>();

    EventState mState;

    public EventsListFragment() {}


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mState = EventState.valueOf(getArguments().getInt(MySQLColumns.State));

        //Connect
        vEventsListRV = getView().findViewById(R.id.fragmentEventsList_eventsListRV);

        //InitialState

        //Adapters
        vEventsListRV.setAdapter(mEventsListRVA);
        vEventsListRV.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_events_list, container, false);
    }

    public void insertOrRemoveOrUpdateEvent(List<Event> events){
        int i1 = 0;
        int i2 = 0;
        while(i2 < events.size()){
            Event event = events.get(i2);
            if((mEvents.size() - 1) == (i1 - 1)) {
                if(event.getState() == mState){
                    mEvents.add(event);
                    if(mEventsListRVA != null){
                        mEventsListRVA.notifyItemInserted(mEvents.size() - 1);
                    }
                }
                i2++;
            }else if(mEvents.get(i1).getEventID() == event.getEventID()){
                if(event.getState() == mState) {
                    if(mEventsListRVA != null){
                        mEvents.set(i1, event);
                        mEventsListRVA.notifyItemChanged(i1);
                    }
                }else if(event.getState() != mState){
                    mEvents.remove(i1);
                    mEventsListRVA.notifyItemRemoved(i1);
                }
                i2++;
            }else if(event.getEventID() < mEvents.get(i1).getEventID()){
                if(event.getState() == mState){
                    mEvents.add(i1, event);
                    if(mEventsListRVA != null){
                        mEventsListRVA.notifyItemInserted(i1);
                    }
                }
                i2++;
            }else{
                i1++;
            }
        }
    }

    class EventsListRVA extends RecyclerView.Adapter {
        private static final String TAG = "##EventsListRVA";

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_in_event, parent, false);
            return new eventHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            Event event = mEvents.get(position);
            ((eventHolder) holder).bind(event);
        }

        @Override
        public int getItemCount() {
            return mEvents.size();
        }

        private class eventHolder extends RecyclerView.ViewHolder {

            TextView categoryNameTV, eventNameTV, eventTimeTV, distanceTV, hostTV, placeTV, inEventTV, howManyPeopleTV;
            CircleImageView civHost;
            ConstraintLayout wholeCL;

            public eventHolder(View itemView) {
                super(itemView);
                inEventTV = itemView.findViewById(R.id.fragmentInEvent_inEventTV);
                categoryNameTV = itemView.findViewById(R.id.fragmentInEvent_hobbyNameTV);
                eventNameTV = itemView.findViewById(R.id.fragmentInEvent_eventNameTV);
                eventTimeTV = itemView.findViewById(R.id.fragmentInEvent_timeTV);
                distanceTV = itemView.findViewById(R.id.fragmentInEvent_distanceTV);
                hostTV = itemView.findViewById(R.id.fragmentInEvent_hostTV);
                placeTV = itemView.findViewById(R.id.fragmentInEvent_placeTV);
                civHost = itemView.findViewById(R.id.fragmentInEvent_hostCIV);
                wholeCL = itemView.findViewById(R.id.fragmentInEvent_wholeCL);
                howManyPeopleTV = itemView.findViewById(R.id.fragmentInEvent_howManyPeopleTV);
            }

            public void bind(final Event event) {
                Location location = LocationUtils.getLastKnownLocation(getContext());

                //initial state
                inEventTV.setVisibility(View.GONE);
                categoryNameTV.setText(event.getHobbyNameTitleCase());
                eventNameTV.setText(event.getName());
                eventTimeTV.setText(DateExtension.UTCToLocal(event.getStartDatetime()) + " - " + DateExtension.UTCToLocal(event.getEndDatetime()));
                distanceTV.setText(TextUtilities.haversine(location.getLatitude(), location.getLongitude(), event.getLatitude(), event.getLongitude(), getContext()));
                hostTV.setText(event.getAppUserName());
                placeTV.setText("This is a House");
                howManyPeopleTV.setText("There are " + Integer.toString(event.getQuantityOfPeople()) + " people.");

                //listeners
                wholeCL.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(getContext(), InEventMoreInfoActivity.class);
                        i.putExtra("EventID", event.getEventID());
                        getContext().startActivity(i);
                    }
                });
            }
        }
    }

}
