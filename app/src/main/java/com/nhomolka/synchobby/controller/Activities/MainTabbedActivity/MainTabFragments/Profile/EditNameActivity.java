package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Profile;

import android.os.AsyncTask;
import android.support.design.widget.TextInputEditText;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.BaseActivity;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Appuser_updateName;
import com.nhomolka.synchobby.model.ServerRequest.ServerAPI;
import com.nhomolka.synchobby.model.SharedPreferencesUtils;
import com.nhomolka.synchobby.model.Toasts;

import org.json.JSONException;
import org.json.JSONObject;

public class EditNameActivity extends BaseActivity implements View.OnClickListener{
    private static final String TAG = "##EditNameActivity";

    TextInputEditText vNameTIET;
    Button vUpdateBT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_name);
        view_build();
    }

    void view_build(){
        vNameTIET = findViewById(R.id.activityEditName_nameTIET);
        vUpdateBT = findViewById(R.id.activityEditName_updateBT);

        //INITIAL STATE
        vNameTIET.setText(SharedPreferencesUtils.getInstance(getBaseContext()).getString(SharedPreferencesUtils.PrefKeys.Name_str));
        vUpdateBT.setOnClickListener(this);

    }
    @Override
    public void onClick(View view) {
        if(view.getId() == vUpdateBT.getId()){
            String newUsername = vNameTIET.getText().toString();
            String oldUsername = SharedPreferencesUtils.getInstance(getBaseContext()).getString(SharedPreferencesUtils.PrefKeys.Username_str);
            if(newUsername.compareTo(oldUsername) !=0 ){
                new UpdateName_http().execute();
            }else{
                Toast.makeText(getBaseContext(), "Nothing Changed. Try a Different Username.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private class UpdateName_http extends AsyncTask<Void, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(Void... voids) {
            JSONObject jsonObject = new ServerAPI(getBaseContext()).serverRequestPOST(new Appuser_updateName(vNameTIET.getText().toString()));
            try {
                switch(jsonObject.getString(APIRequest.message)){
                    case Appuser_updateName.Response.updated:
                        SharedPreferencesUtils.getInstance(getBaseContext()).put(SharedPreferencesUtils.PrefKeys.Name_str, vNameTIET.getText().toString());
                        break;
                    case Appuser_updateName.Response.notUpdated:

                        break;
                    default:
                        Log.i(TAG, "RESPONSE FIELD ERROR: Appuser_updateName 1");

                }
                return jsonObject;
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {

            try {
                if(Toasts.responseIsGoodToaster(jsonObject, getBaseContext())){
                    switch(jsonObject.getString(APIRequest.message)){
                        case  Appuser_updateName.Response.updated:
                            Toast.makeText(getBaseContext(), "Updated", Toast.LENGTH_SHORT).show();
                            break;
                        case Appuser_updateName.Response.notUpdated:

                            break;
                        default:
                            Log.i(TAG, "RESPONSE FIELD ERROR: Appuser_updateName 2");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }



}
