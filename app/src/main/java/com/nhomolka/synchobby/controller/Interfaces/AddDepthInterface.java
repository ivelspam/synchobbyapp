package com.nhomolka.synchobby.controller.Interfaces;

import com.nhomolka.synchobby.model.SQLite.Objects.Hobby;

/**
 * Created by nhomo on 8/5/2018.
 */

public interface AddDepthInterface {

    void addDepth(Hobby hobby);

}
