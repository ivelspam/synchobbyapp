package com.nhomolka.synchobby.controller.Interfaces;

import com.nhomolka.synchobby.model.SQLite.Objects.Event;

import java.util.List;

public interface UpdateEventsFragmentInterface {
    void setUpEvents(List<Event> events);
}

