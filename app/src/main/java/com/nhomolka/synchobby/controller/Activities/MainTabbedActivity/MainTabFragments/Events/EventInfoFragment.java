package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Events;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.model.Constants.MagicNumbers;
import com.nhomolka.synchobby.model.Extensions.DateExtension;
import com.nhomolka.synchobby.model.SQLite.Objects.Event;
import com.nhomolka.synchobby.model.SQLite.Motherbase;
import com.nhomolka.synchobby.model.SQLite.Tables.EventTable;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Event_close;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Event_delete;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Event_join;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Event_leave;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.ServerRequest.ServerAPI;
import com.nhomolka.synchobby.model.SharedPreferencesUtils;
import com.nhomolka.synchobby.model.Toasts;

import org.json.JSONException;
import org.json.JSONObject;

public class EventInfoFragment extends Fragment implements View.OnClickListener{
    private static final String TAG = "##EventInfoFragment";

    TextView eventNameTV, timeTV, hobbyTV, addressTV, vPlaceNameTV, hostTV, quantityOfPeopleTV, descriptionTV;
    Button vRequestEventStatusChangeBT, vDeleteEventBT;
    Event mEvent;
    EventTable mEventTable = Motherbase.getInstance(getContext()).getEventTable();

    public EventInfoFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEvent = Motherbase.getInstance(getContext()).getEventTable().selectEvent( getArguments().getInt(MySQLColumns.EventID));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        build_view();
    }

    void build_view(){
        quantityOfPeopleTV = getView().findViewById(R.id.activityInEventMoreInfo_howManyPeopleTV);
        timeTV = getView().findViewById(R.id.activityInEventMoreInfo_timeTV);
        eventNameTV = getView().findViewById(R.id.activityInEventMoreInfo_eventNameTV);
        vPlaceNameTV = getView().findViewById(R.id.activityInEventMoreInfo_placeNameTV);
        addressTV = getView().findViewById(R.id.activityInEventMoreInfo_addressTV);
        hostTV = getView().findViewById(R.id.activityInEventMoreInfo_hostTV);
        descriptionTV = getView().findViewById(R.id.activityInEventMoreInfo_descriptionTV);
        vRequestEventStatusChangeBT = getView().findViewById(R.id.activityInEventMoreInfo_requestEventStatusChangeBT);
        hobbyTV = getView().findViewById(R.id.activityInEventMoreInfo_hobbyTV);
        vDeleteEventBT = getView().findViewById(R.id.activityInEventMoreInfo_hideShowButtonBT);

        //INITIAL STATE
        vRequestEventStatusChangeBT.setOnClickListener(this);
        vDeleteEventBT.setOnClickListener(this);
        vPlaceNameTV.setOnClickListener(this);

        //INITIAL STATE
        build_state();
    }

    void build_state(){
        updateValues();
        initial_joinBT();
        initial_DescriptionTV();
        initial_deleteBT();
    }

    void updateValues(){
        quantityOfPeopleTV.setText(Integer.toString(mEvent.getQuantityOfPeople()) + " people joined.");
        timeTV.setText(DateExtension.UTCToLocal(mEvent.getStartDatetime()) + " - " + DateExtension.UTCToLocal(mEvent.getEndDatetime()));
        eventNameTV.setText(mEvent.getName());
        addressTV.setText(mEvent.getAddress());
        hostTV.setText(mEvent.getAppUserName());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_event_info, container, false);
    }

    @Override
    public void onClick(View view) {


        if(view == vRequestEventStatusChangeBT){
            vRequestEventStatusChangeBT.setEnabled(false);
            vDeleteEventBT.setEnabled(false);
            switch (vRequestEventStatusChangeBT.getText().toString()){
                case "Close Event" :
                    new CloseEventStatus_http().execute();
                    break;
                case "Leave Event" :
                    new LeaveEvent_http().execute();
                    break;
                case "Join Event" :
                    new JoinEvent_http().execute();
                    break;
            }
        }else if(view == vDeleteEventBT){
            vRequestEventStatusChangeBT.setEnabled(false);
            vDeleteEventBT.setEnabled(false);
            switch (vDeleteEventBT.getText().toString()){
                case "Delete Event" :
                    new DeletedEvent_http().execute();
                    break;
                case "Event Deleted" :
                    new LeaveEvent_http().execute();
                    break;
            }

        }else if(view == vPlaceNameTV){
            Uri uri = Uri.parse("geo:0,0?q=" + mEvent.getAddress());
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW,  uri);
            startActivity(intent);
        }
    }

    void initial_DescriptionTV(){
        if(mEvent.getDescription().length() > 0){
            descriptionTV.setText(mEvent.getDescription());
        }
    }

    void initial_joinBT(){
        int usereventID = SharedPreferencesUtils.getInstance(getContext()).getInt(SharedPreferencesUtils.PrefKeys.EventID_int);
        int appUserID = SharedPreferencesUtils.getInstance(getContext()).getInt(SharedPreferencesUtils.PrefKeys.AppUserID_int);

        boolean owner = appUserID == mEvent.getAppUserID();

        if(mEvent.getState() == Event.EventState.Ended || mEvent.getState() == Event.EventState.Deleted ){
            vRequestEventStatusChangeBT.setText("Event Ended");

        }else{
            if(owner){
                Log.i(TAG, "OWNER");
                if(mEvent.getState() == Event.EventState.Created){
                    vRequestEventStatusChangeBT.setText("Close Event");
                    vRequestEventStatusChangeBT.setEnabled(true);
                }
            }else{
                if(usereventID == mEvent.getEventID()){
                    vRequestEventStatusChangeBT.setText("Leave Event");
                    vRequestEventStatusChangeBT.setEnabled(true);
                }else if(usereventID != MagicNumbers.NOT_IN_EVENT){
                    vRequestEventStatusChangeBT.setText("Already In another Event");
                }else{
                    vRequestEventStatusChangeBT.setText("Join Event");
                    vRequestEventStatusChangeBT.setEnabled(true);
                }
            }
        }
    }

    void initial_deleteBT(){
        int appUserID = SharedPreferencesUtils.getInstance(getContext()).getInt(SharedPreferencesUtils.PrefKeys.AppUserID_int);
        boolean userIsOwner = appUserID == mEvent.getAppUserID();

        Log.i(TAG, "initial_deleteBT");

        Log.i(TAG, appUserID + " " + userIsOwner);
        if(userIsOwner){
            vDeleteEventBT.setVisibility(View.VISIBLE);
            if(mEvent.getState() != Event.EventState.Deleted){
                vDeleteEventBT.setText("Delete Event");
                vDeleteEventBT.setEnabled(true);
            }else{
                vDeleteEventBT.setText("Event Deleted");
            }
        }else{
            vDeleteEventBT.setVisibility(View.GONE);
        }
    }

    class CloseEventStatus_http extends AsyncTask<Void, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(Void... voids) {
            return new ServerAPI(getContext()).serverRequestPOST(new Event_close(mEvent.getEventID()));
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                if(Toasts.responseIsGoodToaster(jsonObject, getActivity())) {
                    switch (jsonObject.getString(APIRequest.message)){
                        case Event_close.Response.closedEvent:
                            Log.i(TAG, Event_close.Response.closedEvent);

                            mEventTable.insertOrReplaceEventFromMySQL(jsonObject.getJSONObject(Event_close.ResponseFields.event));
                            mEvent = mEventTable.selectEvent(mEvent.getEventID());
                            SharedPreferencesUtils.getInstance(getContext()).put(SharedPreferencesUtils.PrefKeys.EventID_int, -1);
                            break;
                        default:
                            Log.i(TAG, "ERROR NOT FOUND RESPONSE CloseEventStatus_http");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            build_state();
        }
    }

    class DeletedEvent_http extends AsyncTask<Void, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(Void... voids) {
            return new ServerAPI(getContext()).serverRequestPOST(new Event_delete(mEvent.getEventID()));
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            Log.i(TAG, "onPostExecute");
            try {
                if(Toasts.responseIsGoodToaster(jsonObject, getActivity())) {
                    switch (jsonObject.getString(APIRequest.message)){
                        case Event_delete.Response.deletedEvent:
                            mEventTable.insertOrReplaceEventFromMySQL(jsonObject.getJSONObject(Event_delete.ResponseFields.event));
                            mEvent = mEventTable.selectEvent(mEvent.getEventID());
                            break;
                        case Event_delete.Response.closedDeletedEvent:
                            mEventTable.insertOrReplaceEventFromMySQL(jsonObject.getJSONObject(Event_delete.ResponseFields.event));
                            mEvent = mEventTable.selectEvent(mEvent.getEventID());
                            SharedPreferencesUtils.getInstance(getContext()).put(SharedPreferencesUtils.PrefKeys.EventID_int, -1);
                            break;
                        default:
                            Log.i(TAG, "ERROR NOT FOUND RESPONSE DeletedEvent_http");
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            build_state();
        }
    }


    class LeaveEvent_http extends AsyncTask<Void, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(Void... voids) {
            return new ServerAPI(getContext()).serverRequestPOST(new Event_leave(mEvent.getEventID()));
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            Log.i(TAG, "onPostExecute");
            try {
                if(Toasts.responseIsGoodToaster(jsonObject, getActivity())) {
                    switch (jsonObject.getString(APIRequest.message)) {
                        case Event_leave.Response.leftEvent:
                            mEventTable.insertOrReplaceEventFromMySQL(jsonObject.getJSONObject(Event_leave.ResponseFields.event));
                            mEvent = mEventTable.selectEvent(mEvent.getEventID());
                            SharedPreferencesUtils.getInstance(getContext()).put(SharedPreferencesUtils.PrefKeys.EventID_int, -1);
                            break;
                        default:
                            Log.i(TAG, "ERROR NOT FOUND RESPONSE CloseEventStatus_http");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            build_state();
        }
    }

    class JoinEvent_http extends AsyncTask<Void, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(Void... voids) {

            Log.i(TAG, "JoinEvent_http");

            JSONObject jsonObject = new ServerAPI(getContext()).serverRequestPOST(new Event_join(mEvent.getEventID()));
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                if(Toasts.responseIsGoodToaster(jsonObject, getActivity())) {
                    switch (jsonObject.getString(APIRequest.message)) {
                        case Event_join.Response.joinedEvent:
                            mEventTable.insertOrReplaceEventFromMySQL(jsonObject.getJSONObject(Event_join.ResponseFields.event));
                            mEvent = mEventTable.selectEvent(mEvent.getEventID());
                            SharedPreferencesUtils.getInstance(getContext()).put(SharedPreferencesUtils.PrefKeys.EventID_int, mEvent.getEventID());
                            break;
                        case Event_join.Response.alreadyInEvent:
                            Toast.makeText(getActivity(), "Already In A Event",
                                    Toast.LENGTH_LONG).show();
                            break;
                        default:
                            Log.i(TAG, "ERROR NOT FOUND RESPONSE CloseEventStatus_http");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            build_state();

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
