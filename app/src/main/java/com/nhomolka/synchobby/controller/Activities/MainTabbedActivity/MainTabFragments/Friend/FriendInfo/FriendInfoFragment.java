package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Friend.FriendInfo;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.model.SQLite.Motherbase;
import com.nhomolka.synchobby.model.SQLite.Objects.Friend;
import com.nhomolka.synchobby.model.ServerRequest.ServerAPI;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Friend_Delete;
import com.nhomolka.synchobby.model.Toasts;
import com.nhomolka.synchobby.model.UniversalImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class FriendInfoFragment extends Fragment implements View.OnClickListener, FriendInfoInterface{
    private static final String TAG = "##FriendInfoFragment";

    CircleImageView vFriendInfoCIV;
    TextView vNameBT;
    Button vRemoveBT;

    Friend mFriend;

    FriendInfoInterface mFriendInfoInterface;

    public FriendInfoFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void customInit(Friend friend, FriendInfoInterface friendInfoInterface){
        mFriend = friend;
        mFriendInfoInterface = friendInfoInterface;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_friend_info, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        build_view();
    }

    void build_view(){

        vFriendInfoCIV = getView().findViewById(R.id.fragmentFriendInfo_imageCIV);
        vNameBT = getView().findViewById(R.id.fragmentFriendInfo_nameBT);
        vRemoveBT = getView().findViewById(R.id.fragmentFriendInfo_deleteBT);

        vRemoveBT.setOnClickListener(this);
        vNameBT.setText(mFriend.getName());

        UniversalImageLoader.setImage(mFriend.getAppUserID(), mFriend.getImageVersion(), vFriendInfoCIV);
    }

    @Override
    public void onClick(View view) {
        if(vRemoveBT == view){
            Log.i(TAG, "RemoveBT");
            new deleteFriend_http().execute();
        }
    }

    @Override
    public void deletedFriend() {

    }

    class deleteFriend_http extends AsyncTask<String, String, JSONObject> {
        @Override
        protected JSONObject doInBackground(String... strings) {
            return new ServerAPI(getContext()).serverRequestPOST(new Friend_Delete(mFriend.getAppUserID()));
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {

            try {
                if(Toasts.responseIsGoodToaster(jsonObject, getActivity())) {
                    switch (jsonObject.getString(APIRequest.message)){
                        case Friend_Delete.Response.deleted:
                            Motherbase.getInstance(getContext()).getFriendTable().updateFriendState(Friend.FriendState.Deleted.getValue(), mFriend.getAppUserID());
                            vRemoveBT.setText("Deleted");

                        default:
                            Log.i(TAG, "SWITCH CASE NOT FOUND: ");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            vRemoveBT.setEnabled(false);
        }
    }
}
