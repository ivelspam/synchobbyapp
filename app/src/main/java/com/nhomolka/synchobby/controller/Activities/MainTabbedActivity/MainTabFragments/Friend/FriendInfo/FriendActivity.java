package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Friend.FriendInfo;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.BaseActivity;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.SQLite.Objects.Friend;

import static com.nhomolka.synchobby.model.SQLite.Motherbase.getInstance;

public class FriendActivity extends BaseActivity implements View.OnClickListener,  ViewPager.OnPageChangeListener, FriendInfoInterface, SwipeRefreshLayout.OnRefreshListener{
    private static final String TAG = "##FriendActivity";

    ViewPager vViewPagerVP;

    FriendChatFragment mFriendChatFragment;
    FriendInfoFragment mFriendInfoFragment;



    FriendInfoPA mFriendInfoPA;

    Button vChatBT, vProfileBT;

    Friend mFriend;

    boolean deleted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_friend_info_activity);
        Bundle bundle = getIntent().getExtras();

        mFriend = getInstance(getBaseContext()).getFriendTable().selectFriendByAppUserID(bundle.getInt(MySQLColumns.AppUserID));

        mFriendChatFragment = new FriendChatFragment();
        mFriendChatFragment.customInit(mFriend, this);

        mFriendInfoFragment = new FriendInfoFragment();
        mFriendInfoFragment.customInit(mFriend, this);

        view_build();
    }

    void view_build(){

        //CONNECT
        vViewPagerVP = findViewById(R.id.activityFriendInfoActivity_viewPagerVP);
        vChatBT = findViewById(R.id.activityFriendInfoActivity_chatBT);
        vProfileBT = findViewById(R.id.activityFriendInfoActivity_profileBT);


        //INITAL STATE
        vChatBT.setSelected(true);



        vChatBT.setOnClickListener(this);
        vProfileBT.setOnClickListener(this);
        vViewPagerVP.addOnPageChangeListener(this);

        //ADAPTERS
        mFriendInfoPA = new FriendInfoPA(getSupportFragmentManager());
        vViewPagerVP.setAdapter(mFriendInfoPA);


    }

    @Override
    public void deletedFriend() {
        deleted = true;
        mFriendChatFragment.userDeleted();
    }

    @Override
    public void onClick(View view) {
        if(view == vChatBT){
            vChatBT.setSelected(true);
            vProfileBT.setSelected(false);
            vViewPagerVP.setCurrentItem(0, false);
        }else if(view == vProfileBT){
            vChatBT.setSelected(false);
            vProfileBT.setSelected(true);
            vViewPagerVP.setCurrentItem(1, false);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if(position == 0){
            vChatBT.setSelected(true);
            vProfileBT.setSelected(false);
        }else if(position == 1){
            vChatBT.setSelected(false);
            vProfileBT.setSelected(true);
        }
    }

    @Override
    public void onPageSelected(int position) {}

    @Override
    public void onPageScrollStateChanged(int state) {}

    @Override
    protected void onDestroy() { super.onDestroy(); }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onRefresh() {
        Log.i(TAG, "BBBBBBBBBBBBBBBBBBBBBB");
    }

    private class FriendInfoPA extends FragmentPagerAdapter {

        public FriendInfoPA(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0 :
                    return mFriendChatFragment;
                case 1 :
                    return mFriendInfoFragment;
                default:
                    Log.i(TAG, "ERROR: Wrong View Page");
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
