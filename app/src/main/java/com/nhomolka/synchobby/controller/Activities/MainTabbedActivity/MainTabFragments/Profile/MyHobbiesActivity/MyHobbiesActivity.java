package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Profile.MyHobbiesActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.BaseActivity;
import com.nhomolka.synchobby.controller.Interfaces.MyHobbiesInterface;
import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.model.SQLite.Objects.Hobby;


public class MyHobbiesActivity extends BaseActivity implements View.OnClickListener, MyHobbiesInterface {
    static String TAG = "##MyHobbiesActivity";
    final int addHobbyActivityResultCode = 1;

    Button showHobbiesBT, showHobbiesTreeBT, openAddHobbyActivityBT;

    AllHobbiesListFragment allHobbiesListFragment = new AllHobbiesListFragment();
    AllHobbiesCategoriesListFragment allHobbiesCategoriesListFragment = new AllHobbiesCategoriesListFragment();

    ViewPager mViewPager;
    MyHobbiesViewPager mMyHobbiesViewPager;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_my_hobbies);
        build_view();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == addHobbyActivityResultCode){
            allHobbiesListFragment.updateTableValues();
            allHobbiesCategoriesListFragment.updateTableValues();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.i(TAG, "onResume");
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == showHobbiesBT.getId()){
            mViewPager.setCurrentItem(0);
            showHobbiesBT.setSelected(true);
            showHobbiesTreeBT.setSelected(false);
        }else if (view.getId() == showHobbiesTreeBT.getId()){
            mViewPager.setCurrentItem(1);
            showHobbiesBT.setSelected(false);
            showHobbiesTreeBT.setSelected(true);
        }else if (view.getId() == openAddHobbyActivityBT.getId()){
            startActivityForResult(new Intent(getBaseContext(), AddHobbyActivity.class), addHobbyActivityResultCode);
        }
    }

    public void build_view(){

        //Connects
        showHobbiesBT = findViewById(R.id.myHobbies_showHobbiesBT);
        showHobbiesTreeBT = findViewById(R.id.myHobbies_showHobbiesTreeBT);
        openAddHobbyActivityBT = findViewById(R.id.myHobbies_openAddHobbiesBT);
        mViewPager = findViewById(R.id.myHobbies_containerVP);

        //Add Listeners
        showHobbiesBT.setOnClickListener(this);
        showHobbiesTreeBT.setOnClickListener(this);
        openAddHobbyActivityBT.setOnClickListener(this);

        //Set Adapters
        mMyHobbiesViewPager = new MyHobbiesViewPager(getSupportFragmentManager());
        mViewPager.setAdapter(mMyHobbiesViewPager);

        //Set Initial State
        showHobbiesBT.setSelected(true);

        //Custom Init
        allHobbiesListFragment.customInit(this);
        allHobbiesCategoriesListFragment.customInit(this);

    }

    @Override
    public void updateHobby(Hobby hobby, Boolean inHobby, String option) {
        if("categories".compareTo(option)==0){
            allHobbiesListFragment.updateItemInList(hobby, inHobby);
        }else if("list".compareTo(option)==0){
            allHobbiesCategoriesListFragment.updateTableValues();
        }else if("both".compareTo(option) == 0){
            allHobbiesListFragment.updateItemInList(hobby, inHobby);
            allHobbiesCategoriesListFragment.updateTableValues();
        }else{
            Log.i(TAG, "MyHobbiesVC updateHobby ERROR");
        }
    }

    private class MyHobbiesViewPager extends FragmentPagerAdapter {

        public MyHobbiesViewPager(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0 :
                    return allHobbiesListFragment;
                case 1 :
                    return allHobbiesCategoriesListFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

}

