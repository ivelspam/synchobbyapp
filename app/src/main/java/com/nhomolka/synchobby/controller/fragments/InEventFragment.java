package com.nhomolka.synchobby.controller.fragments;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Events.InEventMoreInfoActivity;
import com.nhomolka.synchobby.model.Extensions.DateExtension;
import com.nhomolka.synchobby.model.SQLite.Objects.Event;
import com.nhomolka.synchobby.model.TextUtilities;
import com.nhomolka.synchobby.model.Location.LocationUtils;

import de.hdodenhof.circleimageview.CircleImageView;

public class InEventFragment extends Fragment {
    public static final String TAG = "##InEventFragment";

    TextView categoryNameTV, eventNameTV, eventTimeTV, distanceTV, hostTV, placeTV, inEventTV, howManyPeopleTV;
    CircleImageView civHost;
    ConstraintLayout wholeCL;

    Event mEvent;
    public InEventFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setEvent(Event event){
        mEvent = event;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_in_event, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        build_view();
    }

    void build_view(){
        inEventTV = getActivity().findViewById(R.id.fragmentInEvent_inEventTV);
        categoryNameTV = getActivity().findViewById(R.id.fragmentInEvent_hobbyNameTV);
        eventNameTV = getActivity().findViewById(R.id.fragmentInEvent_eventNameTV);
        eventTimeTV = getActivity().findViewById(R.id.fragmentInEvent_timeTV);
        distanceTV = getActivity().findViewById(R.id.fragmentInEvent_distanceTV);
        hostTV = getActivity().findViewById(R.id.fragmentInEvent_hostTV);
        placeTV = getActivity().findViewById(R.id.fragmentInEvent_placeTV);
        civHost = getActivity().findViewById(R.id.fragmentInEvent_hostCIV);
        wholeCL = getActivity().findViewById(R.id.fragmentInEvent_wholeCL);
        howManyPeopleTV = getActivity().findViewById(R.id.fragmentInEvent_howManyPeopleTV);

        Location location = LocationUtils.getLastKnownLocation(getContext());

        //initial state
        categoryNameTV.setText(mEvent.getHobbyNameTitleCase());
        eventNameTV.setText(mEvent.getName());
        eventTimeTV.setText(DateExtension.UTCToLocal(mEvent.getStartDatetime()) + " - " + DateExtension.UTCToLocal(mEvent.getEndDatetime()));
        distanceTV.setText(TextUtilities.haversine(location.getLatitude(), location.getLongitude(), mEvent.getLatitude(), mEvent.getLongitude(), getContext()));
        hostTV.setText(mEvent.getAppUserName());
        placeTV.setText("This is a House");
        howManyPeopleTV.setText("There are " + Integer.toString(mEvent.getQuantityOfPeople()) + " people.");

        //listeners
        wholeCL.setOnClickListener(null);
        wholeCL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), InEventMoreInfoActivity.class);
                i.putExtra("EventID", mEvent.getEventID());
                getContext().startActivity(i);
            }
        });

    }
}
