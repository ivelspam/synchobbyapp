package com.nhomolka.synchobby.controller.CustomViews.FormViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.model.SQLite.Objects.Hobby;
import com.nhomolka.synchobby.model.SQLite.Motherbase;

import java.util.ArrayList;
import java.util.List;

public class HobbiesCL extends ConstraintLayout {
    private static final String TAG = "##HobbiesSP";

    ErrorAndTitlesTextViews errorAndTitlesTextViews;
    Spinner owner;

    final List<Hobby> mHobbies = Motherbase.getInstance(getContext()).getAppUserHobby().selectHobbiesChildWithInfo();

    enum FieldError {
        empty("Event Cannot Be Empty.");
        private String value;
        public String getValue() { return this.value; }
        FieldError(String value){ this.value = value; }
    }

    public HobbiesCL(Context context, AttributeSet attrs) {
        super(context, attrs);
        owner = new Spinner(getContext());
        owner.setId(View.generateViewId());
        this.addView(owner);
        errorAndTitlesTextViews = new ErrorAndTitlesTextViews(getContext(), this, owner, "Event Hobby");
        owner.setAdapter(new HobbiesAA(getContext(), R.layout.adapter_row_spinner_hobby));

        errorAndTitlesTextViews.bottomTV.setVisibility(View.GONE);
        errorAndTitlesTextViews.errorTV.setVisibility(View.GONE);
    }

    public boolean findError(){
        boolean foundError = false;
        List<FieldError> errors = new ArrayList<>();

        if(owner.getSelectedItemPosition() == -1){
            foundError = true;
            errors.add(FieldError.empty);
        }

        if(foundError){
            errorAndTitlesTextViews.errorTV.setVisibility(VISIBLE);
            errorAndTitlesTextViews.errorTV.setText(errors.get(0).value);
        }else{
            errorAndTitlesTextViews.errorTV.setVisibility(GONE);
        }
        return foundError;
    }

    public int getValue(){
        return mHobbies.get(owner.getSelectedItemPosition()).getHobbyID();
    }

    public class HobbiesAA extends ArrayAdapter<Hobby> {

        public HobbiesAA(@NonNull Context context, int resource) {
            super(context, resource, mHobbies);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            return myCustomSpinnerView(position, convertView, parent);
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            return myCustomSpinnerView(position, convertView, parent);
        }

        public View myCustomSpinnerView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
            LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(getContext().LAYOUT_INFLATER_SERVICE);
            View customView = layoutInflater.inflate(R.layout.adapter_row_spinner_hobby, parent, false);
            TextView tvCategoryName = customView.findViewById(R.id.adapterSpinnerRowHobby_tvHobbyName);
            tvCategoryName.setText(mHobbies.get(position).getName());
            return customView;
        }
    }
}
