package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.nhomolka.synchobby.model.ServerRequest.QueryForUpdatedSingleton;

import java.util.concurrent.ScheduledExecutorService;

public class BaseActivity extends AppCompatActivity {
    private static final String TAG = "##BaseActivity";

    private static int sessionDepth = 0;
    ScheduledExecutorService scheduledExecutorService;

    @Override
    protected void onStart() {
        super.onStart();
//        Log.i(TAG, "OnStart");
        sessionDepth++;
        if(sessionDepth == 1) {
            QueryForUpdatedSingleton.getInstance().startTask(getApplicationContext());
        }
    }

    @Override
    protected void onStop() {
//        Log.i(TAG, "onStop");

        super.onStop();
        if (sessionDepth > 0){
            sessionDepth--;
        }

        if (sessionDepth == 0) {
            Log.i(TAG, "Going To BackGround");
            QueryForUpdatedSingleton.getInstance().endTask();
        }
    }
}

