package com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.Events;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabFragments.BaseActivity;
import com.nhomolka.synchobby.controller.CustomViews.FormViews.AddressPA;
import com.nhomolka.synchobby.controller.CustomViews.FormViews.CustomPlaceAutoCompleteFragment;
import com.nhomolka.synchobby.controller.CustomViews.FormViews.DescriptionCL;
import com.nhomolka.synchobby.controller.CustomViews.FormViews.EventNameCL;
import com.nhomolka.synchobby.controller.CustomViews.FormViews.HobbiesCL;
import com.nhomolka.synchobby.controller.CustomViews.FormViews.PlaceNameCL;
import com.nhomolka.synchobby.controller.CustomViews.FormViews.ReachCL;
import com.nhomolka.synchobby.controller.CustomViews.FormViews.StartEndTimeCL;
import com.nhomolka.synchobby.model.Objects.EventsTimes;
import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.model.SQLite.Motherbase;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Event_register;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.ServerRequest.ServerAPI;
import com.nhomolka.synchobby.model.SharedPreferencesUtils;
import com.nhomolka.synchobby.model.Toasts;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class AddEventActivity extends BaseActivity implements View.OnClickListener{
    private static final String TAG = "##AddEventActivity";

    ConstraintLayout parentCL;

    DescriptionCL descriptionCL;
    EventNameCL eventNameCL;
    PlaceNameCL placeNameCL;
    HobbiesCL hobbiesCL;
    AddressPA addressPA;
    StartEndTimeCL startEndTimeCL;
    ReachCL reachCL;
    Calendar calendar;
    Button registerActivityBT;

    EventsTimes eventsTimes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_running);
        calendar = Calendar.getInstance();
        eventsTimes = new EventsTimes();
        view_build();
    }

    void view_build(){

        //FIND
        parentCL = findViewById(R.id.activityAddRunning_parentCL);
        eventNameCL = findViewById(R.id.activityAddRunning_eventNameCL);
        hobbiesCL = findViewById(R.id.activityAddRunning_activitiesCL);
        addressPA = findViewById(R.id.activityAddRunning_addressPA);
        descriptionCL = findViewById(R.id.ractivtyAddRunning_descriptionCL);
        placeNameCL = findViewById(R.id.activityAddRunning_placeNameCL);
        registerActivityBT = findViewById(R.id.activityAddRunning_registerEventBT);
        reachCL = findViewById(R.id.ractivtyAddRunning_reachCL);

        startEndTimeCL = findViewById(R.id.activtyAddRunning_startEndTimeCL);

        //Custom Init
        addressPA.customInit((CustomPlaceAutoCompleteFragment)getFragmentManager().findFragmentById(R.id.activityAddRunning_placeAutocompleteFragment));

        //Add Listeners
        registerActivityBT.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == registerActivityBT.getId()){

            boolean foundError = false;

            if(eventNameCL.findError()){ foundError = true; }
            if(hobbiesCL.findError()){ foundError = true; }
            if(addressPA.findError()){ foundError = true; }
            if(placeNameCL.findError()){ foundError = true; }
            if(startEndTimeCL.findError()){ foundError = true; }
            if(reachCL.findError()){ foundError = true; }

            if(!foundError){
                new registerRunningActivity_http().execute();
            }
        }
    }

    class registerRunningActivity_http extends AsyncTask<String, String, JSONObject>{

        @Override
        protected JSONObject doInBackground(String... strings) {
            Log.i(TAG, "registerRunningActivity_http");

            return new ServerAPI(getBaseContext()).serverRequestPOST( new Event_register(
                    addressPA.getPlace(),
                    descriptionCL.getValue(),
                    startEndTimeCL.getEndDatetime(),
                    hobbiesCL.getValue(),
                    eventNameCL.getValue(),
                    placeNameCL.getValue(),
                    reachCL.getValue(),
                    startEndTimeCL.getEndDatetime()
            ));
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            try {
                if(Toasts.responseIsGoodToaster(jsonObject, AddEventActivity.this)) {
                    switch (jsonObject.getString(APIRequest.message)){
                        case Event_register.Response.eventCreated :
                                Motherbase.getInstance(getBaseContext()).getEventTable().insertOrReplaceEventFromMySQL(jsonObject.getJSONObject(Event_register.ResponseFields.event));
                                SharedPreferencesUtils.getInstance(getBaseContext()).put(SharedPreferencesUtils.PrefKeys.EventID_int, jsonObject.getJSONObject(Event_register.ResponseFields.event).getInt(MySQLColumns.EventID));
                                Intent intent = new Intent(getBaseContext(), InEventMoreInfoActivity.class);
                                intent.putExtra(MySQLColumns.EventID, jsonObject.getJSONObject(Event_register.ResponseFields.event).getInt(MySQLColumns.EventID));
                                startActivity(intent);
                                finish();
                        break;

                        case Event_register.Response.userAlreadyInEvent :

                            break;
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}

