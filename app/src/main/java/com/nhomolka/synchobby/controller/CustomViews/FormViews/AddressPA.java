package com.nhomolka.synchobby.controller.CustomViews.FormViews;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.widget.TextView;

import com.google.android.gms.location.places.Place;
import com.nhomolka.synchobby.R;

import java.util.ArrayList;
import java.util.List;

public class AddressPA extends ConstraintLayout{
    private static final String TAG = "##AddressPA";

    ErrorAndTitlesTextViews errorAndTitlesTextViews;
    TextView placeAutocompleteTV;
    CustomPlaceAutoCompleteFragment owner;

    enum FieldError {
        empty("Address Cannot Be Empty.");
        private String value;
        public String getValue() { return this.value; }
        FieldError(String value){ this.value = value; }
    }

    public AddressPA(Context context) {
        super(context);
    }

    public AddressPA(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void customInit(CustomPlaceAutoCompleteFragment placeAutocompleteFragment){
        this.owner = placeAutocompleteFragment;
        errorAndTitlesTextViews = new ErrorAndTitlesTextViews(getContext(), this, owner.getView(), "Address");
        errorAndTitlesTextViews.hideError();
        placeAutocompleteTV = placeAutocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input);
        placeAutocompleteTV.setTextSize(14);
    }

    public boolean findError(){

        boolean foundError = false;
        List<FieldError> errors = new ArrayList<>();

        if(owner.getPlace() == null){
            foundError = true;
            errors.add(FieldError.empty);
        }

        if(foundError){
            errorAndTitlesTextViews.errorTV.setVisibility(VISIBLE);
            errorAndTitlesTextViews.errorTV.setText(errors.get(0).value);
        }else{
            errorAndTitlesTextViews.errorTV.setVisibility(GONE);
        }
        return foundError;
    }

    public Place getPlace(){

        return owner.getPlace();

    }
}
