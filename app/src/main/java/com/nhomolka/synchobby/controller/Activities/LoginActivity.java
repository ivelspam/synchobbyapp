package com.nhomolka.synchobby.controller.Activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nhomolka.synchobby.BuildConfig;
import com.nhomolka.synchobby.R;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Login_email;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Login_facebook;
import com.nhomolka.synchobby.model.ServerRequest.ServerAPI;
import com.nhomolka.synchobby.model.SharedPreferencesUtils;
import com.nhomolka.synchobby.model.Toasts;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;
import android.app.ProgressDialog;

import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "##LoginActivity";
    private static final int REQUEST_SIGNUP = 0;
    CallbackManager callbackManager;
    LoginButton facebookLoginButton;
    String facebookUserId, facebookUserToken;
    ProgressDialog progressDialog;
    EditText vEmailET, vPasswordET;
    TextView vSignupTV, vBuildDateTV, vBuildVersionTV;
    Button vLoginBT;
    LinearLayout llVertical;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        view_build();
    }

    private void view_build(){

        vEmailET = findViewById(R.id.input_email);
        vPasswordET = findViewById(R.id.input_password);
        vLoginBT = findViewById(R.id.activityLogin_btLogin);
        facebookLoginButton = findViewById(R.id.activityLogin_facebookLoginButton);
        vSignupTV = findViewById(R.id.link_signup);
        llVertical = findViewById(R.id.llVertical);
        vBuildVersionTV = findViewById(R.id.activityLogin_tvBuildVersion);
        vBuildDateTV = findViewById(R.id.activityLogin_tvBuildDate);

        view_btLogin();
        view_tvSignup();
        view_facebookLoginButton();
        view_setupVersion();
    }

    void view_setupVersion(){

        Date buildDate = new Date(BuildConfig.TIMESTAMP);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd-yy HH:mm");
        vBuildDateTV.setText("BUILD DATE: " + simpleDateFormat.format(buildDate));
        try {
            String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            vBuildVersionTV.setText("VERSION: " +versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void view_btLogin(){
        vLoginBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "Login");

                if (!validate()) {
                    errorToast("Fill All Correctly");
                    return;
                }
                vLoginBT.setEnabled(false);
                progressDialog = new ProgressDialog(LoginActivity.this);
                progressDialog.setMessage("Authenticating...");
                progressDialog.show();
                // TODO: Implement your own authentication logic here.
                new CheckEmailLogin_http().execute();
            }
        });
    }

    private void view_facebookLoginButton(){
        callbackManager = CallbackManager.Factory.create();
        facebookLoginButton.setLoginBehavior(LoginBehavior.WEB_VIEW_ONLY);
        facebookLoginButton.setReadPermissions("email", "public_profile", "user_birthday");
        facebookLoginButton.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        Log.i(TAG, "onSuccess");
                        llVertical.setVisibility(View.GONE);
                        facebookUserId = loginResult.getAccessToken().getUserId();
                        facebookUserToken = loginResult.getAccessToken().getToken();

                        progressDialog = new ProgressDialog(LoginActivity.this);
                        progressDialog.setMessage("Authenticating...");
                        progressDialog.show();

                        GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback(){
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response){
                            new CheckFacebookToken_http().execute();
                            }
                        });

//                        Bundle parameters = new Bundle();
//                        parameters.putString("fields", "first_name, last_name, email, id");
//                        graphRequest.setParameters(parameters);
                        graphRequest.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Log.i(TAG, "onCancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.i(TAG, "onError");
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult");

        Log.i(TAG, Integer.toString(requestCode));


        if (requestCode == REQUEST_SIGNUP) {
            Log.i(TAG, "REQUEST_SIGNUP");
            if (resultCode == RESULT_OK) {
                Log.i(TAG, "RESULT_OK");
                this.finish();
            }
        }else if(FacebookSdk.getCallbackRequestCodeOffset() == requestCode){
            super.onActivityResult(requestCode, resultCode, data);
            Log.i(TAG, "FacebookSdk.getCallbackRequestCodeOffset()");

            callbackManager.onActivityResult(requestCode,
                    resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    public void onLoginSuccess(JSONObject loginInformation) throws JSONException {
        SharedPreferencesUtils.getInstance(this).put(SharedPreferencesUtils.PrefKeys.Token_str, loginInformation.getString("token"));
        Intent intent = new Intent(this, TransactionLoginScreenActivity.class);
        startActivity(intent);
        finish();
    }

    public void errorToast(String Message) {
        Toast.makeText(getBaseContext(), "Login failed : " + Message, Toast.LENGTH_LONG).show();
        LoginManager.getInstance().logOut();
        llVertical.setVisibility(View.VISIBLE);
        vLoginBT.setEnabled(true);
    }

    private void view_tvSignup(){
        vSignupTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
            }
        });
    }

    public boolean validate() {
        boolean valid = true;

        String email = vEmailET.getText().toString();
        String password = vPasswordET.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            vEmailET.setError("enter a valid email address");
            valid = false;
        } else {
            vEmailET.setError(null);
        }

        if (password.isEmpty() || password.length() < 4) {
            vPasswordET.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            vPasswordET.setError(null);
        }
        return valid;
    }


    public class CheckEmailLogin_http extends AsyncTask<String, String, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... params) {
            return new ServerAPI(getApplicationContext()).serverRequestPOST(new Login_email( vEmailET.getText().toString(), vPasswordET.getText().toString()));
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {

            progressDialog.dismiss();

            try {

                if(Toasts.responseIsGoodToaster(jsonObject, getBaseContext())){
                    switch(jsonObject.getString(APIRequest.message)){
                        case Login_email.Response.match:
                            onLoginSuccess(jsonObject);
                            break;
                        default:
                            errorToast("Password and Email don't match");
                    }
                }else{
                    vLoginBT.setEnabled(true);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public class CheckFacebookToken_http extends AsyncTask<String, String, JSONObject> {
        @Override
        protected JSONObject doInBackground(String... params) {
            return new ServerAPI(getApplicationContext()).serverRequestPOST(new Login_facebook(facebookUserId, facebookUserToken));
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            progressDialog.dismiss();

            try {
                if(Toasts.responseIsGoodToaster(jsonObject, getBaseContext())){
                    switch (jsonObject.getString(APIRequest.message)){
                        case Login_facebook.Response.logged:
                            onLoginSuccess(jsonObject);
                            break;
                        default:
                            errorToast("Password and Email");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}

