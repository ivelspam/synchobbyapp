package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;

import java.io.File;

public class Upload_profilePicture extends APIRequest {
    public File mFile;
    public UploadType mUploadType;

    public class ResponseFields {

    }

    public class Param {

    }

    public class Response {

    }

    public enum UploadType {
        profileImage("profileImage");
        private String value;
        public String getValue() { return this.value; }
        UploadType(String value){ this.value = value; }
    }

    public Upload_profilePicture(File file, UploadType uploadType){

        mFile = file;
        mUploadType = uploadType;
        APIUrl = APIValues.Links.upload_profilePicture;

    }


}
