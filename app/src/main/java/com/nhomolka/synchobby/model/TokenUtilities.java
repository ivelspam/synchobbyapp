package com.nhomolka.synchobby.model;

import android.content.Context;
import com.nhomolka.synchobby.model.SharedPreferencesUtils;

public class TokenUtilities {
    static public String getToken(Context context){
        return SharedPreferencesUtils.getInstance(context).getString(SharedPreferencesUtils.PrefKeys.Token_str);
    }
}
