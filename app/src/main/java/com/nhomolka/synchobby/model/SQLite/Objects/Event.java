package com.nhomolka.synchobby.model.SQLite.Objects;

import android.database.Cursor;

import com.nhomolka.synchobby.model.Extensions.ExtensionsHack;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.Constants.SQLiteColumns;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class Event implements Cloneable{
    private static final String TAG = "##Event";

    String Address;
    int AppUserID;
    String AppUserName;
    String ClientUpdatedTS;
    String CreatedTime;
    String Description;
    String EndDatetime;
    int EventID;
    int HobbyID;
    String HobbyName;
    Double Latitude;
    Double Longitude;
    String Name;
    String PlaceName;
    int QuantityOfPeople;
    int Reach;
    int ReceivedTS;
    EventState State;
    String StartDatetime;
    String UpdatedTS;
    boolean Hidden;

    public enum EventState {
        NoState(-1),
        Created(0),
        Ended(1),
        Deleted(2);

        private int value;
        private static Map map = new HashMap<>();

        private EventState(int value) {
            this.value = value;
        }

        static {
            for (EventState pageType : EventState.values()) {
                map.put(pageType.value, pageType);
            }
        }

        public static EventState valueOf(int pageType) {
            return (EventState) map.get(pageType);
        }

        public int getValue() {
            return value;
        }

    }

    public Object clone()throws CloneNotSupportedException{
        return super.clone();
    }

    public Event(Cursor cursor){
        Address = cursor.getString(cursor.getColumnIndex(MySQLColumns.Address));
        AppUserID = cursor.getInt(cursor.getColumnIndex(MySQLColumns.AppUserID));
        AppUserName = cursor.getString(cursor.getColumnIndex(MySQLColumns.AppUserName));
        ClientUpdatedTS = cursor.getString(cursor.getColumnIndex(SQLiteColumns.ClientUpdatedTS));
        Description = cursor.getString(cursor.getColumnIndex(MySQLColumns.Description));
        EventID = cursor.getInt(cursor.getColumnIndex(MySQLColumns.EventID));
        EndDatetime = cursor.getString(cursor.getColumnIndex(MySQLColumns.EndDatetime));
        Hidden = ExtensionsHack.BooleanIntToPrint(cursor.getInt(cursor.getColumnIndex(MySQLColumns.Hidden)));
        HobbyID  = cursor.getInt(cursor.getColumnIndex(MySQLColumns.HobbyID));
        HobbyName = cursor.getString(cursor.getColumnIndex(MySQLColumns.HobbyName));
        Latitude = cursor.getDouble(cursor.getColumnIndex(MySQLColumns.Latitude));
        Longitude = cursor.getDouble(cursor.getColumnIndex(MySQLColumns.Longitude));
        Name = cursor.getString(cursor.getColumnIndex(MySQLColumns.Name));
        PlaceName = cursor.getString(cursor.getColumnIndex(MySQLColumns.PlaceName));
        QuantityOfPeople = cursor.getInt(cursor.getColumnIndex(MySQLColumns.QuantityOfPeople));
        Reach = cursor.getInt(cursor.getColumnIndex(MySQLColumns.Reach));
        ReceivedTS = cursor.getInt(cursor.getColumnIndex(SQLiteColumns.ReceivedTS));
        State = EventState.valueOf(cursor.getInt(cursor.getColumnIndex(MySQLColumns.State)));
        StartDatetime = cursor.getString(cursor.getColumnIndex(MySQLColumns.StartDatetime));
        UpdatedTS = cursor.getString(cursor.getColumnIndex(MySQLColumns.UpdatedTS));
    }

    public String getName() {
        return Name;
    }
    public int getEventID() {
        return EventID;
    }
    public void setName(String eventName) {
        this.Name = eventName;
    }
    public int getHobbyID() {
        return HobbyID;
    }
    public void setHobbyID(int hobbyID) {
        this.HobbyID = hobbyID;
    }
    public String getPlaceName() {
        return PlaceName;
    }
    public String getAddress() {
        return Address;
    }
    public String getHobbyName() {
        return HobbyName;
    }
    public String getHobbyNameTitleCase() { return ExtensionsHack.StringToTitleCase(HobbyName); }
    public void setAppUserID(int appUserID) {
        AppUserID = appUserID;
    }
    public String getClientUpdatedTS() {
        return ClientUpdatedTS;
    }
    public String getCreatedTime() {
        return CreatedTime;
    }
    public int getReceivedTS() {
        return ReceivedTS;
    }
    public Double getLatitude() {
        return Latitude;
    }
    public Double getLatitudeDouble() { return Latitude;}
    public Double getLongitude() {
        return Longitude;
    }
    public int getQuantityOfPeople() { return QuantityOfPeople; }
    public int getReach() {
        return Reach;
    }
    public String getDescription() {
        return Description;
    }
    public String getAppUserName() {
        return AppUserName;
    }
    public String getEndDatetime() {
        return EndDatetime;
    }
    public String getStartDatetime() {
        return StartDatetime;
    }
    public int getAppUserID() {return AppUserID;}
    public void setState(EventState state) {State = state;}
    public void setQuantityOfPeople(int quantityOfPeople) {
        QuantityOfPeople = quantityOfPeople;
    }
    public EventState getState() {return State;}
    public void incrementQuantityOfPeople(){ QuantityOfPeople++;}
    public void decrementQuantityOfPeople(){
        QuantityOfPeople--;
    }

    public boolean isHidden() {
        return Hidden;
    }

    public void setHidden(boolean hidden) {
        Hidden = hidden;
    }

    public String getUpdatedTS() {
        return UpdatedTS;
    }



    @Override
    public String toString() {
        StringBuilder stringBuilder =  new StringBuilder()
                .append(getAppend(MySQLColumns.Address, Address))
                .append(getAppend(MySQLColumns.AppUserID, AppUserID))
                .append(getAppend(MySQLColumns.AppUserName, AppUserID))
                .append(getAppend(MySQLColumns.Description, Description))
                .append(getAppend(MySQLColumns.EndDatetime, EndDatetime))
                .append(getAppend(MySQLColumns.EventID, EventID))
                .append(getAppend(MySQLColumns.HobbyID, HobbyID))
                .append(getAppend(MySQLColumns.HobbyName, HobbyName))
                .append(getAppend(MySQLColumns.Latitude, Double.toString(Latitude)))
                .append(getAppend(MySQLColumns.Longitude, Double.toString(Longitude)))
                .append(getAppend(MySQLColumns.Name, Name))
                .append(getAppend(MySQLColumns.PlaceName, PlaceName))
                .append(getAppend(MySQLColumns.QuantityOfPeople, QuantityOfPeople))
                .append(getAppend(MySQLColumns.Reach, Reach))
                .append(getAppend(MySQLColumns.StartDatetime, StartDatetime))
                .append(getAppend(MySQLColumns.State, State.getValue())
                );

        stringBuilder.setLength(stringBuilder.length() - 1);

        stringBuilder.insert(0, "{");
        stringBuilder.append("}");
        return stringBuilder.toString();
    }

    String getAppend(String paramName, int value){
        return paramName + ":"+ value + ",";
    }

    String getAppend(String paramName, String value){
        return paramName + ":\""+ value + "\",";
    }

    public String getPostBody(){
        return  "&Name=" + Name +
                "&HobbyID=" + HobbyID +
                "&Address=" + Address +
                "&Latitude=" + Latitude +
                "&Longitude=" + Longitude +
                "&PlaceName=" + PlaceName +
                "&Reach=" + Reach +
                "&StartDatetime=" + StartDatetime +
                "&EndDatetime=" + EndDatetime +
                "&Description=" + Description;
    }

    public String[] getErrors(){
        return null;
    }

}


//    public Event(JSONObject jsonObject) {
//        try {
//            Address = jsonObject.getString(MySQLColumns.Address);
//            AppUserID = jsonObject.getInt(MySQLColumns.AppUserID);
//            AppUserName = jsonObject.getString(MySQLColumns.AppUserName);
//            Description = jsonObject.getString(MySQLColumns.Description);
//            EndDatetime = MySQLUtilities.mySQLToDefaultPattern(jsonObject.getString(MySQLColumns.EndDatetime));
//            EventID = jsonObject.getInt(MySQLColumns.EventID);
//            HobbyID  = jsonObject.getInt(MySQLColumns.HobbyID);
//            HobbyName = jsonObject.getString(MySQLColumns.HobbyName);
//            Latitude = jsonObject.getDouble(MySQLColumns.Latitude);
//            Longitude = jsonObject.getDouble(MySQLColumns.Longitude);
//            Name = jsonObject.getString(MySQLColumns.Name);
//            PlaceName = jsonObject.getString(MySQLColumns.PlaceName);
//            QuantityOfPeople = jsonObject.getInt(MySQLColumns.QuantityOfPeople);
//            Reach = jsonObject.getInt(MySQLColumns.Reach);
//            StartDatetime = MySQLUtilities.mySQLToDefaultPattern(jsonObject.getString(MySQLColumns.StartDatetime));
//            State = jsonObject.getInt(MySQLColumns.State);
//            UpdatedTS = MySQLUtilities.mySQLToDefaultPattern(jsonObject.getString(MySQLColumns.UpdatedTS));
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }


//    public CreateEvent(String name, int hobbyID, Place place, String placeName, Calendar startTime, Calendar endTime, int reach, String description){
//
//        Name = name;
//        HobbyID = hobbyID;
//        Address = place.getAddress().toString();
//        PlaceName = placeName;
//        Latitude = place.getLatLng().latitude;
//        Longitude = place.getLatLng().longitude;
//        StartDatetime = ExtensionsHack.CalendarToSQLiteTS(startTime);
//        EndDatetime = ExtensionsHack.CalendarToSQLiteTS(endTime);
//        Reach = reach;
//        Description = description;
//
//    }