package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;

public class Login_facebook extends APIRequest{

    public class ResponseFields {

    }

    public class Param {
        public final static String
                FacebookUserID = "FacebookUserID",
                facebookUserToken = "facebookUserToken";
    }

    public class Response {
        public final static String
                logged = "logged";
    }

    public Login_facebook(String facebookUserID, String facebookuserToken){
        token = false;
        keyValues.put(Param.FacebookUserID, facebookUserID);
        keyValues.put(Param.facebookUserToken, facebookuserToken);
        APIUrl = APIValues.Links.login_facebook;
    }

}
