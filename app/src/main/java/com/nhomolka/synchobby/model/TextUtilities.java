package com.nhomolka.synchobby.model;

import android.content.Context;
import android.util.Log;

import com.nhomolka.synchobby.model.SharedPreferencesUtils;

import java.text.DecimalFormat;

public class TextUtilities {
    private static final String TAG = "##TextUtilities";

    public static double distance(double lat1, double lat2, double lon1,
                                  double lon2, double el1, double el2) {
        Log.i(TAG, Double.toString(lat1) + " " + Double.toString(lat2) + " " +
                Double.toString(lon1) + " " + Double.toString(lon1) + " " +
                Double.toString(el1) + " " + Double.toString(el2));

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }


    public static String haversine(
            double lat1, double lng1, double lat2, double lng2, Context context) {
        String measureType = SharedPreferencesUtils.getInstance(context).getString(SharedPreferencesUtils.PrefKeys.MeasureSystem_str);
        int r;
        if(measureType.compareTo( "km") == 0){
            r = 6371;
        }else{
            r = 3959;
        }

        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                        * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = r * c;

        DecimalFormat df = new DecimalFormat("#");

        return df.format(d) + " " + measureType;
    }
}
