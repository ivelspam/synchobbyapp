package com.nhomolka.synchobby.model.SQLite.Objects;

import com.nhomolka.synchobby.model.Extensions.DateExtension;
import com.nhomolka.synchobby.model.Extensions.ExtensionsHack;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

public class EventComment {
    static private String TAG = "##EventComment";

    int AppUserID;
    String Comment;
    Calendar CreatedTS;
    int EventCommentID;
    int EventID;
    int ImageVersion;
    String Name;
    String UUID;

    public EventComment(JSONObject chatMessageJSONObject) throws JSONException {
        AppUserID = chatMessageJSONObject.getInt(MySQLColumns.AppUserID);
        EventCommentID = chatMessageJSONObject.getInt(MySQLColumns.EventCommentID);
        UUID = chatMessageJSONObject.getString(MySQLColumns.UUID);
        EventID = chatMessageJSONObject.getInt(MySQLColumns.EventID);
        Comment = chatMessageJSONObject.getString(MySQLColumns.Comment);

        CreatedTS =  DateExtension.CalendarMySQLDatetimeTZToCalendar(chatMessageJSONObject.getString(MySQLColumns.CreatedTS));
        Name = chatMessageJSONObject.getString(MySQLColumns.Name);
        ImageVersion = chatMessageJSONObject.getInt(MySQLColumns.ImageVersion);
    }

    public EventComment(int appUserID, String comment, int eventID, int imageVersion, String name) {
        EventID = eventID;
        Comment = comment;
        AppUserID = appUserID;
        UUID = java.util.UUID.randomUUID().toString();
        ImageVersion = imageVersion;
        Name = name;
        CreatedTS = Calendar.getInstance();
    }

    public String getJSONString(){

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(MySQLColumns.EventCommentID, EventCommentID);
            jsonObject.put(MySQLColumns.UUID, UUID);
            jsonObject.put(MySQLColumns.Comment, Comment);
            jsonObject.put(MySQLColumns.AppUserID, AppUserID);
            jsonObject.put(MySQLColumns.EventID, EventID);
            jsonObject.put(MySQLColumns.CreatedTS, DateExtension.CalendarToSQLiteTS(CreatedTS));
            jsonObject.put(MySQLColumns.Name, Name);
            jsonObject.put(MySQLColumns.ImageVersion, ImageVersion);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject.toString();

    }


    public int getEventCommentID() {
        return EventCommentID;
    }

    public void setEventCommentID(int eventCommentID) {
        EventCommentID = eventCommentID;
    }

    public String getName() {
        return Name;
    }

    public int getImageVersion() {
        return ImageVersion;
    }

    public String getComment() {
        return Comment;
    }

    public int getEventID() {
        return EventID;
    }

    public int getAppUserID() {
        return AppUserID;
    }

    public String getUUID() {
        return UUID;
    }

    public Calendar getCreatedTS() {
        return CreatedTS;
    }

    @Override
    public String toString() {
        return "EventComment{" +
                "EventCommentID=" + EventCommentID +
                ", EventID=" + EventID +
                ", AppUserID=" + AppUserID +
                ", UUID='" + UUID + '\'' +
                ", Comment='" + Comment + '\'' +
                ", UpdatedTS='" + CreatedTS + '\'' +
                ", ImageVersion='" + ImageVersion + '\'' +

                '}';
    }
}
