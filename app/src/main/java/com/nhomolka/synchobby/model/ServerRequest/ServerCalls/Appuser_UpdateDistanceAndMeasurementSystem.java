package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;

public class Appuser_UpdateDistanceAndMeasurementSystem extends APIRequest{

    public class ResponseFields {

    }

    public class Param {
        public final static String
                MeasureSystem  = MySQLColumns.MeasureSystem,
                Distance = MySQLColumns.Distance;
    }

    public class Response {
        public final static String
                updated = "updated",
                notUpdated = "notUpdated";
    }

    public enum Type {
        km("km"),
        mi("mi");

        private String value;
        public String getValue() { return this.value; }
        Type(String value){ this.value = value; }
    }
    public Appuser_UpdateDistanceAndMeasurementSystem(Type type, int distance){

        keyValues.put(Param.MeasureSystem, type.value);
        keyValues.put(Param.Distance, distance);
        APIUrl = APIValues.Links.appuser_UpdateDistanceAndMeasurementSystem;
    }

}
