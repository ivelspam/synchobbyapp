package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;

public class Email_isConfirmed extends APIRequest {

    public class ResponseFields {
        public final static String token = "token";
    }

    public class Param {

    }

    public class Response {
        public final static String  confirmed = "confirmed",
                                    notConfirmedYet = "notConfirmedYet";
    }

    public Email_isConfirmed(){
        APIUrl = APIValues.Links.email_isConfirmed;
    }
}
