package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;

public class Appuser_deleteAccount extends APIRequest {

    public class Param {}

    public class Response {
        public static final String
                accountDeleted = "accountDeleted";
    }

    public class ResponseFields {}

    public Appuser_deleteAccount(){
        APIUrl = APIValues.Links.appuser_deleteAccount;
    }
}
