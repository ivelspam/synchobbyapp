package com.nhomolka.synchobby.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.auth0.android.jwt.JWT;
import com.nhomolka.synchobby.model.Constants.TokenValues;
import com.nhomolka.synchobby.model.Extensions.DateExtension;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.AllInfo_getAllInfo;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.AllInfo_getUpdates;
import com.nhomolka.synchobby.model.Constants.MagicNumbers;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class SharedPreferencesUtils {

    private static final String SETTINGS_NAME = "userInfo",
                                TAG = "##SharedPreferencesU";

    static private SharedPreferencesUtils sSharedPreferencesUtils;
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;
    private boolean mBulkUpdate = false;

    public static SharedPreferencesUtils getInstance(Context context){

        if(sSharedPreferencesUtils == null){
            sSharedPreferencesUtils = new SharedPreferencesUtils(context.getApplicationContext());
        }
        return sSharedPreferencesUtils;

    }

    private SharedPreferencesUtils(Context context){
        mSharedPreferences = context.getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE);
    }

    public enum PrefKeys{
        AppUserID_int,
        Token_str,
        Name_str,
        Email_str,
        dob_str,
        Username_str,
        Distance_int,
        MeasureSystem_str,
        EventID_int,
        ImageVersion_int,
        UpdatedTS_str,
        latestChatID_str,
        eventUpdatedTS_str,
        inEventUpdatedTS_str,
        friendUpdatedTS_str
    }


    //PUTTERS
    public void put(PrefKeys prefKeys, String val) {
        doEdit();
        mEditor.putString(prefKeys.name(), val);
        doCommit();
    }

    public void put(PrefKeys prefKeys, int val) {
        doEdit();
        mEditor.putInt(prefKeys.name(), val);
        doCommit();
    }

    //GETTERS
    public String getString(PrefKeys prefKeys) {
        return mSharedPreferences.getString(prefKeys.name(), null);
    }

    public String getString(PrefKeys prefKeys, String defValeu) {
        return mSharedPreferences.getString(prefKeys.name(), defValeu);
    }

    public int getInt(PrefKeys prefKeys) {
        return mSharedPreferences.getInt(prefKeys.name(), -1);
    }

    public int getInt(PrefKeys prefKeys, int defValue) {
        return mSharedPreferences.getInt(prefKeys.name(), defValue);
    }

    public void clear() {
        doEdit();
        mEditor.clear();
        doCommit();
    }

    public boolean contains(PrefKeys prefKey){
        return mSharedPreferences.contains(prefKey.name());
    }


    public void delete(PrefKeys prefKeys) {
        doEdit();
        mEditor.remove(prefKeys.name());
        doCommit();

    }

    private void doEdit() {
        if (!mBulkUpdate && mEditor == null) {
            mEditor = mSharedPreferences.edit();
        }
    }

    private void doCommit() {
        if (!mBulkUpdate && mEditor != null) {
            mEditor.commit();
            mEditor = null;
        }
    }

    public String toString(){


        Map<String,?> keys = mSharedPreferences.getAll();
        StringBuilder stringBuilder = new StringBuilder("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
        stringBuilder.append("SharedPreferences Values: \n");

        for(Map.Entry<String,?> entry : keys.entrySet()){
            stringBuilder.append(entry.getKey() + ": " + entry.getValue().toString() + "\n");
        }
        stringBuilder.append("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        return stringBuilder.toString();
    }


    public void  updateUpdatedTS(JSONObject jsonValue, JSONArray jsonArray, PrefKeys prefKey){

        Log.i(TAG, "updateUpdatedTS");
        try {
            switch (prefKey){
                case inEventUpdatedTS_str:
                    if(jsonValue != null){
                        updateHighestTS(prefKey, DateExtension.CalendarMySQLToSQLite(jsonValue.getString(MySQLColumns.UpdatedTS)));
                    }else{
                        updateHighestTS(prefKey,  DateExtension.CalendarMySQLToSQLite(MagicNumbers.LOWEST_TS));
                    }
                    break;
                case UpdatedTS_str:
                    if(jsonValue != null){
                        updateHighestTS(prefKey, DateExtension.CalendarMySQLToSQLite(jsonValue.getString(MySQLColumns.UpdatedTS)));
                    }else{
                        updateHighestTS(prefKey,  DateExtension.CalendarMySQLToSQLite(MagicNumbers.LOWEST_TS));
                    }
                    break;
                case latestChatID_str:
                    int value = -1;

                    if(jsonArray != null){
                        for(int i = 0; i<jsonArray.length(); i++){
                            if(value < jsonArray.getJSONObject(i).getInt(MySQLColumns.ChatID)){
                                value = jsonArray.getJSONObject(i).getInt(MySQLColumns.ChatID);
                            }
                        }
                    }

                    updateHighestID(prefKey, value);
                    break;
                default:
                    String updatedTS = MagicNumbers.LOWEST_MYSQL_TS;
                    if(jsonArray != null){
                        for(int i = 0; i<jsonArray.length(); i++){
                            if(!DateExtension.CalendarCompareTwoMySQLTSFirstBiggerThanSecond(updatedTS, jsonArray.getJSONObject(i).getString(MySQLColumns.UpdatedTS))){
                                updatedTS = jsonArray.getJSONObject(i).getString(MySQLColumns.UpdatedTS);
                            }
                        }
                    }

                    updateHighestTS(prefKey, DateExtension.CalendarMySQLToSQLite(updatedTS));

                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void updateHighestID(PrefKeys prefKey, int value){
        if(contains(prefKey)){
            if(getInt(prefKey) < value){
                put(prefKey, value);
            }
        }else{
            put(prefKey, value);
        }
    }

    private void updateHighestTS(PrefKeys prefKey, String value){
        if(contains(prefKey)){
            if(!DateExtension.CalendarCompareTwoSQLiteTSFirstBiggerThanSecond(getString(prefKey), value)){
                put(prefKey, value);
            }
        }else{
            put(prefKey, value);
        }
    }

    public void updateUser(JSONObject jsonUser) throws JSONException {
        Log.i(TAG, "updateUser");

        if(jsonUser.has(AllInfo_getUpdates.ResponseFields.AppUserChange)){

            Log.i(TAG, jsonUser.getJSONObject(AllInfo_getUpdates.ResponseFields.AppUserChange).toString());

            JSONObject jsonObject = jsonUser.getJSONObject(AllInfo_getUpdates.ResponseFields.AppUserChange);
            put(SharedPreferencesUtils.PrefKeys.Distance_int, jsonObject.getInt(MySQLColumns.Distance));
            put(SharedPreferencesUtils.PrefKeys.dob_str, jsonObject.getString(MySQLColumns.DOB));
            put(SharedPreferencesUtils.PrefKeys.Email_str, jsonObject.getString(MySQLColumns.Email));
            if(!jsonUser.isNull(MySQLColumns.EventID)){
                put(SharedPreferencesUtils.PrefKeys.EventID_int, jsonObject.getInt(MySQLColumns.EventID));
            }else{
                delete(SharedPreferencesUtils.PrefKeys.EventID_int);
            }
            put(SharedPreferencesUtils.PrefKeys.ImageVersion_int, jsonObject.getInt(MySQLColumns.ImageVersion));
            put(SharedPreferencesUtils.PrefKeys.MeasureSystem_str, jsonObject.getString(MySQLColumns.MeasureSystem));
            put(SharedPreferencesUtils.PrefKeys.Name_str, jsonObject.getString(MySQLColumns.Name));
            put(SharedPreferencesUtils.PrefKeys.Username_str, jsonObject.getString(MySQLColumns.Username));
            put(SharedPreferencesUtils.PrefKeys.UpdatedTS_str, jsonObject.getString(MySQLColumns.UpdatedTS));
        }
    }

    public void buildUser(JSONObject resultJSONObject) throws JSONException {
        Log.i(TAG, "buildUser");

        if(resultJSONObject.has(AllInfo_getAllInfo.ResponseFields.appUserInfo)){
            JSONObject jsonUser = resultJSONObject.getJSONObject(AllInfo_getAllInfo.ResponseFields.appUserInfo);

            String token = getString(SharedPreferencesUtils.PrefKeys.Token_str);
            JWT jwt = new JWT(token);

            put(PrefKeys.AppUserID_int, jwt.getClaim(TokenValues.AppUserID).asInt());
            put(PrefKeys.Distance_int, jsonUser.getInt(MySQLColumns.Distance));
            put(PrefKeys.dob_str, jsonUser.getString(MySQLColumns.DOB));
            put(PrefKeys.Email_str, jsonUser.getString(MySQLColumns.Email));

            if(!jsonUser.isNull(MySQLColumns.EventID)){
                put(SharedPreferencesUtils.PrefKeys.EventID_int, jsonUser.getInt(MySQLColumns.EventID));
            }else{
                delete(SharedPreferencesUtils.PrefKeys.EventID_int);
            }

            put(PrefKeys.ImageVersion_int, jsonUser.getInt(MySQLColumns.ImageVersion));
            put(PrefKeys.MeasureSystem_str, jsonUser.getString(MySQLColumns.MeasureSystem));
            put(PrefKeys.Name_str, jsonUser.getString(MySQLColumns.Name));
            put(PrefKeys.UpdatedTS_str, DateExtension.CalendarMySQLToSQLite(jsonUser.getString(MySQLColumns.UpdatedTS)));
            put(PrefKeys.Username_str, jsonUser.getString(MySQLColumns.Username));
        }
    }

}
