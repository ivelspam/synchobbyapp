package com.nhomolka.synchobby.model;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.nhomolka.synchobby.R;

import static android.content.Context.NOTIFICATION_SERVICE;

public class NotificationUtils {

    public void createActivityNotification(int uniqueID, Activity activity, Class<?> temp){
        Intent intent = new Intent(activity, temp);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(activity, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notification = new NotificationCompat.Builder(activity, NotificationChannel.DEFAULT_CHANNEL_ID)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic__group)
                .setContentTitle("New Event")
                .setContentTitle("THIS IS THE KIND OF ACTIVITY")
                .setTicker("This is the ticker");

        NotificationManager  notificationManager  = (NotificationManager) activity.getSystemService(NOTIFICATION_SERVICE );
        notificationManager.notify(uniqueID, notification.build());
    }

}
