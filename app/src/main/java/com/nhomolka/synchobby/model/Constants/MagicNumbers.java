package com.nhomolka.synchobby.model.Constants;

public class MagicNumbers {

    public static final String LOWEST_TS = "1970-01-01 00:00:01.000";
    public static final String LOWEST_MYSQL_TS = "1970-01-01T00:00:01.000Z";
    public static final int NOT_IN_EVENT = -1;
    public static final int NO_PICTURE = -1;
    public static final int NULL_PARENT_ID = -1;
    public static final int NO_ID_FOUND = -1;




}


