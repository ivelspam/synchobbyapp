package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;

public class Friend_accept extends APIRequest{

    public class Param {
        public static final String AppUserID = MySQLColumns.AppUserID;
    }

    public class Response {
        public static final String  accepted = "accepted",
                                    notPossible = "notPossible";

    }

    public class ResponseFields {

    }

    public Friend_accept(int appUserID){
        keyValues.put(Param.AppUserID, appUserID);
        APIUrl = APIValues.Links.friend_accept;

    }
}
