package com.nhomolka.synchobby.model.SQLite;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.nhomolka.synchobby.model.SQLite.Objects.Hobby;
import com.nhomolka.synchobby.model.SQLite.Tables.AppUserHobbyTable;
import com.nhomolka.synchobby.model.SQLite.Tables.ChatTable;
import com.nhomolka.synchobby.model.SQLite.Tables.EventTable;
import com.nhomolka.synchobby.model.SQLite.Tables.FriendTable;
import com.nhomolka.synchobby.model.SQLite.Tables.HobbyClosureTable;
import com.nhomolka.synchobby.model.SQLite.Tables.HobbyTable;

import java.util.ArrayList;
import java.util.List;

public class Motherbase extends SQLiteOpenHelper {
    static private String TAG = "##Motherbase";

    private static Motherbase sMotherbase;

    private static final String  DATABASE_NAME = "synchobby.db",
            TABLE_NAME = "chatMessage";

    Context mContext;
    private Motherbase(Context context) {
        super(context, DATABASE_NAME, null, 1);
        mContext = context;
    }

    public static Motherbase getInstance(Context context) {
        if (sMotherbase == null) {
            sMotherbase = new Motherbase(context.getApplicationContext());
        }
        return sMotherbase;
    }

    public FriendTable getFriendTable(){
        return FriendTable.getInstance(getWritableDatabase());
    }

    public ChatTable getChatMessageTable(){
        return ChatTable.getInstance(getWritableDatabase());
    }

    public HobbyClosureTable getHobbyClosureTable(){
        return HobbyClosureTable.getInstance(getWritableDatabase());
    }

    public HobbyTable getHobbyTable(){
        return HobbyTable.getInstance(getWritableDatabase());
    }

    public AppUserHobbyTable getAppUserHobby(){
        return AppUserHobbyTable.getInstance(getWritableDatabase());
    }

    public EventTable getEventTable(){
        return EventTable.getInstance(getWritableDatabase());
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
//        Log.i(TAG, "onCreate");
//        database.execSQL("DROP TABLE IF EXISTS " + FriendTable.TABLE_NAME);
//        database.execSQL("DROP TABLE IF EXISTS " + ChatTable.TABLE_NAME);
//        database.execSQL("DROP TABLE IF EXISTS " + EventTable.TABLE_NAME);
//        database.execSQL("DROP TABLE IF EXISTS " + HobbyTable.TABLE_NAME);
//        database.execSQL("DROP TABLE IF EXISTS " + HobbyClosureTable.TABLE_NAME);
//        database.execSQL("DROP TABLE IF EXISTS " + AppUserHobbyTable.TABLE_NAME);


        database.execSQL(FriendTable.TABLE_CREATION);
        database.execSQL(ChatTable.TABLE_CREATION);
        database.execSQL(EventTable.TABLE_CREATION);
        database.execSQL(HobbyTable.TABLE_CREATION);
        database.execSQL(HobbyClosureTable.TABLE_CREATION);
        database.execSQL(AppUserHobbyTable.TABLE_CREATION);

    }

    public void createTableIfNotExists(){

    }

    public void resetDatabase(){
        onCreate(getWritableDatabase());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        createTableIfNotExists();
    }

    public List<Hobby> getOnChilds(){
        List<Hobby> hobbies = new ArrayList<>();
        Cursor cursor = getWritableDatabase().rawQuery("SELECT * FROM hobby WHERE Type='child'", null);
        while(cursor.moveToNext()){
            Hobby hobby = new Hobby(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
            hobbies.add(hobby);
        }
        return hobbies;
    }

    public void rebuildDatabase(){
        AppUserHobbyTable.getInstance(getWritableDatabase()).rebuildTable();
        ChatTable.getInstance(getWritableDatabase()).rebuildTable();
        EventTable.getInstance(getWritableDatabase()).rebuildTable();
        FriendTable.getInstance(getWritableDatabase()).rebuildTable();
        HobbyTable.getInstance(getWritableDatabase()).rebuildTable();
        HobbyClosureTable.getInstance(getWritableDatabase()).rebuildTable();
    }

    public void clearDatabaseForNewInfo(){
        ChatTable.getInstance(getWritableDatabase()).rebuildTable();
        FriendTable.getInstance(getWritableDatabase()).rebuildTable();
        EventTable.getInstance(getWritableDatabase()).rebuildTable();
        AppUserHobbyTable.getInstance(getWritableDatabase()).rebuildTable();
    }

    public void restartDatabase(){
        rebuildDatabase();
    }


    public List<Hobby> getChildsFromAncestor(int ancestor){
        List<Hobby> hobbies = new ArrayList<>();

        Cursor cursor = getWritableDatabase().rawQuery("SELECT c. * FROM hobby c\n" +
                "JOIN\n" +
                "    hobbyclosure cc ON (c.HobbyID = cc.Descendant)\n" +
                "WHERE\n" +
                "    cc.ancestor = "+ ancestor + " AND depth=1", null);

        while(cursor.moveToNext()){
            Hobby hobby = new Hobby(cursor.getInt(0), cursor.getString(1), cursor.getString(2));

            hobbies.add(hobby);
        }
        return hobbies;
    }

    public List<Hobby> getParentsNodes(){
        List<Hobby> hobbies = new ArrayList<>();

        Cursor cursor = getWritableDatabase().rawQuery("SELECT * FROM hobby WHERE NOT EXISTS " +
                "( SELECT 1 FROM hobbyclosure WHERE Descendant = HobbyID AND depth > 0 LIMIT 1);", null);

        while(cursor.moveToNext()){
            Hobby hobby = new Hobby(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
            hobbies.add(hobby);
        }
        return hobbies;
    }





    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("ChatTable: \n");
        stringBuilder.append(ChatTable.getInstance(getWritableDatabase()).toString() + "\n");
        stringBuilder.append("FriendTable: \n");
        stringBuilder.append(FriendTable.getInstance(getWritableDatabase()).toString() + "\n");
        stringBuilder.append("EventTable: \n");
        stringBuilder.append(EventTable.getInstance(getWritableDatabase()).toString() + "\n");
        stringBuilder.append("HobbyTable: \n");
        stringBuilder.append(HobbyTable.getInstance(getWritableDatabase()).toString() + "\n");
        stringBuilder.append("HobbyClosureTable: \n");
        stringBuilder.append(HobbyClosureTable.getInstance(getWritableDatabase()).toString() + "\n");

        return stringBuilder.toString();
    }

    public static String printCursor(Cursor cursor){
        return DatabaseUtils.dumpCursorToString(cursor);
   }
}
