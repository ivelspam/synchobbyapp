package com.nhomolka.synchobby.model.SQLite;

import android.content.Context;

import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.AllInfo_getAllInfo;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.AllInfo_getUpdates;
import com.nhomolka.synchobby.model.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class ServerRequestUpdateSQLiteMethods {

    private static final String TAG = "##ServerRequestUpdateSQ";

    public static void insertOrReplaceFriends(JSONObject resultJSONObject, Context context) throws JSONException {
//        Log.i(TAG, "insertOrReplaceFriends");
        if(resultJSONObject.has(AllInfo_getAllInfo.ResponseFields.friends)){
            Motherbase.getInstance(context).getFriendTable().insertOrReplacesFriendsFromMySQL(resultJSONObject.getJSONArray(AllInfo_getAllInfo.ResponseFields.friends));
            SharedPreferencesUtils.getInstance(context).updateUpdatedTS(null, resultJSONObject.getJSONArray(AllInfo_getAllInfo.ResponseFields.friends), SharedPreferencesUtils.PrefKeys.friendUpdatedTS_str);
        }else{
            SharedPreferencesUtils.getInstance(context).updateUpdatedTS(null, null, SharedPreferencesUtils.PrefKeys.friendUpdatedTS_str);
        }
    }

    public static void insertOrReplaceAppUserHobbies(JSONObject resultJSONObject, Context context) throws JSONException {
//        Log.i(TAG, "insertOrReplaceAppUserHobbies");
        if(resultJSONObject.has(AllInfo_getAllInfo.ResponseFields.appUserHobbies)) {
            Motherbase.getInstance(context).getAppUserHobby().insertOrReplaceAppUserHobbiesFromMySQL(resultJSONObject.getJSONArray(AllInfo_getAllInfo.ResponseFields.appUserHobbies));
        }
    }

    public static void insertOrReplaceChats(JSONObject resultJSONObject, Context context) throws JSONException {
//        Log.i(TAG, "insertOrReplaceChats");
        if(resultJSONObject.has(AllInfo_getAllInfo.ResponseFields.chats)){
            Motherbase.getInstance(context).getChatMessageTable().insertOrReplaceChatsFromMySQL(resultJSONObject.getJSONArray(AllInfo_getAllInfo.ResponseFields.chats));
            SharedPreferencesUtils.getInstance(context).updateUpdatedTS(null, resultJSONObject.getJSONArray(AllInfo_getAllInfo.ResponseFields.chats), SharedPreferencesUtils.PrefKeys.latestChatID_str);

        }else{
            SharedPreferencesUtils.getInstance(context).updateUpdatedTS(null, null, SharedPreferencesUtils.PrefKeys.latestChatID_str);
        }
    }

    public static void insertOrReplaceEvents(JSONObject resultJSONObject, Context context) throws JSONException {
//        Log.i(TAG, "insertOrReplaceEvents");
        if(resultJSONObject.has(AllInfo_getAllInfo.ResponseFields.events)){
            Motherbase.getInstance(context).getEventTable().insertOrReplaceEventsFromMySQL(resultJSONObject.getJSONArray(AllInfo_getAllInfo.ResponseFields.events));
            SharedPreferencesUtils.getInstance(context).updateUpdatedTS(null, resultJSONObject.getJSONArray(AllInfo_getAllInfo.ResponseFields.events), SharedPreferencesUtils.PrefKeys.eventUpdatedTS_str);
        }else{
            SharedPreferencesUtils.getInstance(context).updateUpdatedTS(null, null, SharedPreferencesUtils.PrefKeys.eventUpdatedTS_str);
        }
    }

    public static void insertHobbiesAndHobbyClosure(JSONObject resultJSONObject, Context context) throws JSONException {
//        Log.i(TAG, "insertHobbiesAndHobbyClosure");
        if(resultJSONObject.has(AllInfo_getAllInfo.ResponseFields.hobbiesTable)) {
            Motherbase.getInstance(context).getHobbyClosureTable().insertAllHobbiesClosureFromMySQL(resultJSONObject.getJSONArray(AllInfo_getAllInfo.ResponseFields.hobbiesTable));
            Motherbase.getInstance(context).getHobbyTable().insertOrReplaceAllHobbiesFromMySQL(resultJSONObject.getJSONArray(AllInfo_getAllInfo.ResponseFields.hobbiesTable));
        }
    }

    public static void insertOrReplaceInEvent(JSONObject resultJSONObject, Context context) throws JSONException {
//        Log.i(TAG, "insertOrReplaceInEvent");

        if(resultJSONObject.has(AllInfo_getUpdates.ResponseFields.inEvent)) {
            Motherbase.getInstance(context).getEventTable().insertOrReplaceEventFromMySQL(resultJSONObject.getJSONObject(AllInfo_getUpdates.ResponseFields.inEvent));
            SharedPreferencesUtils.getInstance(context).updateUpdatedTS(resultJSONObject.getJSONObject(AllInfo_getUpdates.ResponseFields.inEvent), null, SharedPreferencesUtils.PrefKeys.inEventUpdatedTS_str);
        }else{
            SharedPreferencesUtils.getInstance(context).updateUpdatedTS(null, null, SharedPreferencesUtils.PrefKeys.inEventUpdatedTS_str);
        }
    }

    public static void updateEventChanges(JSONObject resultJSONObject, Context context) throws JSONException {
//        Log.i(TAG, "updateEventChanges");

        if(resultJSONObject.has(AllInfo_getUpdates.ResponseFields.eventsUpdate)) {
            Motherbase.getInstance(context).getEventTable().updateEventsChanges(resultJSONObject.getJSONArray(AllInfo_getUpdates.ResponseFields.eventsUpdate));
        }
    }

}
