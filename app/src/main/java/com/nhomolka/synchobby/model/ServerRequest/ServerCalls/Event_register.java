package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.google.android.gms.location.places.Place;
import com.nhomolka.synchobby.model.Extensions.DateExtension;
import com.nhomolka.synchobby.model.Extensions.ExtensionsHack;
import com.nhomolka.synchobby.model.Constants.APIValues;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;

import java.util.Calendar;

public class Event_register extends APIRequest {


    public class Param {
        public final static String
                Address = MySQLColumns.Address,
                Description = MySQLColumns.Description,
                EndDatetime = MySQLColumns.EndDatetime,
                HobbyID = MySQLColumns.HobbyID,
                Latitude = MySQLColumns.Latitude,
                Longitude = MySQLColumns.Longitude,
                Name = MySQLColumns.Name,
                PlaceName = MySQLColumns.PlaceName,
                Reach = MySQLColumns.Reach,
                StartDatetime = MySQLColumns.StartDatetime;
    }

    public class Response {
        public static final String eventCreated = "eventCreated",
                userAlreadyInEvent = "userAlreadyInEvent";
    }

    public class ResponseFields {
        public static final String event = "event";
    }

    public Event_register(Place address, String description, Calendar endDatetime, int hobbyID, String name, String placeName, int reach, Calendar startDatetime){
        keyValues.put(Param.Address, address.getAddress().toString());
        keyValues.put(Param.Description, description);
        keyValues.put(Param.EndDatetime, DateExtension.CalendarToSQLiteTS(endDatetime));
        keyValues.put(Param.HobbyID, hobbyID);
        keyValues.put(Param.Latitude, address.getLatLng().latitude);
        keyValues.put(Param.Longitude, address.getLatLng().longitude);
        keyValues.put(Param.Name, name);
        keyValues.put(Param.PlaceName, placeName);
        keyValues.put(Param.Reach, reach);
        keyValues.put(Param.StartDatetime,  DateExtension.CalendarToSQLiteTS(startDatetime));


        APIUrl = APIValues.Links.event_register;
    }
}
