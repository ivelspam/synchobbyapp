package com.nhomolka.synchobby.model.SQLite.Tables;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.nhomolka.synchobby.model.Extensions.ExtensionsHack;
import com.nhomolka.synchobby.model.SQLite.Objects.Hobby;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.Constants.SQLiteType;
import com.nhomolka.synchobby.model.Constants.SQLiteColumns;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AppUserHobbyTable {

    private static final String TAG = "##AppUserHobbyTable";
    private static AppUserHobbyTable mUserRegisteredCategory;

    public static final String DATABASE_NAME = "synchobby.db";
    public static final String TABLE_NAME = "appuserhobby";
    public static final String
            ClientUpdatedTS = SQLiteColumns.ClientUpdatedTS,
            HobbyID = MySQLColumns.HobbyID,
            ReceivedTS = SQLiteColumns.ReceivedTS;

    private SQLiteDatabase mDatabase;

    public static final String TABLE_CREATION =
                    "CREATE table " + TABLE_NAME + " (" +
                    HobbyID +" INTEGER PRIMARY KEY, " +
                    ClientUpdatedTS + " " + SQLiteType.TimestampDefault +", " +
                    ReceivedTS + " " + SQLiteType.TimestampDefault +
                    ")";

    public static final String allfields = HobbyID;

    public AppUserHobbyTable(SQLiteDatabase sqLiteDatabase) {
        mDatabase = sqLiteDatabase;
    }

    public static AppUserHobbyTable getInstance(SQLiteDatabase sqLiteDatabase) {
        if (mUserRegisteredCategory == null) {
            mUserRegisteredCategory = new AppUserHobbyTable(sqLiteDatabase);
        }
        return mUserRegisteredCategory;
    }

    public void rebuildTable(){
        mDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        mDatabase.execSQL(TABLE_CREATION);
    }

    //DELETE
    public void removeAHobbies(int hobbyID) throws JSONException {
        Log.i(TAG, "removeAHobbies");
        String query = "DELETE FROM " + TABLE_NAME + " WHERE " + HobbyID + "=" + hobbyID;
        mDatabase.execSQL(query);
    }

    //INSERT
    public void insertAHobby(int hobbyID) throws JSONException {

        Log.i(TAG, "insertAHobby");

        String insertHobby = "INSERT INTO " + TABLE_NAME +
                "(" +
                    HobbyID  +
                ") VALUES (" +
                    hobbyID +
                ")";

        Log.i(TAG, insertHobby);
        mDatabase.execSQL(insertHobby);
    }

    public void insertOrReplaceAppUserHobbiesFromMySQL(JSONArray jsonArray) {
//        Log.i(TAG, "insertOrReplaceAppUserHobbiesFromMySQL");
        mDatabase.beginTransaction();

        try {
            for(int i =0; i < jsonArray.length(); i++){
                insertOrReplaceAppUserHobbyTableFromMySQL(jsonArray.getJSONObject(i));
            }
            mDatabase.setTransactionSuccessful();

        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            mDatabase.endTransaction();
        }
//        Log.i(TAG, toString());
    }

    public void insertOrReplaceAppUserHobbyTableFromMySQL(JSONObject jsonObject) throws JSONException {

//        Log.i(TAG, "insertOrReplaceAppUserHobbyTableFromMySQL");

        StringBuilder sb = new StringBuilder();

        sb.append("REPLACE INTO " + TABLE_NAME+ " (");

        sb.append(MySQLColumns.HobbyID);
        sb.append(") VALUES (");
        try {
            sb.append(Integer.toString(jsonObject.getInt(MySQLColumns.HobbyID)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sb.append(")");
        mDatabase.execSQL(sb.toString());
    }

    //SELECT


    public List<Integer> selectAllAppUserHobbies(){
        List<Integer> ids = new ArrayList<>();
        String query =  "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = mDatabase.rawQuery(query, null);
        while (cursor.moveToNext()) {
            ids.add(cursor.getInt(cursor.getColumnIndex(HobbyID)));
        }
        return ids;
    }

    public List<Hobby> selectChildNodesWithInHobby(int hobbyID) {
//        Log.i(TAG, "selectChildNodesWithInHobby()");

        String query =  "SELECT " + HobbyTable.TABLE_NAME + "." + HobbyID + " as HobbyID, Name, " + HobbyTable.Type +", " + HobbyTable.ParentID +", " + HobbyTable.Version + ", IFNULL(" + AppUserHobbyTable.TABLE_NAME + "." + AppUserHobbyTable.HobbyID + ", -1) inHobby FROM appuserhobby" +
                " LEFT JOIN " + HobbyClosureTable.TABLE_NAME + " ON " + AppUserHobbyTable.TABLE_NAME + "." +AppUserHobbyTable.HobbyID + "=" + HobbyClosureTable.TABLE_NAME + "." + HobbyClosureTable.Descendant +
                " LEFT JOIN "+ HobbyTable.TABLE_NAME +" on " + HobbyTable.TABLE_NAME + "." + HobbyTable.HobbyID + "=" + "hobbyclosure.Ancestor" +
                " WHERE " + HobbyTable.TABLE_NAME + "." +HobbyTable.ParentID + " =" + Integer.toString(hobbyID) +
                " GROUP BY " + HobbyClosureTable.Ancestor;


        Cursor cursor = mDatabase.rawQuery(query, null);

//        Log.i(TAG, ExtensionsHack.CursorToString(cursor));
        List<Hobby> hobbies = new ArrayList<>();

//        Log.i(TAG, Motherbase.printCursor(cursor));
        while(cursor.moveToNext()){
            Hobby hobby = new Hobby(cursor);
            hobbies.add(hobby);
        }

        ExtensionsHack.CursorToString(cursor);
        cursor.close();
        return hobbies;
    }
    public List<Hobby> selectHobbiesWithInfo(){
        String query = "SELECT *, 1 AS inHobby FROM " + TABLE_NAME +" LEFT JOIN " + HobbyTable.TABLE_NAME + " ON " + TABLE_NAME +"." + HobbyID + "=" + HobbyTable.TABLE_NAME + "." + HobbyID;

//        Log.i(TAG, query);
        Cursor cursor = mDatabase.rawQuery(query, null);
        List<Hobby> hobbies = new ArrayList<>();

        while(cursor.moveToNext()){
            hobbies.add(new Hobby(cursor));
        }
        return hobbies;
    }

    public List<Integer> selectHobbiesIDs(){
        String query = "SELECT " + HobbyID + " FROM " + TABLE_NAME;

//        Log.i(TAG, query);
        Cursor cursor = mDatabase.rawQuery(query, null);
        List<Integer> hobbiesIDS = new ArrayList<>();

        while(cursor.moveToNext()){
            hobbiesIDS.add(cursor.getInt(cursor.getColumnIndex(HobbyID)));
        }
        return hobbiesIDS;
    }

    public List<Hobby> selectHobbiesChildWithInfo(){
        String query = "SELECT * FROM " + TABLE_NAME +
                " LEFT JOIN " + HobbyTable.TABLE_NAME + " ON " + TABLE_NAME +"." + HobbyID + "=" + HobbyTable.TABLE_NAME + "." + HobbyID +
                " WHERE " + HobbyTable.TABLE_NAME + "." + HobbyTable.Type + "='child'";

        Cursor cursor = mDatabase.rawQuery(query, null);
        List<Hobby> hobbies = new ArrayList<>();

        while(cursor.moveToNext()){
            hobbies.add(new Hobby(cursor));
        }

//        Log.i(TAG, ExtensionsHack.CursorToString(cursor));
        return hobbies;
    }



    //UPDATE











    public String toString() {
        return DatabaseUtils.dumpCursorToString(mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME, null));
    }

}
