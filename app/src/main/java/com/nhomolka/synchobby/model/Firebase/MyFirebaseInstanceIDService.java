package com.nhomolka.synchobby.model.Firebase;

import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by nhomo on 1/31/2018.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService{

    private static final String REG_TOKEN = "REG_TOKEN";
    private static final String TAG = "##MyFirebaseInstanceIDS";

    @Override
    public void onTokenRefresh() {
        String recent_token = FirebaseInstanceId.getInstance().getToken();
        Log.i(TAG , recent_token);

    }


    static public String getToken(){
        return FirebaseInstanceId.getInstance().getToken();
    }

    static public String getAndroid_id(Context context){
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    static public String getBody(Context context){
        return "&android_id=" + Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID) + "&FCMToken=" + FirebaseInstanceId.getInstance().getToken();
    }

}