package com.nhomolka.synchobby.model.Constants;

public class Timers {

    public static int UPDATE_EVENTS_FRAGMENT = 10 * 1000,
            UPDATE_FRIENDS_FRAGMENT = 10 * 1000,
            EVENT_SPINNER_DIVISIONS = 48,
            SYNC_SERVER = 15;

}
