package com.nhomolka.synchobby.model.Firebase;

import android.content.Context;
import android.util.Log;

import com.nhomolka.synchobby.model.SQLite.Objects.Friend;
import com.nhomolka.synchobby.model.SQLite.Motherbase;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by nhomo on 1/31/2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    static private String TAG = "##MyFirebaseMessagingService";
    Context mContext;

    public MyFirebaseMessagingService() {
        super();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage){
        super.onMessageReceived(remoteMessage);
        mContext = this;
        Map<String, String> data = remoteMessage.getData();
        switch (data.get("type")){
            case "chatMessage" :
                updateReceivedMessage(data);
                break;

            case "newFriendRequest" :
                Log.i(TAG, "newFriendRequest");
                newFriendRequest(data);
                break;

            case "friendAcceptedRequest" :
                Log.i(TAG, "friendAcceptedRequest");
                acceptedFriendRequest(data);
                break;
            case "activityEnded" :
                Log.i(TAG, "activityEnded");
                activityEnded(data);
                break;
        }
    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
    }

    @Override
    public void onMessageSent(String s) {
        super.onMessageSent(s);
    }

    @Override
    public void onSendError(String s, Exception e) {
        super.onSendError(s, e);
    }

    void updateReceivedMessage(Map<String, String> data){
        Log.i(TAG, "updateReceivedMessage");
//        try {
//            JSONObject ChatMessageJSON = new JSONObject(data.get("messageJSON"));
//            Chat chatMessage = new Chat(ChatMessageJSON);
//            chatMessage.setState(2);
//            Motherbase.getInstance(mContext).getChatMessageTable().insertMessage(chatMessage);
//            KeyValues keyValues = new KeyValues("ChatID", chatMessage.getChatID());
//
//            JSONObject resultJSON = new ServerAPI(mContext).serverRequestPOST("api/chat/receivedMessageFromFirebase",  keyValues.toString(), true);
//            String message = resultJSON.getString("message");
//            Log.i(TAG, Motherbase.getInstance(mContext).getChatMessageTable().toString());
//            if(message.compareTo("received") == 0){
//                Motherbase.getInstance(mContext).getChatMessageTable().updateFromStageTwoToThree(chatMessage);
//            }
//            Log.i(TAG, Motherbase.getInstance(mContext).getChatMessageTable().toString());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
    }

    void newFriendRequest(Map<String, String> data){

    }

    void acceptedFriendRequest(Map<String, String> data){
        Log.i(TAG, "friendAcceptedRequest 222222");
        try {
            JSONObject ChatMessageJSON = new JSONObject(data.get("messageJSON"));
            Friend friend = new Friend(ChatMessageJSON);
            Motherbase.getInstance(mContext).getFriendTable().confirmFriend(friend.getAppUserID());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void activityEnded(Map<String, String> data){
        Log.i(TAG, "friendAcceptedRequest 222222");
        try {
            JSONObject ChatMessageJSON = new JSONObject(data.get("messageJSON"));
            Friend friend = new Friend(ChatMessageJSON);
            Motherbase.getInstance(mContext).getFriendTable().confirmFriend(friend.getAppUserID());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
