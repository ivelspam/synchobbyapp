package com.nhomolka.synchobby.model.SQLite.Tables;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.nhomolka.synchobby.model.Extensions.DateExtension;
import com.nhomolka.synchobby.model.Extensions.ExtensionsHack;
import com.nhomolka.synchobby.model.SQLite.Objects.Event;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.Constants.SQLiteType;
import com.nhomolka.synchobby.model.Constants.SQLiteColumns;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EventTable {
    private static final String TAG = "##EventTable";

    private static EventTable mEventTable;

    public static final String
            TABLE_NAME = "event",

            Address = MySQLColumns.Address,
            AppUserID =  MySQLColumns.AppUserID,
            AppUserName =  MySQLColumns.AppUserName,
            CreatedTS =  MySQLColumns.CreatedTS,
            ClientUpdatedTS =  SQLiteColumns.ClientUpdatedTS,
            Description = MySQLColumns.Description,
            EndDatetime = MySQLColumns.EndDatetime,
            EventID = MySQLColumns.EventID,
            Hidden = MySQLColumns.Hidden,
            HobbyID = MySQLColumns.HobbyID,
            HobbyName = MySQLColumns.HobbyName,
            Latitude = MySQLColumns.Latitude,
            Longitude = MySQLColumns.Longitude,
            Name = MySQLColumns.Name,
            PlaceName = MySQLColumns.PlaceName,
            QuantityOfPeople = MySQLColumns.QuantityOfPeople,
            Reach = MySQLColumns.Reach,
            ReceivedTS = SQLiteColumns.ReceivedTS,
            StartDatetime = MySQLColumns.StartDatetime,
            State = MySQLColumns.State,
            UpdatedTS = MySQLColumns.UpdatedTS,

            TABLE_CREATION =
            "CREATE table IF NOT EXISTS " + TABLE_NAME +
                    " (" +
                    Address + " TEXT, " +
                    AppUserID + " TEXT, " +
                    AppUserName + " TEXT, " +
                    ClientUpdatedTS + " " + SQLiteType.TimestampDefault +", " +
                    CreatedTS + " TIMESTAMP, " +
                    Description + " TEXT, " +
                    EndDatetime + " TEXT, " +
                    EventID +" INTEGER PRIMARY KEY, " +
                    Hidden +" INTEGER, " +
                    HobbyID + " INTEGER, " +
                    HobbyName + " TEXT, " +
                    Latitude + " TEXT, " +
                    Longitude + " TEXT, " +
                    Name + " TEXT, " +
                    PlaceName + " TEXT, " +
                    QuantityOfPeople + " INTEGER, " +
                    Reach + " TEXT, " +
                    ReceivedTS + " " + SQLiteType.TimestampDefault +", " +
                    StartDatetime + " TEXT, " +
                    State + " INTEGER, " +
                    UpdatedTS + " TEXT " +
                    ")";

    SQLiteDatabase mDatabase;

    private EventTable(SQLiteDatabase sqLiteDatabase) {
        mDatabase = sqLiteDatabase;
    }

    public static EventTable getInstance(SQLiteDatabase sqLiteDatabase) {
        if (mEventTable == null) {
            mEventTable = new EventTable(sqLiteDatabase);
        }
        return mEventTable;
    }

    public void rebuildTable(){
        mDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        mDatabase.execSQL(TABLE_CREATION);
    }

    //INSERT
    public void insertOrReplaceEventsFromMySQL(JSONArray jsonArrayEvents){
        Log.i(TAG, "insertOrReplaceEventsFromMySQL");

        mDatabase.beginTransaction();
        try {
            for(int i = 0; i< jsonArrayEvents.length(); i++){
                insertOrReplaceEventFromMySQL(jsonArrayEvents.getJSONObject(i));
            }
            mDatabase.setTransactionSuccessful();

        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            mDatabase.endTransaction();
        }
    }

    public void insertOrReplaceEventFromMySQL(JSONObject eventJSONObject){
        Log.i(TAG, "insertOrReplaceEventFromMySQL");

        StringBuilder sb = new StringBuilder();

        sb.append("REPLACE INTO " + TABLE_NAME+ " (");

        sb.append(
                        MySQLColumns.Address + ", " +
                        MySQLColumns.AppUserID + ", " +
                        MySQLColumns.AppUserName + ", " +
                        MySQLColumns.CreatedTS + ", " +
                        MySQLColumns.Description + ", " +
                        MySQLColumns.EndDatetime + ", " +
                        MySQLColumns.EventID + ", " +
                        MySQLColumns.Hidden + ", " +
                        MySQLColumns.HobbyID + ", " +
                        MySQLColumns.HobbyName + ", " +
                        MySQLColumns.Latitude + ", " +
                        MySQLColumns.Longitude + ", " +
                        MySQLColumns.Name + ", " +
                        MySQLColumns.PlaceName + ", " +
                        MySQLColumns.QuantityOfPeople + ", " +
                        MySQLColumns.Reach + ", " +
                        MySQLColumns.StartDatetime + ", " +
                        MySQLColumns.State + ", " +
                        MySQLColumns.UpdatedTS
        );

        sb.append(") VALUES (");

        try {
            sb.append(
                "'" + eventJSONObject.getString(MySQLColumns.Address) + "', " +
                Integer.toString(eventJSONObject.getInt(MySQLColumns.AppUserID)) + ", " +
                "'" + eventJSONObject.getString(MySQLColumns.AppUserName) + "', " +
                "'" + eventJSONObject.getString(MySQLColumns.CreatedTS) + "', " +
                "'" + eventJSONObject.getString(MySQLColumns.Description) + "', " +
                "'" + DateExtension.CalendarMySQLToSQLite(eventJSONObject.getString(MySQLColumns.EndDatetime)) + "', " +
                Integer.toString(eventJSONObject.getInt(MySQLColumns.EventID)) + ", " +
                Integer.toString(eventJSONObject.getInt(MySQLColumns.Hidden)) + ", " +
                Integer.toString(eventJSONObject.getInt(MySQLColumns.HobbyID)) + ", " +
                "'" + eventJSONObject.getString(MySQLColumns.HobbyName) + "', " +
                "'" + eventJSONObject.getString(MySQLColumns.Latitude) + "', " +
                "'" + eventJSONObject.getString(MySQLColumns.Longitude) + "', " +
                "'" + eventJSONObject.getString(MySQLColumns.Name) + "', " +
                "'" + eventJSONObject.getString(MySQLColumns.PlaceName) + "', " +
                Integer.toString(eventJSONObject.getInt(MySQLColumns.QuantityOfPeople)) + ", " +
                Integer.toString(eventJSONObject.getInt(MySQLColumns.Reach)) + ", " +
                "'" + DateExtension.CalendarMySQLToSQLite(eventJSONObject.getString(MySQLColumns.StartDatetime)) + "', " +
                Integer.toString(eventJSONObject.getInt(MySQLColumns.State)) + ", " +
                "'" + DateExtension.CalendarMySQLToSQLite(eventJSONObject.getString(MySQLColumns.UpdatedTS)) + "'"
            );
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sb.append(")");

        mDatabase.execSQL(sb.toString());


    }

    //SELECT
    public Event selectEvent(int eventID){
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + EventID + "=" + eventID, null);

        if(cursor.moveToNext()){
            Event event = new Event(cursor);
            return event;
        }
        cursor.close();
        return null;
    }

    public List<Event> selectAllEventsNoEnded(){
        Log.i(TAG, "selectAllEventsNoEnded");

        Cursor cursor = mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME +" WHERE " + State + "<2", null);
        List<Event> activities = new ArrayList<>();

        while(cursor.moveToNext()){
            activities.add(new Event(cursor));
        }
        cursor.close();
        return activities;
    }

    public List<Event> selectEventGreaterThanClientUpdatedTS(String clientUpdatedTS){
        Log.i(TAG, "selectEventGreaterThanClientUpdatedTS");
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE \"" + MySQLColumns.UpdatedTS + "\">\"" + clientUpdatedTS + "\"";
        Cursor cursor = mDatabase.rawQuery(query, null);
        List<Event> events = new ArrayList<>();
        while(cursor.moveToNext()){
            events.add(new Event(cursor));
        }

        Log.i(TAG, ExtensionsHack.CursorToString(cursor));

        cursor.close();

        return events;
    }

    public List<Integer> getCurrentEventIDs(){
        Cursor cursor = mDatabase.rawQuery("SELECT eventID FROM " + TABLE_NAME, null);

        List<Integer> activitiesIDs = new ArrayList<>();

        while(cursor.moveToNext()){
            activitiesIDs.add(cursor.getInt(cursor.getColumnIndex(EventID)));
        }

        return activitiesIDs;
    }


    //UPDATE

    public void updateEventsChanges(JSONArray jsonArrayEvents){
        Log.i(TAG, "updateEventsChanges");

        mDatabase.beginTransaction();
        try {
            for(int i = 0; i< jsonArrayEvents.length(); i++){

                    JSONObject jsonObject = jsonArrayEvents.getJSONObject(i);

                    String updateQuery = "UPDATE " + TABLE_NAME +
                            " SET " +
                            QuantityOfPeople + "=" + jsonObject.getInt(MySQLColumns.QuantityOfPeople) + ", " +
                            State + "=" + jsonObject.getInt(MySQLColumns.State) + ", " +
                            Hidden + "=" + jsonObject.getInt(MySQLColumns.Hidden) + ", " +
                            UpdatedTS + "='" + DateExtension.CalendarMySQLToSQLite(jsonObject.getString(MySQLColumns.UpdatedTS)) + "',"+
                            ClientUpdatedTS + "='" + DateExtension.CalendarToSQLiteTS(Calendar.getInstance()) + "'" +
                            " WHERE " +
                            EventID + "=" + jsonObject.getInt("EventID");
                    mDatabase.execSQL(updateQuery);

            }
            mDatabase.setTransactionSuccessful();

        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            mDatabase.endTransaction();
        }
    }

    public void joinEvent(int oldEvent, int newEvent){
        if(oldEvent != -1){
            mDatabase.execSQL("UPDATE " + TABLE_NAME + " SET " + QuantityOfPeople + "=" + QuantityOfPeople + "+"+ Integer.toString(-1) + " WHERE " + HobbyID + "=" + oldEvent);
        }
        mDatabase.execSQL("UPDATE " + TABLE_NAME + " SET " + QuantityOfPeople + "=" + QuantityOfPeople + "+"+ Integer.toString(1) + " WHERE " + HobbyID + "=" + newEvent );

    }

    public void leaveEvent(int eventID){
        mDatabase.execSQL("UPDATE " + TABLE_NAME + " SET " + QuantityOfPeople + "=" + QuantityOfPeople + "+" + Integer.toString(-1) + " WHERE " + HobbyID + "=" + eventID  );
    }

    public void setState(int eventID, int state){
        mDatabase.execSQL("UPDATE " + TABLE_NAME + " SET " + State + "=" + state + " WHERE " + eventID + "=" + eventID);
    }

    public String toString() {
        return DatabaseUtils.dumpCursorToString(mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME, null));
    }

    public String toString2(){
        Cursor result = mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        StringBuilder sb = new StringBuilder();

        sb.append("TABLE: " + TABLE_NAME + "\n");
        sb.append("LENGHT: " + result.getCount() + "\n");
        while(result.moveToNext()){
            sb.append(result.getString(0) + " - ");
            sb.append(result.getString(1) + " - ");
            sb.append(result.getString(2) + " \n");
        }
        return sb.toString();
    }

    public void removeOldValues(){
        Cursor result = mDatabase.rawQuery("DELETE FROM " + TABLE_NAME + " WHERE state=2", null);
    }

}