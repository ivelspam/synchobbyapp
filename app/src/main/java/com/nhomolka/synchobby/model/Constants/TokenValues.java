package com.nhomolka.synchobby.model.Constants;

import com.nhomolka.synchobby.model.SQLite.Objects.Event;

import java.util.HashMap;
import java.util.Map;

public class TokenValues {

    static public final String AccountStatus = "AccountStatus",
                                AppUserID = "AppUserID";


    public enum AccountStatusState {
        NotConfirmed(0),
        Confirmed(1),
        Deleted(2);

        private int value;
        private static Map map = new HashMap<>();

        private AccountStatusState(int value) {
            this.value = value;
        }

        static {
            for (AccountStatusState pageType : AccountStatusState.values()) {
                map.put(pageType.value, pageType);
            }
        }

        public static Event.EventState valueOf(int pageType) {
            return (Event.EventState) map.get(pageType);
        }

        public int getValue() {
            return value;
        }

        public String getValueString() {
            return Integer.toString(value);
        }


    }


}
