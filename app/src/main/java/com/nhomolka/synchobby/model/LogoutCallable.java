package com.nhomolka.synchobby.model;

import android.content.Context;

import com.facebook.login.LoginManager;
import com.nhomolka.synchobby.model.SQLite.Motherbase;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.FCMToken_removeTokenFromSystem;
import com.nhomolka.synchobby.model.ServerRequest.ServerAPI;

import java.util.concurrent.Callable;

public class LogoutCallable implements Callable<Boolean> {
    private static final String TAG = "##LogoutCallable";

    Context mContext;

    public LogoutCallable(Context context){
        mContext = context;
    }

    @Override
    public Boolean call() throws Exception {

        new ServerAPI(mContext).serverRequestPOST(new FCMToken_removeTokenFromSystem(mContext));
        Motherbase.getInstance(mContext).restartDatabase();
        LoginManager.getInstance().logOut();

        //go to login screen
        return true;
    }
}
