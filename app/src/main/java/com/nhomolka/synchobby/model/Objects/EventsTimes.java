package com.nhomolka.synchobby.model.Objects;

import android.util.Log;

import com.nhomolka.synchobby.model.Constants.Timers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EventsTimes {


    static private String TAG = "##EventsTimes";

    private List<StartingEventCalendar> startingActivityCalendaries = new ArrayList<>();

    int interval = 5,
        hours = 10,
        divisions = Timers.EVENT_SPINNER_DIVISIONS,
        difference = 3,
        startListPosition = 0,
        endListStart = startListPosition + difference,
        endListEnd = endListStart + divisions,
        endSpinnerPosition = 0,
        endListPosition = difference;

    int change = 0;

    long timeToTrigger;

    public EventsTimes(){
        createList();
    }

    private void createList(){
        Calendar futureCalendar = Calendar.getInstance();
        long howManyMill = futureCalendar.getTimeInMillis();
        futureCalendar.set(Calendar.SECOND, 0);
        futureCalendar.set(Calendar.MILLISECOND, 0);
        int nextBreakInMinutes = interval - (futureCalendar.get(Calendar.MINUTE) % interval);
        int nextThing = nextBreakInMinutes + futureCalendar.get(Calendar.MINUTE);
        futureCalendar.set(Calendar.MINUTE, nextThing);
        timeToTrigger = futureCalendar.getTimeInMillis() - howManyMill;

        //Current Time
        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.set(Calendar.SECOND, 0);
        currentCalendar.set(Calendar.MILLISECOND, 0);

        startingActivityCalendaries.add(new StartingEventCalendar(currentCalendar, 0));

        for(int j = 0 ; j < (hours * (60 / interval)); j++){
            Calendar temp = Calendar.getInstance();
            temp.setTime(futureCalendar.getTime());
            startingActivityCalendaries.add(new StartingEventCalendar(temp, j + 1));
            futureCalendar.add(Calendar.MINUTE, interval);
        }
    }

    public List<StartingEventCalendar> getStartTimeActivities(){
        return startingActivityCalendaries.subList(startListPosition, divisions);

    }

    public List<StartingEventCalendar> getEndTimeActivities(){
        return startingActivityCalendaries.subList(endListStart, endListEnd);
    }

    public StartingEventCalendar getStartByPosition(int position){
        return startingActivityCalendaries.get(position);
    }

    public StartingEventCalendar getEndTime(int position){
        return startingActivityCalendaries.get(endListPosition);
    }

    public void setStartListPosition(int startListPosition){
        printPositions();
        change = startListPosition - this.startListPosition;
        this.startListPosition = startListPosition;
        endListStart = startListPosition + difference;
        endListEnd = endListStart + divisions;
        if(change > 0){
            this.startListPosition = startListPosition;
            if((startListPosition + difference) >= endListEnd){
                endSpinnerPosition = 0;
                endListStart = startListPosition + difference;
                endListPosition = endListStart;
            }
            else{
                endSpinnerPosition = endSpinnerPosition - change;
            }
        }else if(change < 0)
        {
            if((this.startListPosition + divisions + difference) <= endListPosition){
                endListPosition = this.startListPosition + divisions;
                endSpinnerPosition = divisions - 1;
            }else{
                endSpinnerPosition = endSpinnerPosition - change;
            }
        }
        printPositions();

    }

    public void printPositions(){
        Log.i(TAG, "setStartListPosition: \n" +
                "startListPosition: " + this.startListPosition +"\n" +
                "new startListPosition: " + startListPosition +"\n" +
                "endListStart: " + endListStart  +"\n" +
                "endListEnd: " + endListEnd+ "\n" +
                "endSpinnerPosition: " + endSpinnerPosition+ "\n" +
                "endListPosition: " + endListPosition+ "\n" +
                "change: " + change + "\n"
        );
    }
    public void setEndListPosition(int endSpinnerPosition){
        this.endSpinnerPosition = endSpinnerPosition;
        endListPosition = endListStart + endSpinnerPosition;
        printPositions();
    }

    public int getCurrentStartActivityTime(){
        return startListPosition;
    }

    public int getCurrentEndStartingTimeSpinner(){
        return endSpinnerPosition;
    }

    public long getTimeToTrigger() {
        return timeToTrigger;
    }

    public int getInterval() {
        return interval;
    }
}
