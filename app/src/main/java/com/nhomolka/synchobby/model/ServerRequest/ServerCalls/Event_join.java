package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;

public class Event_join extends APIRequest{

    public class Param {
        public static final String EventID = MySQLColumns.EventID;
    }

    public class Response {
        public static final String  joinedEvent = "joinedEvent",
                                    alreadyInEvent = "alreadyInEvent";

    }

    public class ResponseFields {
        public static final String event = "event";

    }


    public Event_join(int eventID){
        keyValues.put(Param.EventID, eventID);
        APIUrl = APIValues.Links.event_join;

    }


}
