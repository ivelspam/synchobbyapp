package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;

public class Email_addUser extends APIRequest {

    public class ResponseFields {
        public final static String
                appuseremailuser = "alreadyExists",
                token = "token";
    }

    public class Param {
        public final static String
                Email = MySQLColumns.Email,
                Name = MySQLColumns.Name,
                Password = MySQLColumns.Password;
    }

    public class Response {
        public final static String
                alreadyExists = "alreadyExists",
                registered = "registered";
    }

    public Email_addUser(String name, String email, String password) {
        token = false;
        keyValues.put(Param.Email, email);
        keyValues.put(Param.Name, name);
        keyValues.put(Param.Password, password);
        APIUrl = APIValues.Links.email_addUser;
    }
}
