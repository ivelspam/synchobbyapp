package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;

public class Login_email extends APIRequest{

    public class ResponseFields {

    }

    public class Param {
        public final static String
                Email = "Email",
                Password = "Password";
    }

    public class Response {
        public final static String
                noMatch = "noMatch",
                match = "match";
    }

    public Login_email(String email, String password){
        keyValues.put(Param.Email, email);
        keyValues.put(Param.Password, password);
        APIUrl = APIValues.Links.login_email;
    }

}
