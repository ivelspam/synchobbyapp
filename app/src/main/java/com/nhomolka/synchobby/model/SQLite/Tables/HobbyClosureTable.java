package com.nhomolka.synchobby.model.SQLite.Tables;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.SQLite.Objects.Hobby;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HobbyClosureTable {

    private static final String TAG = "##HobbyCloure";

    private static HobbyClosureTable sHobbyClosureTable;

    public static final String TABLE_NAME = "hobbyclosure";
    public static final String Ancestor = "Ancestor";
    public static final String Descendant = "Descendant";
    public static final String Depth = "Depth";

    public static final String TABLE_CREATION =
            "CREATE table " + TABLE_NAME + " (" +
                    Ancestor +" INTEGER," +
                    Descendant + " INTEGER, " +
                    Depth + " INTEGER, " +
                    "UNIQUE(" + Ancestor + ", " + Descendant + ") ON CONFLICT IGNORE" +
                    ")";

    private SQLiteDatabase mDatabase;

    String[] mColumns = new String[]{Ancestor, Descendant, Depth};

    public HobbyClosureTable(SQLiteDatabase sqLiteDatabase) {
        mDatabase = sqLiteDatabase;
    }

    public static HobbyClosureTable getInstance(SQLiteDatabase sqLiteDatabase) {
        if (sHobbyClosureTable == null) {
            sHobbyClosureTable = new HobbyClosureTable(sqLiteDatabase);
        }
        return sHobbyClosureTable;
    }

    public void rebuildTable(){
        mDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        mDatabase.execSQL(TABLE_CREATION);
    }

    //DELETE
    //INSERT
    public void insertAllHobbiesClosureFromMySQL(JSONArray hobbies) throws JSONException {
        mDatabase.beginTransaction();

        try {
            for(int i = 0; i<hobbies.length(); i++){
                insertAllHobbyClosureFromMySQL(hobbies.getJSONObject(i));
            }
            mDatabase.setTransactionSuccessful();

        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            mDatabase.endTransaction();
        }


    }


    public void insertAllHobbyClosureFromMySQL(JSONObject jsonObject) throws JSONException {
            Integer ancestor = jsonObject.getInt(MySQLColumns.HobbyID);
            Integer newDescendant = jsonObject.isNull(MySQLColumns.ParentID) ? -1 : jsonObject.getInt(MySQLColumns.ParentID);
            Integer hobbyID =  jsonObject.getInt(MySQLColumns.HobbyID);


            if (newDescendant.intValue() == -1) {
                newDescendant = ancestor;
            }

            String query = "INSERT INTO " + TABLE_NAME + " (Ancestor, Descendant, Depth) " +
                    "SELECT Ancestor, " + ancestor.toString() + ", (Depth + 1) " +
                    "FROM hobbyclosure " +
                    "WHERE descendant =" + newDescendant.toString() +
                    " UNION ALL SELECT " + hobbyID.toString() + ", " + hobbyID.toString() + ", 0";
            mDatabase.execSQL(query);
    }

    //SELECT

    //UPDATE

    public String toString() {
        return DatabaseUtils.dumpCursorToString(mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME, null));
    }


    public String toString2(){
        Cursor result = mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        StringBuilder sb = new StringBuilder();
        sb.append("TABLE: " + TABLE_NAME + "\n");
        sb.append("LENGHT: " + result.getCount() + "\n");
        while(result.moveToNext()){
            sb.append(result.getInt(0) + " - ");
            sb.append(result.getInt(1) + " - ");
            sb.append(result.getInt(2) + " \n");
        }
        return sb.toString();
    }
}
