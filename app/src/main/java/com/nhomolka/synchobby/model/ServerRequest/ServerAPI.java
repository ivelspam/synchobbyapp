package com.nhomolka.synchobby.model.ServerRequest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.Upload_profilePicture;
import com.nhomolka.synchobby.model.TokenUtilities;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;

import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.nhomolka.synchobby.controller.Activities.MainTabbedActivity.MainTabbedActivity.getMimeType;


public class ServerAPI {
    private static final String TAG = "##ServerAPI";
    private static final String ADDRESS = "https://synchobby.com/api/";
//    private static final String ADDRESS = "http://192.168.29.248:8080/";

    int timeout = 1500;

    Context mContext;
    String token;
    public ServerAPI(Context context){
        mContext = context;
        token = TokenUtilities.getToken(context);
    }


    public JSONObject serverRequestGET(APIRequest apiRequest){
//        Log.i(TAG, "##methodJSONObjectGET");

        HttpURLConnection httpURLConnection = null;
        String exceptionMessage;

        try {
            String fullAddress = ADDRESS + apiRequest.getAPIUrl() + apiRequest.getQuery();

            Log.i(TAG, fullAddress);
            URL url = new URL( fullAddress);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setConnectTimeout(timeout);
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            if(apiRequest.token){
                httpURLConnection.setRequestProperty("authorization", "bearer " + token);
            }

            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                return new JSONObject(response.toString());
            } else {
                Log.i(TAG, Integer.toString(httpURLConnection.getResponseCode()));
                return new JSONObject("{message : " + Integer.toString(httpURLConnection.getResponseCode()) + "}");
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            exceptionMessage = "'MalformedURLException'";
        } catch (ConnectTimeoutException e){
            exceptionMessage = "'ConnectTimeoutException'";
            e.printStackTrace();
        }catch (SocketTimeoutException e){
            exceptionMessage = "'SocketTimeoutException'";
            e.printStackTrace();
        }catch (IOException e) {
            exceptionMessage = "'IOException'";
            e.printStackTrace();
        } catch (JSONException e) {
            exceptionMessage = "'JSONException'";
            e.printStackTrace();
        }
        try {
            switch(exceptionMessage){
                case "JSONException":
                    return new JSONObject("{message : 'JSONException', error : "+exceptionMessage+"}");
                default:
                    return new JSONObject("{message : 'problem connecting', error : "+exceptionMessage+"}");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONObject serverRequestPOST(APIRequest apiRequest){
//        Log.i(TAG, "serverRequestPOST");
        String exceptionMessage;

        try {
            String fullAddress = ADDRESS + apiRequest.getAPIUrl();

            Log.i(TAG, fullAddress);
            URL url = new URL(fullAddress);
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
            httpsURLConnection.setConnectTimeout(timeout);
            httpsURLConnection.setDoOutput(true);
            httpsURLConnection.setDoInput(true);
            httpsURLConnection.setRequestMethod("POST");
            httpsURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            if(apiRequest.token){
                httpsURLConnection.setRequestProperty("authorization", "bearer " + token);
            }

            byte[] outputBytes = apiRequest.getBody().getBytes("UTF-8");

            OutputStream os = httpsURLConnection.getOutputStream();
            os.write(outputBytes);
            os.close();

            if (httpsURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(httpsURLConnection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                return new JSONObject(response.toString());
            } else {

                Log.i(TAG, "{message : " + Integer.toString(httpsURLConnection.getResponseCode()) + "}");

                return new JSONObject("{message : " + Integer.toString(httpsURLConnection.getResponseCode()) + "}");
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            exceptionMessage = "'MalformedURLException'";
        } catch (ConnectTimeoutException e){
            exceptionMessage = "'ConnectTimeoutException'";
            e.printStackTrace();
        }catch (SocketTimeoutException e){
            exceptionMessage = "'SocketTimeoutException'";
            e.printStackTrace();
        }catch (IOException e) {
            exceptionMessage = "'IOException'";
            e.printStackTrace();
        } catch (JSONException e) {
            exceptionMessage = "JSONException";
            e.printStackTrace();
        }
        try {
            switch(exceptionMessage){
                case "JSONException":
                    return new JSONObject("{message : 'JSONException', error : "+exceptionMessage+"}");
                default:
                    return new JSONObject("{message : 'problem connecting', error : "+exceptionMessage+"}");


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Bitmap downloadBitmap(APIRequest apiRequest) {
        HttpURLConnection httpURLConnection = null;

        Log.i(TAG, "downloadBitmap");
        String exceptionMessage;

        try {

            Log.i(TAG, "downloadBitmap");
            String finalURL = ADDRESS + apiRequest.getAPIUrl()+ apiRequest.getQuery();

            Log.i(TAG, finalURL);
            URL uri = new URL(finalURL);
            httpURLConnection = (HttpURLConnection) uri.openConnection();
            httpURLConnection.setConnectTimeout(2000);

            int statusCode = httpURLConnection.getResponseCode();
            if (statusCode != HttpURLConnection.HTTP_OK) {
                return null;
            }

            InputStream inputStream = httpURLConnection.getInputStream();
            if (inputStream != null) {
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                return bitmap;
            }
        }  catch (MalformedURLException e) {
            e.printStackTrace();
            exceptionMessage = "'MalformedURLException'";
        } catch (ConnectTimeoutException e){
            exceptionMessage = "'ConnectTimeoutException'";
            e.printStackTrace();
        }catch (SocketTimeoutException e){
            exceptionMessage = "'SocketTimeoutException'";
            e.printStackTrace();
        }catch (IOException e) {
            exceptionMessage = "'IOException'";
            e.printStackTrace();
        }finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }
        return null;
    }

    public boolean uploadPicturesBitmap(final Upload_profilePicture upload_profilePicture) {

        String content_type = getMimeType(upload_profilePicture.mFile.getPath());

        String file_path = upload_profilePicture.mFile.getAbsolutePath();
        OkHttpClient client = new OkHttpClient();
        RequestBody file_body = RequestBody.create(MediaType.parse(content_type), upload_profilePicture.mFile);
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("type", content_type)
                .addFormDataPart(upload_profilePicture.mUploadType.getValue(), file_path.substring(file_path.lastIndexOf("/") + 1), file_body)
                .build();

        Request request = new Request.Builder()
                .header("authorization", "bearer " + token)
                .url(ADDRESS + upload_profilePicture.APIUrl)
                .post(requestBody)
                .build();
        try {
            Response response = client.newCall(request).execute();
            Log.i(TAG, response.body().toString());

            if (!response.isSuccessful()) {
                throw new IOException("Error : " + response);
            } else {
                return true;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

}


//    public Bitmap getImage(int appUserID, int imageVersion, Download_image.Type type) {
//        if(imageVersion <= 0){ return null; }
//        HttpURLConnection httpURLConnection = null;
//        ContextWrapper contextWrapper = new ContextWrapper(mContext);
//        File directory = contextWrapper.getDir("bitmaps", Context.MODE_PRIVATE);
//        if(!directory.exists()){
//            directory.mkdir();
//        }
//
//        File imagePath = new File(directory, Integer.toString(appUserID) + "-" + Integer.toString(imageVersion));
//
//
//        if(!imagePath.exists()){
//              Bitmap bitmap = downloadBitmap(new Download_image(type, appUserID, imageVersion));
//            if (bitmap != null) {
//                FileOutputStream fileOutputStream = null;
//
//                try {
//                    fileOutputStream = new FileOutputStream(imagePath);
//                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
//                    fileOutputStream.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//                return BitmapFactory.decodeFile(imagePath.getAbsolutePath());
//            } else {
//                return null;
//            }
//        }
//        return BitmapFactory.decodeFile(imagePath.getAbsolutePath());
//    }