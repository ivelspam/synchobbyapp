package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;

public class Appuser_getImageVersion extends APIRequest {


    public class ResponseFields {
        public final static String
                ImageVersion = MySQLColumns.ImageVersion;

    }

    public class Param {
        public final static String
                AppUserID = "AppUserID";
    }

    public class Response {
        public final static String
                foundImage = "foundImage",
                notFoundImage = "notFoundImage";
    }

    public Appuser_getImageVersion(int appUserID){
        keyValues.put(Param.AppUserID, appUserID);
        APIUrl = APIValues.Links.appuser_getImageVersion;
        token = false;
    }
}
