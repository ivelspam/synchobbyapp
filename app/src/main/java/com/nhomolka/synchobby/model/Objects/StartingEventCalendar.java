package com.nhomolka.synchobby.model.Objects;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class StartingEventCalendar {
    static private String TAG = "##StartingActivityTime";

    Calendar calendar;
    int arrayPosition;

    public StartingEventCalendar(Calendar calendar, int arrayPosition) {
        this.calendar = calendar;
        this.arrayPosition = arrayPosition;
    }

    public Calendar getCalendar() {
        return calendar;
    }

    @Override
    public String toString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E H:mma");
//        if(calendar.getTimeInMillis() < Calendar.getInstance().getTimeInMillis()){
//            return "Right Now " + simpleDateFormat.format(calendar.getTime()) + " " + arrayPosition;
//        }else{
            return simpleDateFormat.format(calendar.getTime());
//        }
    }
}
