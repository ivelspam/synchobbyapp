package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;

public class Appuser_updateUsername extends APIRequest{
    public class ResponseFields {

    }

    public class Param {
        public static final String Username = MySQLColumns.Username;
    }

    public class Response {
        public static final String updated = "updated",
                notUnique = "notUnique";
    }

    public Appuser_updateUsername(String username){
        keyValues.put(Param.Username, username);
        APIUrl = APIValues.Links.appuser_updateUsername;
    }
}
