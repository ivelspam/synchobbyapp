package com.nhomolka.synchobby.model.Extensions;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.nhomolka.synchobby.model.Constants.CalendarFormats;
import com.nhomolka.synchobby.model.SQLite.Objects.Event;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class ExtensionsHack {
    private static final String TAG = "##ExtensionsHack";

    public static String StringToTitleCase(String str) {

        if (str == null) {
            return null;
        }

        boolean space = true;
        StringBuilder builder = new StringBuilder(str);
        final int len = builder.length();

        for (int i = 0; i < len; ++i) {
            char c = builder.charAt(i);
            if (space) {
                if (!Character.isWhitespace(c)) {
                    builder.setCharAt(i, Character.toTitleCase(c));
                    space = false;
                }
            } else if (Character.isWhitespace(c)) {
                space = true;
            } else {
                builder.setCharAt(i, Character.toLowerCase(c));
            }
        }
        return builder.toString();
    }

    static public String GsonPrettify(JSONObject jsonObject){
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonParser jp = new JsonParser ();
        JsonElement je = jp.parse(jsonObject.toString());
        return  gson.toJson(je);
    }

    static public String CursorToString(Cursor cursor){
        return DatabaseUtils.dumpCursorToString(cursor);
    }

    static public int IntegerToString(Context context, int pixels){
        final float scale = context.getResources().getDisplayMetrics().density;
        return  (int) (pixels * scale + 0.5f);

    }




    static public boolean BooleanIntToPrint(int tinyint){
        return tinyint == 0 ? false : true;
    }
}
