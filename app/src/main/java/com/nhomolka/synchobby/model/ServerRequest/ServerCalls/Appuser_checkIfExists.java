package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;

public class Appuser_checkIfExists extends APIRequest {

    public class Param {}

    public class Response {
        public static final String
                userFound = "userFound",
                userNotFound = "userNotFound";
    }

    public class ResponseFields {}

    public Appuser_checkIfExists(){
        APIUrl = APIValues.Links.appuser_checkIfExists;
    }
}
