package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;

public class Friend_sendRequest extends  APIRequest {

    public class Param {
        public final static String
                AppUserID = "AppUserID";
    }

    public class Response {
        public final static String
                userAlreadyExists = "userAlreadyExists",
                userAdded = "userAdded";
    }

    public class ResponseFields {
        public final static String
                ImageVersion = MySQLColumns.ImageVersion;
    }

    public Friend_sendRequest(int appUserID){
        keyValues.put(Param.AppUserID, appUserID);
        APIUrl = APIValues.Links.friend_sendRequest;
    }

}
