package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;


public class Appuser_updateName extends APIRequest {

    public class ResponseFields {

    }

    public class Param {
        public final static String
                Name  = MySQLColumns.Name;
    }

    public class Response {
        public final static String
                updated = "updated",
                notUpdated = "notUpdated";
    }

    public Appuser_updateName(String name){
        keyValues.put(Param.Name, name);
        APIUrl = APIValues.Links.appuser_updateName;
    }

}
