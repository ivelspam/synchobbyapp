package com.nhomolka.synchobby.model.Extensions;

import android.util.Log;

import com.nhomolka.synchobby.model.Constants.CalendarFormats;
import com.nhomolka.synchobby.model.SQLite.Objects.Event;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class DateExtension {

    private static final String TAG = "##DateExtension";

    public static String CalendarMySQLToSQLite(String dateString){
        String replacedString = dateString.replace("T", " ").replace("Z", "");
        return replacedString;
    }

    public static Calendar CalendarSQLiteDatetimeToCalendar(String dateString){
        Log.i(TAG, "CalendarMySQLDatetimeTZToCalendar");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(CalendarFormats.SQLiteTS);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        Date date = null;
        try {
            date = simpleDateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    public static Calendar CalendarMySQLDatetimeTZToCalendar(String dateString){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(CalendarFormats.MySQLTZ);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        Date date = null;
        try {
            date = simpleDateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        return cal;
    }

    static public String CalendarToSQLiteTS(Calendar calendar){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(CalendarFormats.SQLiteTS);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return simpleDateFormat.format(calendar.getTime());
    }

    public static Calendar CalendarDateToCalendar(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }


    static public String CalendarGetUpdatedTS(List<Event> events){
        List<String> updatesTSs = new ArrayList<>();

        if(events.size() == 0){
            return "1970-01-01 00:00:00.000";
        }

        for(int i = 0; i < events.size(); i++){
            updatesTSs.add(events.get(i).getUpdatedTS());
        }

        Calendar calendar = null;
        for(int i = 0; i < updatesTSs.size(); i++){

            Calendar calendar1 = CalendarSQLiteDatetimeToCalendar(updatesTSs.get(i));
            if(calendar == null){
                calendar = calendar1;
            }else if(calendar.getTimeInMillis() < calendar1.getTimeInMillis()){
                calendar = calendar1;
            }
        }
        return CalendarToSQLiteTS(calendar);
    }

    static public int CalendarGetDifference(Calendar firstCalendar, Calendar secondCalendar, Integer calendarType){
        long timeInMillis = firstCalendar.getTimeInMillis() - secondCalendar.getTimeInMillis();
        int divisions = 1;
        if(calendarType == Calendar.HOUR){
            divisions = 1000 * 60 * 60;
        }else if(calendarType == Calendar.MINUTE){
            divisions = 1000 * 60;
        }else if(calendarType == Calendar.SECOND){
            divisions = 1000;
        }

        return (int)timeInMillis/divisions;
    }

    public static String MySQLToPattern(String dateString, String pattern){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(CalendarFormats.MySQLTZ);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        Calendar calendar = Calendar.getInstance();
        simpleDateFormat.setCalendar(calendar);
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(pattern);
        return simpleDateFormat2.format(calendar.getTime());
    }

    public static String UTCToLocal(String datetime) {
        DateFormat utcFormat = new SimpleDateFormat(CalendarFormats.SQLiteTS);
        utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = utcFormat.parse(datetime);
            DateFormat localFormat = new SimpleDateFormat(CalendarFormats.shortFormat);

            localFormat.setTimeZone(TimeZone.getDefault());
            return localFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getCalendarInSQLiteTS(){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(CalendarFormats.SQLiteTS);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return simpleDateFormat.format(calendar.getTime());
    }

    public static String MySQLToDefaultPattern(String dateString){
        String replacedString = dateString.replace("T", " ").replace("Z", "");
        return replacedString;
    }

    static public Boolean CalendarCompareTwoSQLiteTSFirstBiggerThanSecond(String first, String second){
        SimpleDateFormat format = new SimpleDateFormat(CalendarFormats.SQLiteTS);
        Date date1 = null;
        Date date2 = null;

        try {
            date1 = format.parse(first);
            date2 = format.parse(second);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date1.compareTo(date2) > 0;
    }

    static public Calendar CalendarToNearestRoundUp(int nextMinute){
        Calendar calendar = Calendar.getInstance();
        int unroundedMinutes = calendar.get(Calendar.MINUTE);
        int mod = unroundedMinutes % nextMinute;
        calendar.add(Calendar.MINUTE, mod == 0 ? nextMinute : nextMinute - mod);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar;
    }

    static public String CalendarGetFormated(Calendar calendar, String format){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(calendar.getTime());
    }

    static public Boolean CalendarCompareTwoMySQLTSFirstBiggerThanSecond(String first, String second){

        SimpleDateFormat format = new SimpleDateFormat( CalendarFormats.MySQLTZ);

        Date date1 = null;
        Date date2 = null;
        try {
            date1 = format.parse(first);
            date2 = format.parse(second);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date1.compareTo(date2) > 0;
    }
}
