package com.nhomolka.synchobby.model.SQLite.Objects;

import android.content.ContentValues;
import android.database.Cursor;

import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.Constants.SQLiteColumns;
import com.nhomolka.synchobby.model.Extensions.ExtensionsHack;

import org.json.JSONException;
import org.json.JSONObject;

public class Hobby {

    public static final String child = "child";
    public static final String parent = "parent";

    int HobbyID, Version, ParentID;
    String Name, Type;
    boolean inHobby;

    public Hobby(int hobbyID, String name, String type, int version, int parentID) {
        HobbyID = hobbyID;
        Name = name;
        Type = type;
        Version =  version;
        ParentID = parentID;
    }

    public Hobby(Cursor cursor) {
        HobbyID = cursor.getInt(cursor.getColumnIndex(MySQLColumns.HobbyID));
        Name = cursor.getString(cursor.getColumnIndex(MySQLColumns.Name));
        Type = cursor.getString(cursor.getColumnIndex(MySQLColumns.Type));
        Version = cursor.getInt(cursor.getColumnIndex(MySQLColumns.Version));
        ParentID = cursor.getInt(cursor.getColumnIndex(MySQLColumns.ParentID));
        if(cursor.getColumnIndex(SQLiteColumns.inHobby) != -1){
            if(cursor.getInt(cursor.getColumnIndex(SQLiteColumns.inHobby)) != -1){
                inHobby = true;
            }
        }
    }

    public Hobby(int hobbyID, String name, String type, boolean inHobby) {
        HobbyID = hobbyID;
        Name = name;
        Type = type;
        this.inHobby = true;
    }

    public Hobby(int hobbyID, String name, String type) {
        HobbyID = hobbyID;
        Name = name;
        Type = type;
        this.inHobby = true;
    }

    public Hobby(int hobbyID, String name, String type, int parentID) {
        HobbyID = hobbyID;
        Name = name;
        Type = type;
        ParentID = parentID;
    }

    public Hobby(JSONObject hobbyJSON) {

        try {
            HobbyID = hobbyJSON.getInt(MySQLColumns.HobbyID);
            Name = hobbyJSON.getString(MySQLColumns.Name);
            Type = hobbyJSON.getString(MySQLColumns.Type);
            if(hobbyJSON.has(MySQLColumns.ParentID) && !hobbyJSON.isNull(MySQLColumns.ParentID) ){
                ParentID = hobbyJSON.getInt(MySQLColumns.ParentID);
            }else{
                ParentID = -1;
            }

            Version = hobbyJSON.getInt(MySQLColumns.Version);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getHobbyID() {
        return HobbyID;
    }

    public void setHobbyID(int hobbyID) {
        this.HobbyID = hobbyID;
    }

    public String getName() {
        return Name;
    }
    public String getNameTitleCase() {
        return ExtensionsHack.StringToTitleCase(Name);

    }

    public void setName(String name) {
        Name = name;
    }


    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public int getParentID() {
        return ParentID;
    }

    public boolean isInHobby() {
        return inHobby;
    }

    public void setInHobby(boolean inHobby) {
        this.inHobby = inHobby;
    }

    @Override
    public String toString() {
        return "Hobby{" +
                "HobbyID=" + HobbyID +
                ", Version=" + Version +
                ", ParentID=" + ParentID +
                ", Name='" + Name + '\'' +
                ", Type='" + Type + '\'' +
                ", inHobby=" + inHobby +
                '}';
    }

    public ContentValues getContentValues(){
        ContentValues contentValues = new ContentValues();
        contentValues.put("HobbyID", HobbyID);
        contentValues.put("Name", Name);
        contentValues.put("Type", Type);
        contentValues.put("Version", Version);
        return contentValues;
    }

    public String getMySQLValues(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(Integer.toString(HobbyID) + ", ");
        stringBuilder.append("'" + Name + "', ");
        stringBuilder.append("'" + Type + "', ");
        stringBuilder.append(Integer.toString(Version) + ", ");
        stringBuilder.append(Integer.toString(ParentID));

        return stringBuilder.toString();
    }


}
