package com.nhomolka.synchobby.model.SQLite.Tables;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.nhomolka.synchobby.model.SQLite.Objects.Chat;
import com.nhomolka.synchobby.model.SQLite.Objects.Friend;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.Constants.SQLiteType;
import com.nhomolka.synchobby.model.Constants.SQLiteColumns;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class ChatTable {
    static private String TAG = "##ChatTable";

    private static ChatTable sChatTable;

    public static final String TABLE_NAME = "chat",

                    ChatID = MySQLColumns.ChatID,
                    ClientUpdatedTS = SQLiteColumns.ClientUpdatedTS,
                    CreatedTS = MySQLColumns.CreatedTS,
                    Owner = MySQLColumns.Owner,
                    Message = MySQLColumns.Message,
                    Receiver = MySQLColumns.Receiver,
                    ReceivedTS = SQLiteColumns.ReceivedTS,
                    SentDatetime = MySQLColumns.SentDatetime,
                    State = MySQLColumns.State,
                    UpdatedTS = MySQLColumns.UpdatedTS,
                    UUID = MySQLColumns.UUID;


    public static final String TABLE_CREATION =
            "CREATE table IF NOT EXISTS " + TABLE_NAME +
                    " (" +
                        ChatID + " INTEGER DEFAULT -1, " +
                        ClientUpdatedTS + " " + SQLiteType.TimestampDefault +", " +
                        CreatedTS + " TIMESTAMP, " +
                        Message + " TEXT, " +
                        Owner + " INTEGER, " +
                        Receiver + " INTEGER, " +
                        ReceivedTS+ " " + SQLiteType.TimestampDefault +", " +
                        State + " INTEGER DEFAULT 0, " +
                        SentDatetime + " TIMESTAMP, " +
                        UpdatedTS + " TIMESTAMP, " +
                        UUID + " TEXT PRIMARY KEY " +
                    ")";

    SQLiteDatabase mDatabase;

    private ChatTable(SQLiteDatabase sqLiteDatabase) {
        mDatabase = sqLiteDatabase;
    }

    public static ChatTable getInstance(SQLiteDatabase sqLiteDatabase) {
        if (sChatTable == null) {
            sChatTable = new ChatTable(sqLiteDatabase);
        }
        return sChatTable;
    }

    public void rebuildTable() {
//        Log.i(TAG, "rebuildTable");
        mDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        mDatabase.execSQL(TABLE_CREATION);
    }



    //INSERT
    public void insertOrReplaceChatsFromMySQL(JSONArray jsonArray){

        mDatabase.beginTransaction();

        try {
            for(int i = 0; i< jsonArray.length(); i++){
                insertOrReplaceChatFromMySQL(jsonArray.getJSONObject(i));
            }
            mDatabase.setTransactionSuccessful();

        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            mDatabase.endTransaction();
        }
    }

    public void insertOrReplaceChatFromMySQL(JSONObject chatJSONObject){
//        Log.i(TAG, "insertOrReplaceChatFromMySQL");

        StringBuilder sb = new StringBuilder();

        sb.append("INSERT OR REPLACE INTO " + TABLE_NAME+ " (");

        sb.append(
                MySQLColumns.ChatID + ", " +
                MySQLColumns.CreatedTS + ", " +
                MySQLColumns.Owner + ", " +
                MySQLColumns.Message + ", " +
                MySQLColumns.Receiver + ", " +
                MySQLColumns.SentDatetime + ", " +
                MySQLColumns.State + ", " +
                MySQLColumns.UpdatedTS + ", " +
                MySQLColumns.UUID
        );

        sb.append(") VALUES (");

        try {
            sb.append(
                    Integer.toString(chatJSONObject.getInt(MySQLColumns.ChatID)) + ", " +
                            "'" + chatJSONObject.getString(MySQLColumns.CreatedTS) + "', " +
                            Integer.toString(chatJSONObject.getInt(MySQLColumns.Owner)) + ", " +
                            "'" + chatJSONObject.getString(MySQLColumns.Message) + "', " +
                            Integer.toString(chatJSONObject.getInt(MySQLColumns.Receiver)) + ", " +
                            "'" + chatJSONObject.getString(MySQLColumns.SentDatetime) + "', " +
                            Integer.toString(chatJSONObject.getInt(MySQLColumns.State)) + ", " +
                            "'" + chatJSONObject.getString(MySQLColumns.UpdatedTS) + "', " +
                            "'" + chatJSONObject.getString(MySQLColumns.UUID) + "'"
            );
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sb.append(")");

        mDatabase.execSQL(sb.toString());


    }

    //SELECT
    public List<Chat> selectChatsFromAppUserID(int owner){

        Cursor cursor = mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + Owner + "=" + owner + " OR " + Receiver + "=" + owner + " ORDER BY julianday(`" + CreatedTS + "`) ASC", null);
        List<Chat> chats = new ArrayList<>();
        while (cursor.moveToNext()) {
            chats.add(new Chat(cursor));
        }
        cursor.close();
        return chats;
    }

    public List<Chat> selectUnreadMessages(int friendID) {
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE (" + Owner + "=" + friendID + " OR " + Receiver + "=" + friendID + ") AND " + State + "=3 ORDER BY " + CreatedTS + " ASC", null);
        List<Chat> chats = new ArrayList<>();
        while (cursor.moveToNext()) {
            chats.add(new Chat(cursor));
        }
        return chats;
    }

    public List<Chat> selectFriendChatHigherThanChatID(int friendID, int latestID){
        Log.i(TAG, "selectFriendChatHigherThanChatID");

        List<Chat> chats = new ArrayList<>();

        final String query =    "SELECT * FROM " + TABLE_NAME +
                                " WHERE (" + Owner + "=" + friendID + " OR "+ Receiver+ "=" + friendID + ") AND " + ChatID + ">" + latestID +
                                " ORDER BY " + ChatID + " ASC" ;

        Cursor cursor = mDatabase.rawQuery(query, null);
        while(cursor.moveToNext()){
            chats.add(new Chat(cursor));
        }

        cursor.close();

        return chats;
    }

    public int selectLatestChatID(int friendID){
//        Log.i(TAG, "selectLatestChatID");

        ArrayList<Friend> friends = new ArrayList<>();

        final String query = "SELECT MAX("+ ChatID  +") as "+ ChatID+ " FROM " + TABLE_NAME + " WHERE " + Owner + "=" + friendID + " OR " + Receiver + "=" + friendID;
        Cursor cursor = mDatabase.rawQuery(query, null);
        while(cursor.moveToNext()){
            return cursor.getInt(cursor.getColumnIndex(ChatID));
        }

        cursor.close();

        return -1;
    }

    //UPDATE

    public void updateAllReadMessaged() {
        List<Friend> friends = FriendTable.getInstance(mDatabase).selectAllFriends();
        for (int i = 0; i < friends.size(); i++) {
            Friend friend = friends.get(i);
            mDatabase.execSQL("UPDATE " + TABLE_NAME + " SET State=4 WHERE julianday("+ MySQLColumns.CreatedTS +") AND State=3 AND (Owner=" + friend.getAppUserID() + " OR Receiver=" + friend.getAppUserID() + ")");
        }
    }

    public String toString() {
        return DatabaseUtils.dumpCursorToString(mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME + " ORDER BY ChatID ASC", null));
    }



    class GetCountAsync_table extends AsyncTask<String, String, Integer> {
        private final WeakReference<TextView> aTvunreadmessages;
        int aFriendID = 0;

        public GetCountAsync_table(int friendID, TextView tvunreadmessages) {
            aTvunreadmessages = new WeakReference<>(tvunreadmessages);
            aFriendID = friendID;
        }

        @Override
        protected Integer doInBackground(String... params) {
            Cursor cursor = mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE (" + Owner + "=" + aFriendID + " OR " + Receiver + "=" + aFriendID + ") AND " + State + "=3 ORDER BY " + CreatedTS + " ASC", null);
            return cursor.getCount();
        }

        @Override
        protected void onPostExecute(Integer count) {
            if(count == 0){
                aTvunreadmessages.get().setVisibility(View.GONE);
            }else{
                aTvunreadmessages.get().setText(Integer.toString(count));
            }
        }
    }
}
