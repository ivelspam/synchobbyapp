package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import android.content.Context;
import android.location.Location;

import com.nhomolka.synchobby.model.SQLite.Objects.Event;
import com.nhomolka.synchobby.model.SQLite.Motherbase;
import com.nhomolka.synchobby.model.Constants.APIValues;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.Location.LocationUtils;
import com.nhomolka.synchobby.model.SQLite.Objects.Hobby;
import com.nhomolka.synchobby.model.SharedPreferencesUtils;

import java.util.List;

public class AllInfo_getUpdates extends APIRequest{
    static private String TAG = "##AllInfo_getAllInfo";
    public Boolean token = true;

    public class Response {
        public final static String  foundUpdates = "foundUpdates",
                                    noUpdates = "noUpdates";
    }

    public class ResponseFields {
        public final static String
                            AppUserChange = "AppUserChange",
                            chats = "chats",
                            inEvent = "inEvent",
                            eventsUpdate = "eventsUpdate",
                            events = "events",
                            friends = "friends";
    }

    public class Param {
        public final static String
                    eventsIDs = "eventsIDs",
                    eventUpdatedTS = "eventUpdatedTS",
                    friendUpdatedTS = "friendUpdatedTS",
                    hobbiesIDs = "hobbyIDs",
                    inEventsUpdatedTS = "inEventsUpdatedTS",
                    latestChatID = "latestChatID",
                    Latitude = MySQLColumns.Latitude,
                    Longitude = MySQLColumns.Longitude,
                    UpdatedTS = MySQLColumns.UpdatedTS;
    }

    public AllInfo_getUpdates(Context context){
        Location location = LocationUtils.getLastKnownLocation(context);

        List<Event> events = Motherbase.getInstance(context).getEventTable().selectAllEventsNoEnded();
        List<Integer> hobbiesIDs = Motherbase.getInstance(context).getAppUserHobby().selectAllAppUserHobbies();

        SharedPreferencesUtils sharedPreferencesUtils = SharedPreferencesUtils.getInstance(context);
        keyValues.put(Param.eventsIDs, getEventIDList(events));
        keyValues.put(Param.eventUpdatedTS, sharedPreferencesUtils.getString(SharedPreferencesUtils.PrefKeys.eventUpdatedTS_str));
        keyValues.put(Param.hobbiesIDs, getHobbyIds(hobbiesIDs));
        keyValues.put(Param.friendUpdatedTS, sharedPreferencesUtils.getString(SharedPreferencesUtils.PrefKeys.friendUpdatedTS_str));
        keyValues.put(Param.inEventsUpdatedTS, sharedPreferencesUtils.getString(SharedPreferencesUtils.PrefKeys.inEventUpdatedTS_str));
        keyValues.put(Param.latestChatID, sharedPreferencesUtils.getInt(SharedPreferencesUtils.PrefKeys.latestChatID_str));
        keyValues.put(Param.Latitude,  Double.toString(location.getLatitude()));
        keyValues.put(Param.Longitude,  Double.toString(location.getLongitude()));
        keyValues.put(Param.UpdatedTS, sharedPreferencesUtils.getString(SharedPreferencesUtils.PrefKeys.UpdatedTS_str));


        APIUrl = APIValues.Links.allInfo_getUpdates;
    }

    String getEventIDList(List<Event> events){
        if(events.size() == 0) return "";

        StringBuilder stringBuilder = new StringBuilder("(-1,");

        for(Event event : events){
            stringBuilder.append(Integer.toString(event.getEventID()) + ",");
        }

        stringBuilder.setLength(stringBuilder.length() - 1);
        stringBuilder.append(")");

        return stringBuilder.toString();
    }

    String getHobbyIds(List<Integer> hobbies){
        if(hobbies.size() == 0) return "";

        StringBuilder stringBuilder = new StringBuilder("(-1,");

        for(Integer integer : hobbies){
            stringBuilder.append(Integer.toString(integer) + ",");
        }
        stringBuilder.setLength(stringBuilder.length() - 1);
        stringBuilder.append(")");

        return stringBuilder.toString();
    }
}
