package com.nhomolka.synchobby.model.SQLite.Objects;


import android.database.Cursor;

import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.Extensions.DateExtension;

import org.json.JSONException;
import org.json.JSONObject;

public class Chat {
    static private String TAG = "##Chat";

    int ChatID = -1, Owner = -1, Receiver, State = 0;
    String Message, CreatedTS, SentDatetime, UUID, UpdatedTS;

    public Chat(Cursor cursor) {
        ChatID = cursor.getInt(cursor.getColumnIndex(MySQLColumns.ChatID));
        UUID =    cursor.getString(cursor.getColumnIndex(MySQLColumns.UUID));
        Owner =        cursor.getInt(cursor.getColumnIndex(MySQLColumns.Owner));
        Receiver =        cursor.getInt(cursor.getColumnIndex(MySQLColumns.Receiver));
        State =        cursor.getInt(cursor.getColumnIndex(MySQLColumns.State));
        Message =        cursor.getString(cursor.getColumnIndex(MySQLColumns.Message));
        SentDatetime =        cursor.getString(cursor.getColumnIndex(MySQLColumns.SentDatetime));
        CreatedTS =        cursor.getString(cursor.getColumnIndex(MySQLColumns.CreatedTS));
        UpdatedTS =        cursor.getString(cursor.getColumnIndex(MySQLColumns.UpdatedTS));
    }

    public Chat(int owner, int receiver, String message) {

        this.Owner = owner;
        this.Receiver = receiver;
        this.Message = message;
        this.SentDatetime = DateExtension.getCalendarInSQLiteTS();
        this.UUID = java.util.UUID.randomUUID().toString();
    }

    public Chat(JSONObject chatMessageJSONObject) throws JSONException {
        ChatID = chatMessageJSONObject.getInt(MySQLColumns.ChatID);
        CreatedTS = DateExtension.MySQLToDefaultPattern(chatMessageJSONObject.getString(MySQLColumns.UpdatedTS));
        Owner = chatMessageJSONObject.getInt(MySQLColumns.Owner);
        Message = chatMessageJSONObject.getString(MySQLColumns.Message);
        Receiver = chatMessageJSONObject.getInt(MySQLColumns.Receiver);
        SentDatetime = DateExtension.MySQLToDefaultPattern(chatMessageJSONObject.getString(MySQLColumns.SentDatetime));
        State = chatMessageJSONObject.getInt(MySQLColumns.State);
        UpdatedTS = DateExtension.MySQLToDefaultPattern(chatMessageJSONObject.getString(MySQLColumns.UpdatedTS));
        UUID = chatMessageJSONObject.getString(MySQLColumns.UUID);
    }

    public int getChatID() {
        return ChatID;
    }

    public int getOwner() {
        return Owner;
    }

    public String getMessage() {
        return Message;
    }

    public int getState() {
        return State;
    }

    public void setState(int state) { this.State = state; }

    public String getCreatedTS() {
        return CreatedTS;
    }

    public String getUUID() {
        return UUID;
    }

    public String getJSONString(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(MySQLColumns.UUID, UUID);
            jsonObject.put(MySQLColumns.Owner, Owner);
            jsonObject.put(MySQLColumns.Receiver, Receiver);
            jsonObject.put(MySQLColumns.Message, Message);
            jsonObject.put(MySQLColumns.SentDatetime, SentDatetime);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    public String getSetMysqlValues(){
        return  "ChatID='" + ChatID + "', " +
                "UUID='" + UUID + "', " +
                "Owner='" + Owner + "', " +
                "Receiver='" + Receiver + "', " +
                "State='" + State + "', " +
                "message='" + Message + "', " +
                "SentDatetime='" + SentDatetime + "', " +
                "CreatedTS'" + CreatedTS + "', " +
                "UpdatedTS'" + UpdatedTS + "'";
    }

    public String getMysqlValuesSentValues(){
        return  "'" + UUID + "', " +
                "'" + Owner + "', " +
                "'" + Receiver + "', " +
                "'" + Message + "', " +
                "'" + SentDatetime + "' ";
    }

    public static String getMySQLColumns(){

        return  "`" + MySQLColumns.ChatID + "`, " +
                "`" + MySQLColumns.UUID + "`, " +
                "`" + MySQLColumns.Owner + "`, " +
                "`" + MySQLColumns.Receiver + "`, " +
                "`" + MySQLColumns.State + "`, " +
                "`" + MySQLColumns.Message + "`, " +
                "`" + MySQLColumns.SentDatetime + "`, " +
                "`" + MySQLColumns.CreatedTS + "`, " +
                "`" + MySQLColumns.UpdatedTS + "` ";
    }

    public String getMySQLValues(){

        return        Integer.toString(ChatID)      + ", " +
                "'" + UUID                          + "', " +
                      Integer.toString(Owner)       + ", " +
                      Integer.toString(Receiver)    + ", " +
                      Integer.toString(State)       + ", " +
                "'" + Message                       + "', " +
                "'" + SentDatetime                  + "', " +
                "'" + CreatedTS                     + "', " +
                "'" + UpdatedTS                     + "' ";
    }

    @Override
    public String toString() {
        return "Chat{" +
                "chatID=" + ChatID +
                ", owner=" + Owner +
                ", receiver=" + Receiver +
                ", state=" + State +
                ", message='" + Message + '\'' +
                ", UpdatedTS='" + CreatedTS + '\'' +
                ", SentDatetime='" + SentDatetime + '\'' +
                ", UUID='" + UUID + '\'' +
                '}';
    }

    public String getRequest(){

        return "&Receiver=" + Receiver +
                "&Message=" + Message +
                "&UUID=" + UUID +
                "&SentDatetime=" + SentDatetime;
    }


}
