package com.nhomolka.synchobby.model.SQLite.Tables;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.nhomolka.synchobby.model.SQLite.Objects.Hobby;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;

import java.util.ArrayList;
import java.util.List;

public class HobbyTable {

    private static final String TAG = "##HobbyTable";
    private static HobbyTable mHobbyTable;

    public static final String DATABASE_NAME = "synchobby.db",
                                TABLE_NAME = "hobby",
                                HobbyID = "HobbyID",
                                ParentID = "ParentID",
                                Name = "Name",
                                Type = "Type",
                                Version = "Version";


    private SQLiteDatabase mDatabase;

    public static final String TABLE_CREATION =
            "CREATE table " + TABLE_NAME + " (" +
                HobbyID +" INTEGER PRIMARY KEY, " +
                    ParentID + " INTEGER, " +
                    Name + " TEXT, " +
                    Type + " TEXT, " +
                    Version + " INTEGER " +
            ")";

    public HobbyTable(SQLiteDatabase sqLiteDatabase) {
        mDatabase = sqLiteDatabase;
    }

    public static HobbyTable getInstance(SQLiteDatabase sqLiteDatabase) {
        if (mHobbyTable == null) {
            mHobbyTable = new HobbyTable(sqLiteDatabase);
        }
        return mHobbyTable;
    }

    public void rebuildTable(){
        mDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        mDatabase.execSQL(TABLE_CREATION);
    }

    //INSERT
    public void insertOrReplaceAllHobbiesFromMySQL(JSONArray jsonArray) {
//        Log.i(TAG, "insertOrReplaceAllHobbiesFromMySQL");

        mDatabase.beginTransaction();
        try {

            for(int i = 0; i < jsonArray.length(); i++){
                insertOrReplaceHobbyFromMySQL(jsonArray.getJSONObject(i));
            }
            mDatabase.setTransactionSuccessful();

        }catch (JSONException e){
            e.printStackTrace();
        }finally {
            mDatabase.endTransaction();
        }
    }


    public void insertOrReplaceHobbyFromMySQL(JSONObject jsonObject) {
        StringBuilder sb = new StringBuilder();

        sb.append("REPLACE INTO " + TABLE_NAME+ " (");

        sb.append(
                MySQLColumns.HobbyID + ", " +
                        MySQLColumns.ParentID + ", " +
                        MySQLColumns.Name + ", " +
                        MySQLColumns.Type + ", " +
                        MySQLColumns.Version
        );

        sb.append(") VALUES (");

        try {
            sb.append(
                    Integer.toString(jsonObject.getInt(MySQLColumns.HobbyID)) + ", " +
                            (jsonObject.isNull(MySQLColumns.ParentID) ? "-1" : Integer.toString(jsonObject.getInt(MySQLColumns.ParentID))) + ", " +
                            "'" +   jsonObject.getString(MySQLColumns.Name) + "', " +
                            "'" +   jsonObject.getString(MySQLColumns.Type) + "', " +
                            Integer.toString(jsonObject.getInt(MySQLColumns.Version))
            );
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sb.append(")");

        mDatabase.execSQL(sb.toString());
    }

    //SELECT
    public List<Hobby> selectHobbyByToken(String token){

        String query =  "SELECT " + TABLE_NAME + ".*, IFNULL(" + AppUserHobbyTable.TABLE_NAME + "." + AppUserHobbyTable.HobbyID + ", -1) AS inHobby FROM " + TABLE_NAME +
                        " LEFT JOIN " + AppUserHobbyTable.TABLE_NAME + " ON " + AppUserHobbyTable.TABLE_NAME + "." + AppUserHobbyTable.HobbyID + "=" + TABLE_NAME + "." + HobbyID +
                        " WHERE " + TABLE_NAME + "." + Name + " LIKE '%" + token + "%' AND " + TABLE_NAME + "." + Type + "='child'";


        Cursor cursor = mDatabase.rawQuery(query, null);
        List<Hobby> hobbies = new ArrayList<>();


        while (cursor.moveToNext()) {
            hobbies.add(new Hobby(cursor));
        }

        cursor.close();
        return hobbies;
    }

    public Hobby selectHobbyByID(int hobbyID){

        Log.i(TAG, "selectHobbyByID" + Integer.toString(hobbyID));

        String query =  "SELECT * FROM " + TABLE_NAME +
                        " WHERE " + HobbyID + "=" + Integer.toString(hobbyID);
        Cursor cursor = mDatabase.rawQuery(query, null);
        while (cursor.moveToNext()) {
            return new Hobby(cursor);
        }
        return null;
    }

    public List<Hobby> selectAllHobbies(){


        List<Hobby> hobbies = new ArrayList<>();
        String query =  "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = mDatabase.rawQuery(query, null);
        while (cursor.moveToNext()) {
            hobbies.add(new Hobby(cursor));
        }
        return hobbies;
    }

    public List<Hobby> selectChildrenOFHobbyJoined(int hobbyID){

        String query =  "SELECT " + TABLE_NAME + ".*, IFNULL(" + AppUserHobbyTable.TABLE_NAME + "." + AppUserHobbyTable.HobbyID + ", -1) AS inHobby FROM " + TABLE_NAME +
                        " LEFT JOIN " + AppUserHobbyTable.TABLE_NAME + " ON " + TABLE_NAME + "." + HobbyID + "=" + AppUserHobbyTable.TABLE_NAME + "." + HobbyID +
                        " WHERE " + ParentID + "=" + Integer.toString(hobbyID);

        Cursor cursor = mDatabase.rawQuery(query, null);
        List<Hobby> hobbies = new ArrayList<>();


        while (cursor.moveToNext()) {
            hobbies.add(new Hobby(cursor));
        }

        cursor.close();
        return hobbies;
    }

    //UPDATE

    public String toString() {
        return DatabaseUtils.dumpCursorToString(mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME, null));
    }
}
