package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;

public class Email_sendAnotherToken extends APIRequest{

    public class ResponseFields {

    }

    public class Param {

    }

    public class Response {
        public final static String
                sent = "sent",
                registeredAlready = "registeredAlready";
    }

    public Email_sendAnotherToken(){
        APIUrl = APIValues.Links.email_sendAnotherToken;
    }


}
