package com.nhomolka.synchobby.model.Objects;

import java.util.HashMap;
import java.util.Map;

public class KeyValues extends HashMap<String, String> {

    public KeyValues() {}

    public String put(String key, String value){
        super.put(key, value);
        return value;
    }

    public String put(String key, int value){
        super.put(key, Integer.toString(value));

        return Integer.toString(value);
    }

    public String put(String key, double value){
        super.put(key, Double.toString(value));
        return Double.toString(value);
    }

    public String getBody(){
        StringBuilder sb = new StringBuilder();

        if(this.size() == 0 ){
            return "";
        }

        for(Map.Entry<String, String> entry: this.entrySet()){

            sb.append("&" + entry.getKey() +"=" +  entry.getValue());
        }
        return sb.toString();
    }

    public String getQuery() {
        if (this.size() == 0) {
            return "";
        }

        StringBuilder sb = new StringBuilder();

        for (Map.Entry<String, String> entry : this.entrySet()) {
            sb.append("&" + entry.getKey() + "=" + entry.getValue());
        }

        sb.deleteCharAt(0);
        sb.insert(0, "?");
        return sb.toString();
    }
}
