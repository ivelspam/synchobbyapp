package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;

public class Event_close extends APIRequest{

    public class ResponseFields {
        public final static String
                event = "event";
    }

    public class Param {
        public final static String
                EventID  = MySQLColumns.EventID;
    }

    public class Response {
        public static final String closedEvent = "closedEvent";
    }

    public Event_close(int eventID){
        keyValues.put(Param.EventID, eventID);
        APIUrl = APIValues.Links.event_close;
    }
}
