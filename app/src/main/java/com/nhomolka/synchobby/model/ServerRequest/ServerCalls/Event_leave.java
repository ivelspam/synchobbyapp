package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;

public class Event_leave extends APIRequest{

    public class Param {
        public static final String EventID = MySQLColumns.EventID;
    }

    public class Response {
        public static final String leftEvent = "leftEvent";
    }

    public class ResponseFields {
        public static final String event = "event";
    }

    public Event_leave(int eventID){
        keyValues.put(Param.EventID, eventID);
        APIUrl = APIValues.Links.event_leave;
    }

}
