package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;

public class Friend_Delete extends APIRequest {

    public class Param {
        public final static String
                AppUserID = "AppUserID";
    }

    public class Response {
        public final static String
                deleted = "deleted";
    }

    public class ResponseFields {
        public final static String
                ImageVersion = MySQLColumns.ImageVersion;
    }

    public Friend_Delete(int appUserID){
        keyValues.put(Friend_sendRequest.Param.AppUserID, appUserID);
        APIUrl = APIValues.Links.friend_delete;
    }


}
