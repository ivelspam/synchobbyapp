package com.nhomolka.synchobby.model.Constants;

public class SocketIOConnections {

    public static String clientReceiveEventMessage(int eventID){
        return "clientReceiveEventMessage" + Integer.toString(eventID);
    }

    public static String friendChat(int owner, int receiver){
        StringBuilder sb = new StringBuilder("friendChat");
        if(owner < receiver){
            sb.append(Integer.toString(owner) + "-" + Integer.toString(receiver));
        }else{
            sb.append(Integer.toString(receiver) + "-" + Integer.toString(owner));
        }

        return sb.toString();
    }

    static public String sendChatMessage = "sendChatMessage";
    static public String serverReceiveEventComment = "serverReceiveEventComment";

}