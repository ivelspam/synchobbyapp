package com.nhomolka.synchobby.model.SQLite.Objects;

import android.database.Cursor;
import android.util.Log;

import com.nhomolka.synchobby.model.Constants.MagicNumbers;
import com.nhomolka.synchobby.model.Extensions.DateExtension;
import com.nhomolka.synchobby.model.Extensions.ExtensionsHack;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.Constants.SQLiteColumns;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Friend {

    private static final String TAG = "##Friend";

    private int AppUserID;
    private String ClientUpdatedTS;
    private int EventID;
    private int ImageVersion;
    private String Name;
    private String ReceivedTS;
    private FriendState State;
    private String UpdatedTS;

    public enum FriendState {
            NoState(-1),
            SentRequest(0),
            ReceivedRequest(1),
            Confirmed(2),
            Deleted(3),
            Banned(4);

        private int value;
        private static Map map = new HashMap<>();

        private FriendState(int value) {
            this.value = value;
        }

        static {
            for (FriendState pageType : FriendState.values()) {
                map.put(pageType.value, pageType);
            }
        }

        public static FriendState valueOf(int pageType) {
            return (FriendState) map.get(pageType);
        }

        public int getValue() {
            return value;
        }

    }

//    public Friend(int appUserID, String name, int EventID, int state, int imageVersion, String updatedTS) {
//        AppUserID = appUserID;
//        Name = name;
//        EventID = EventID;
//        State = state;
//        ImageVersion = imageVersion;
//        UpdatedTS = updatedTS;
//    }

    public Friend(JSONObject friendJSONObject){
        try {
            AppUserID = friendJSONObject.getInt(MySQLColumns.AppUserID);
            if(!friendJSONObject.isNull(MySQLColumns.EventID)){ EventID = friendJSONObject.getInt(MySQLColumns.EventID); }
            ImageVersion =  friendJSONObject.getInt(MySQLColumns.ImageVersion);
            Name = friendJSONObject.getString(MySQLColumns.Name);
            if(friendJSONObject.isNull(MySQLColumns.State)){  State =  FriendState.NoState; }else{ State = FriendState.valueOf(friendJSONObject.getInt(MySQLColumns.State)); }
            UpdatedTS = friendJSONObject.has(MySQLColumns.UpdatedTS) ?  DateExtension.CalendarMySQLToSQLite(friendJSONObject.getString(MySQLColumns.UpdatedTS)) : MagicNumbers.LOWEST_TS;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Friend(Cursor cursor){
        AppUserID = cursor.getInt(cursor.getColumnIndex(MySQLColumns.AppUserID));
        ClientUpdatedTS = cursor.getString(cursor.getColumnIndex(SQLiteColumns.ClientUpdatedTS));
        EventID = cursor.getInt(cursor.getColumnIndex(MySQLColumns.EventID));
        ImageVersion = cursor.getInt(cursor.getColumnIndex(MySQLColumns.ImageVersion));
        Name = cursor.getString(cursor.getColumnIndex(MySQLColumns.Name));
        ReceivedTS = cursor.getString(cursor.getColumnIndex(SQLiteColumns.ReceivedTS));
        State = FriendState.valueOf(cursor.getInt(cursor.getColumnIndex(MySQLColumns.State)));
        UpdatedTS = cursor.getString(cursor.getColumnIndex(MySQLColumns.UpdatedTS));
    }

    public int getAppUserID() {
        return AppUserID;
    }

    public void setAppUserID(int appUserID) {
        AppUserID = appUserID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getEventID() {return EventID;}

    public FriendState getState() {
        return State;
    }

    public int getImageVersion() {
        return ImageVersion;
    }
    public String getClientUpdatedTS() {
        return ClientUpdatedTS;
    }


    public String getUpdatedTS(){ return UpdatedTS; }

    @Override
    public String toString() {
        return "Friend{" +
                    "AppUserID=" + AppUserID +
                    ", EventID=" + EventID +
                    ", State=" + State +
                    ", Name='" + Name + '\'' +
                    ", ImageVersion='" + ImageVersion + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        Log.i(TAG, Integer.toString(AppUserID) + " " + Integer.toString( ((Friend)obj).getAppUserID()));

        return AppUserID == ((Friend)obj).getAppUserID();
    }
}
