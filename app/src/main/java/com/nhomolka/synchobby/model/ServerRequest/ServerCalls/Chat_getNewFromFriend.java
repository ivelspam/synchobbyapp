package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import android.content.Context;

import com.nhomolka.synchobby.model.Constants.APIValues;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.SQLite.Motherbase;

public class Chat_getNewFromFriend extends APIRequest {

    public int latestChatID;

    public class ResponseFields {
        public final static String
                newMessagesFound = "newMessagesFound";
    }

    public class Param {
        public final static String
                AppUserID  = MySQLColumns.AppUserID,
                lastestChatID = "lastestChatID";
    }

    public class Response {
        public static final String foundNewMessages = "foundNewMessages",
                notFoundNewMessages = "notFoundNewMessages";
    }

    public Chat_getNewFromFriend(int appUserID, Context context){
        latestChatID = Motherbase.getInstance(context).getChatMessageTable().selectLatestChatID(appUserID);
        keyValues.put(Param.AppUserID, appUserID);
        keyValues.put(Param.lastestChatID, latestChatID);
        APIUrl = APIValues.Links.chat_getNewFromFriend;
    }

}
