package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import android.content.Context;
import android.location.Location;

import com.nhomolka.synchobby.model.Constants.APIValues;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.Firebase.MyFirebaseInstanceIDService;
import com.nhomolka.synchobby.model.Location.LocationUtils;

public class AllInfo_getAllInfo extends APIRequest{
    static private String TAG = "##AllInfo_getAllInfo";

    public class Param {
        static final String
                            DeviceID = MySQLColumns.DeviceID,
                            DeviceType = MySQLColumns.DeviceType,
                            FCMToken = MySQLColumns.FCMToken,
                            Latitude = MySQLColumns.Latitude,
                            Longitude = MySQLColumns.Longitude;
    }

    public class Response {
        public static final String userFound = "userFound",
                            userNotFound = "userNotFound";


    }

    public class ResponseFields {
        public final static String
                appUserHobbies = "appUserHobbies",
                appUserInfo = "appUserInfo",
                chats = "chats",
                events = "events",
                friends = "friends",
                hobbiesTable = "hobbiesTable",
                inEvent = "inEvent";
    }

    public AllInfo_getAllInfo(Context context){
        Location location = LocationUtils.getLastKnownLocation(context);

        keyValues.put(Param.DeviceID, MyFirebaseInstanceIDService.getAndroid_id(context));
        keyValues.put(Param.DeviceType, "android");
        keyValues.put(Param.FCMToken, MyFirebaseInstanceIDService.getToken());
        keyValues.put(Param.Latitude, location.getLatitude());
        keyValues.put(Param.Longitude, location.getLongitude());

        APIUrl = APIValues.Links.allInfo_getAllInfo;
    }

}
