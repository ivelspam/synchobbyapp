package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;

public class Hobbies_addHobby extends APIRequest {

    public class Param {
        public final static String
                HobbyID = "HobbyID";
    }

    public class Response {
        public final static String
                alreadyExists = "alreadyExists",
                hobbyDoNotExists = "hobbyDoNotExists",
                hobbyAdded = "hobbyAdded";
    }

    public class ResponseFields {

    }

    public Hobbies_addHobby(int hobbyID){
        keyValues.put(Param.HobbyID, hobbyID);
        APIUrl = APIValues.Links.hobbies_addHobby;
    }
}
