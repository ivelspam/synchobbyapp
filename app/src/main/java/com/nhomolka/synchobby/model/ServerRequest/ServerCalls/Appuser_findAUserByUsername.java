package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;

public class Appuser_findAUserByUsername extends APIRequest {

    public class ResponseFields {
        public final static String
                user = "user";
    }

    public class Param {
        public final static String
                Username = "Username";
    }

    public class Response {
        public final static String
                userFound = "userFound",
                userNotFound = "userNotFound";
    }

    public Appuser_findAUserByUsername(String username){
        keyValues.put(Param.Username, username);
        APIUrl = APIValues.Links.appuser_findAUserByUsername;
        token = false;

    }

}
