package com.nhomolka.synchobby.model.LogOut;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.nhomolka.synchobby.model.LogoutCallable;
import com.nhomolka.synchobby.controller.Activities.LoginActivity;
import com.nhomolka.synchobby.model.ServerRequest.QueryForUpdatedSingleton;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Logout {

    static public void logout(Context context) {

        Activity activity = (Activity)context;
        ExecutorService executorService = Executors.newCachedThreadPool();

        Future<Boolean> future = executorService.submit(new LogoutCallable(context));
        executorService.shutdown();
        try {
            future.get();

            QueryForUpdatedSingleton.getInstance().endTask();
            Intent intent = new Intent(context, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            activity.startActivity(intent);
            activity.finish();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
