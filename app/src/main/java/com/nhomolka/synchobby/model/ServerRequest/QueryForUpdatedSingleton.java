package com.nhomolka.synchobby.model.ServerRequest;

import android.content.Context;

import com.nhomolka.synchobby.model.SQLite.ServerRequestUpdateSQLiteMethods;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.AllInfo_getUpdates;
import com.nhomolka.synchobby.model.SharedPreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.nhomolka.synchobby.model.Constants.Timers.SYNC_SERVER;


public class QueryForUpdatedSingleton {
    static private String TAG = "##QueryForUpdatedSingleton";

    private static final QueryForUpdatedSingleton lock = new QueryForUpdatedSingleton();
    private static volatile QueryForUpdatedSingleton instance;

    ScheduledExecutorService scheduledExecutorService;
    GetNewUpdated myTask;
    Context mContext;
    boolean taskRunning = false;

    public static QueryForUpdatedSingleton getInstance() {

        QueryForUpdatedSingleton r = instance;

        if(r == null) {
            synchronized (lock) {
                r = instance;
                if(r == null){
                    r = new QueryForUpdatedSingleton();
                    instance = r;
                }
            }
        }
        return r;
    }

    private QueryForUpdatedSingleton(){}

    public void startTask(Context context){
//        Log.i(TAG, "startTask");
        this.mContext = context;
        GetNewUpdated myTask = new GetNewUpdated();
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleWithFixedDelay(myTask,  SYNC_SERVER, SYNC_SERVER, TimeUnit.SECONDS);
    }

    public void endTask(){
//        Log.i(TAG, "startTask");

        if(scheduledExecutorService != null){
            scheduledExecutorService.shutdown();
        }

    }

    static public JSONObject getUpdates(Context context){

        JSONObject jsonObject = new ServerAPI(context).serverRequestPOST(new AllInfo_getUpdates(context));

        try {
            if(jsonObject.getString(APIRequest.message).compareTo(AllInfo_getUpdates.Response.foundUpdates) == 0){
                //AppUserChange
                SharedPreferencesUtils.getInstance(context).updateUser(jsonObject);
                //chats
                ServerRequestUpdateSQLiteMethods.insertOrReplaceChats(jsonObject, context);
                //events
                ServerRequestUpdateSQLiteMethods.insertOrReplaceEvents(jsonObject, context);
                //eventsUpdate
                ServerRequestUpdateSQLiteMethods.updateEventChanges(jsonObject, context);
                //friends
                ServerRequestUpdateSQLiteMethods.insertOrReplaceFriends(jsonObject, context);
                //inEvent
                ServerRequestUpdateSQLiteMethods.insertOrReplaceInEvent(jsonObject, context);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;

    }

    public class GetNewUpdated implements Runnable {
        @Override
        public void run() {
            getUpdates(mContext);
        }
    }
}
