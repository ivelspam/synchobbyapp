package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import android.content.Context;

import com.nhomolka.synchobby.model.Constants.APIValues;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.Firebase.MyFirebaseInstanceIDService;

public class FCMToken_removeTokenFromSystem extends APIRequest{

    public class Param {
        public final static String
                FCMToken  = MySQLColumns.FCMToken;
    }

    public class Response {
        public final static String
                deleted  = "deleted";
    }

    public class ResponseFields {

    }

    public FCMToken_removeTokenFromSystem(Context context){
        keyValues.put(Param.FCMToken, MyFirebaseInstanceIDService.getToken());
        APIUrl = APIValues.Links.fcmToken_removeTokenFromSystem;

    }
}



