package com.nhomolka.synchobby.model.Constants;

public class APIValues {

    public class Links {
        public static final String

            allInfo_getAllInfo = "allInfo_getAllInfo",
            allInfo_getUpdates = "allInfo_getUpdates",
            allInfo_updates = "allInfo_getUpdates",

            appuser_checkIfExists = "appuser_checkIfExists",
            appuser_deleteAccount = "appuser_deleteAccount",
            appuser_findByAppUserID = "appuser_findByAppUserID",
            appuser_findAUserByUsername = "appuser_findAUserByUsername",
            appuser_getImageVersion = "appuser_getImageVersion",
            appuser_UpdateDistanceAndMeasurementSystem = "appuser_UpdateDistanceAndMeasurementSystem",
            appuser_updateName = "appuser_updateName",
            appuser_updateUsername = "appuser_updateUsername",

            chat_getNewFromFriend = "chat_getNewFromFriend",

            download_image = "download_image",

            event_close = "event_close",
            event_getEventByDistance = "event_getEventByDistance",
            event_getEventUpdate = "event_getEventUpdate",

            event_delete = "event_delete",
            event_hide = "event_hide",
            event_leave = "event_leave",
            event_join = "event_join",
            event_register = "event_register",
            event_show = "event_show",

            eventcomment_getComments = "eventcomment_getComments",
            eventcomment_send = "eventcomment_send",


            email_addUser = "email_addUser",
            email_getNewToken = "Token_getNewToken",
            email_isConfirmed = "email_isConfirmed",
            email_sendAnotherToken = "Email_sendAnotherToken",

            eventcomment_getEventComments = "eventcomment_getEventComments",
            eventcomment_sendACommentMessage = "eventcomment_sendACommentMessage",

            fcmToken_removeTokenFromSystem = "fcmToken_removeTokenFromSystem",

            friend_accept = "friend_accept",
            friend_cancel = "friend_cancel",
            friend_sendRequest = "friend_sendRequest",
            friend_delete = "friend_delete",

            hobbies_addHobby = "hobbies_addHobby",
            hobbies_removeHobby = "hobbies_removeHobby",

            login_email = "login_email",
            login_facebook = "login_facebook",

            upload_profilePicture = "upload_profilePicture";

    }
}
