package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;

public class Event_hide extends APIRequest{

    public class Param {
        public final static String
                EventID = MySQLColumns.EventID;
    }

    public class Response {
        public static final String hidedEvent = "hidedEvent";
    }

    public class ResponseFields {
        public static final String event = "event";

    }


    public Event_hide(int eventID){
        keyValues.put(Param.EventID, eventID);
        APIUrl = APIValues.Links.event_hide;


    }

}
