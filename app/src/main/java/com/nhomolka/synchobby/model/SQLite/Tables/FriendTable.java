package com.nhomolka.synchobby.model.SQLite.Tables;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.nhomolka.synchobby.model.Extensions.DateExtension;
import com.nhomolka.synchobby.model.Extensions.ExtensionsHack;
import com.nhomolka.synchobby.model.SQLite.Objects.Friend;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;
import com.nhomolka.synchobby.model.Constants.SQLiteType;
import com.nhomolka.synchobby.model.Constants.SQLiteColumns;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public class FriendTable{
    private static final String TAG = "##FriendTable";

    private static FriendTable mFriendTable;




    public static final String  TABLE_NAME = "friend",
                                AppUserID = MySQLColumns.AppUserID,
                                ClientUpdatedTS  = SQLiteColumns.ClientUpdatedTS,
                                EventID = MySQLColumns.EventID,
                                ImageVersion = MySQLColumns.ImageVersion,
                                Name = MySQLColumns.Name,
                                ReceivedTS = SQLiteColumns.ReceivedTS,
                                State = MySQLColumns.State,
                                UpdatedTS = MySQLColumns.UpdatedTS;

    public static final String TABLE_CREATION =
                                "CREATE table " + TABLE_NAME +
                                    " (" +
                                        AppUserID +" INTEGER PRIMARY KEY, " +
                                        ClientUpdatedTS + " " + SQLiteType.TimestampDefault +", " +
                                        EventID + " INTEGER DEFAULT -1," +
                                        ImageVersion + " INTEGER, " +
                                        Name + " TEXT, " +
                                        ReceivedTS + " " + SQLiteType.TimestampDefault +", " +
                                        State + " INTEGER, " +
                                        UpdatedTS + " TIMESTAMP " +
                                    ")";

    String[] mColumns = new String[]{AppUserID, Name, EventID, ImageVersion};

    SQLiteDatabase mDatabase;

    private FriendTable(SQLiteDatabase sqLiteDatabase){
        mDatabase = sqLiteDatabase;
    }

    public static FriendTable getInstance(SQLiteDatabase sqLiteDatabase) {
        if (mFriendTable == null) {
            mFriendTable = new FriendTable(sqLiteDatabase);
        }
        return mFriendTable;
    }

    public void rebuildTable(){
        mDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        mDatabase.execSQL(TABLE_CREATION);
    }

    //DELETE
    public void deleteFriend(int appUserID){
        String query = "DELETE FROM " + TABLE_NAME + " WHERE " + AppUserID + "=" + Integer.toString(appUserID);
        mDatabase.execSQL(query);
    }

    //INSERT
    public void insertOrReplacesFriendsFromMySQL(JSONArray jsonArray){

        mDatabase.beginTransaction();

        try {
            for(int i =0; i < jsonArray.length(); i++){
                insertOrReplaceFriendFromMySQL(jsonArray.getJSONObject(i));
            }
            mDatabase.setTransactionSuccessful();

        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            mDatabase.endTransaction();
        }
    }

    public void insertOrReplaceFriendFromMySQL(JSONObject friendJSONObject){
        StringBuilder sb = new StringBuilder();

        sb.append("INSERT OR REPLACE INTO " + TABLE_NAME+ " (");

        sb.append(
                MySQLColumns.AppUserID + ", " +
                MySQLColumns.EventID + ", " +
                MySQLColumns.ImageVersion + ", " +
                MySQLColumns.Name + ", " +
                MySQLColumns.State + ", " +
                MySQLColumns.UpdatedTS + ""
        );

        sb.append(") VALUES (");

        try {
            sb.append(
                    Integer.toString(friendJSONObject.getInt(MySQLColumns.AppUserID)) + ", " +
                            Integer.toString(friendJSONObject.isNull(MySQLColumns.EventID) ? -1 : friendJSONObject.getInt(MySQLColumns.EventID)) + ", " +
                            Integer.toString(friendJSONObject.isNull(MySQLColumns.ImageVersion) ? -1 : friendJSONObject.getInt(MySQLColumns.ImageVersion)) + ", " +
                            "'" + friendJSONObject.getString(MySQLColumns.Name) + "', " +
                            Integer.toString(friendJSONObject.getInt(MySQLColumns.State)) + ", " +
                            "'" + DateExtension.CalendarMySQLToSQLite( friendJSONObject.getString(MySQLColumns.UpdatedTS)) + "'"
            );
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sb.append(")");

        mDatabase.execSQL(sb.toString());
    }

    //SELECT

    public ArrayList<Friend> selectAllFriends(){
//        Log.i(TAG, "selectAllFriends");

        ArrayList<Friend> friends = new ArrayList<>();
        Log.i(TAG, toString());

        Cursor cursor = mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        while(cursor.moveToNext()){
            friends.add(new Friend(cursor));
        }

        cursor.close();

        return friends;
    }

    public Friend selectFriendByAppUserID(int appUserID){
        Cursor cursor = mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + AppUserID + "=" + appUserID , null);
        while(cursor.moveToNext()){
            return new Friend(cursor);
        }
        return null;
    }

    public ArrayList<Friend> selectFriendsByGreaterThanClientUpdatedTS(String clientUpdatedTS){
        ArrayList<Friend> friends = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + ClientUpdatedTS + ">'" + clientUpdatedTS + "' ORDER BY " + AppUserID;
        Cursor cursor = mDatabase.rawQuery(query, null);
        while(cursor.moveToNext()){ friends.add(new Friend(cursor)); }
        cursor.close();
        return friends;
    }



    //UPDATE

    public void updateFriendState(int state, int appUserID){
        Log.i(TAG, "updateFriendState");
        mDatabase.execSQL("UPDATE " + TABLE_NAME + " SET " + State + "=" + state + ", " + UpdatedTS + "=" + SQLiteType.DefaultSTRFTIME + " WHERE " + AppUserID + "=" + appUserID);
    }

    public void updateFriendsFromMySQL(JSONArray jsonArray){
        Log.i(TAG, "updateFriendsFromMySQL");
        mDatabase.beginTransaction();
        String clientUpdatedTS = DateExtension.CalendarToSQLiteTS(Calendar.getInstance());
        try {
            for(int i =0; i < jsonArray.length(); i++){
                updateFriendFromMySQL(jsonArray.getJSONObject(i), clientUpdatedTS);
            }
            mDatabase.setTransactionSuccessful();

        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            mDatabase.endTransaction();
        }
    }

    public void updateFriendFromMySQL(JSONObject friendJSONObject, String clientUpdatedTS){
        Log.i(TAG, "insertOrReplaceFriendFromMySQL");

        if(clientUpdatedTS == null){
            clientUpdatedTS = DateExtension.CalendarToSQLiteTS(Calendar.getInstance());
        }

        StringBuilder sb = new StringBuilder();

        sb.append("INSERT OR REPLACE INTO " + TABLE_NAME+ " (");

        sb.append(
                MySQLColumns.AppUserID + ", " +SQLiteColumns.ClientUpdatedTS + ", " +MySQLColumns.EventID + ", " +
                MySQLColumns.ImageVersion + ", " + MySQLColumns.Name + ", " + MySQLColumns.State + ", " +
                MySQLColumns.UpdatedTS + ""
        );

        sb.append(") VALUES (");

        try {
            sb.append(
                    Integer.toString(friendJSONObject.getInt(MySQLColumns.AppUserID)) + ", " +
                            "'" + clientUpdatedTS + "', " +
                            Integer.toString(friendJSONObject.isNull(MySQLColumns.EventID) ? -1 : friendJSONObject.getInt(MySQLColumns.EventID)) + ", " +
                            Integer.toString(friendJSONObject.isNull(MySQLColumns.ImageVersion) ? -1 : friendJSONObject.getInt(MySQLColumns.ImageVersion)) + ", " +
                            "'" + friendJSONObject.getString(MySQLColumns.Name) + "', " +
                            Integer.toString(friendJSONObject.getInt(MySQLColumns.State)) + ", " +
                            "'" + DateExtension.CalendarMySQLToSQLite( friendJSONObject.getString(MySQLColumns.UpdatedTS)) + "'"
            );
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sb.append(")");

        mDatabase.execSQL(sb.toString());
    }

    public void confirmFriend(int appUserID){
        Log.i(TAG, "confirmFriend: " + Integer.toString(appUserID));

        String query = "UPDATE "+ TABLE_NAME + " SET " +
                State + "=2, " +
                ClientUpdatedTS + "='" + DateExtension.CalendarToSQLiteTS(Calendar.getInstance()) + "'" +
                " WHERE " + AppUserID + "=" +Integer.toString(appUserID);



        Log.i(TAG, query);


        mDatabase.execSQL(query);
    }

    public String toString() {
        return DatabaseUtils.dumpCursorToString(mDatabase.rawQuery("SELECT * FROM " + TABLE_NAME, null));
    }
}



//public static enum FRIEND_STATE{
//    REQUESTED(0),
//    RESPONSE(1),
//    CONFIRMED(2),
//    BANNED(3);
//
//    private final int value;
//
//    FRIEND_STATE(final int value){
//        this.value = value;
//    }
//
//    public int getValue() {
//        return value;
//    }
//}