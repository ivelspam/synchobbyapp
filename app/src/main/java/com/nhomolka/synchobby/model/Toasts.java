package com.nhomolka.synchobby.model;

import android.content.Context;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.nhomolka.synchobby.model.ServerRequest.ServerCalls.APIRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class Toasts {

    public static boolean responseIsGoodToaster(JSONObject jsonObject, Context context) throws JSONException {
        switch (jsonObject.getString(APIRequest.message)){
            case "problem connecting":
                switch (jsonObject.getString("error")){
                    case "MalformedURLException" :
                        errorToast("MalformedURLException", context);
                        break;
                    case "ConnectTimeoutException" :
                        errorToast("Server is Down. (1)", context);
                        break;
                    case "SocketTimeoutException" :
                        errorToast("Server is Down. (2)", context);
                        break;
                    case "IOException" :
                        errorToast("You Have no Connection to Internet", context);
                        break;
                }
                break;
            case "JSONException":
                errorToast("JSONException", context);
                break;
        }

        switch (jsonObject.getString(APIRequest.message)){
            case "problem connecting":
                return false;
            case "JSONException":
                return false;
        }
        return true;
    }

    public static Boolean responseIsGood(JSONObject jsonObject, Context context) throws JSONException {
        switch (jsonObject.getString(APIRequest.message)){
            case "problem connecting":
                return false;
            case "JSONException":
                return false;
        }

        return true;
    }

    private static void errorToast(String Message, Context context) {
        Toast.makeText(context, "Error Connection with the server : " + Message, Toast.LENGTH_LONG).show();
        LoginManager.getInstance().logOut();
    }

}
