package com.nhomolka.synchobby.model.ServerRequest.ServerCalls;

import com.nhomolka.synchobby.model.Constants.APIValues;
import com.nhomolka.synchobby.model.Constants.MySQLColumns;


public class Eventcomment_getEventComments extends APIRequest {

    public class Param {
        public final static String
                EventID  = MySQLColumns.EventID;
    }

    public class Response {
        public static final String foundComments = "foundComments",
                noComments = "noComments";
    }

    public class ResponseFields {
        public static final String comments = "comments";
    }

    public Eventcomment_getEventComments(int eventID){
        keyValues.put(Param.EventID, eventID);
        APIUrl = APIValues.Links.eventcomment_getEventComments;
        token = false;
    }

}


